--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

--
-- Data for Name: t_config_collection; Type: TABLE DATA; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--

INSERT INTO gisdata_test.t_config_collection(id,auxiliary_variable,config_function,label,aggregated,catalog_name,schema_name,table_name,column_ident,column_name,unit,description,closed,edit_date) VALUES
(101,2,100,'OLIL-2013',false,'contrib_regression','gisdata_test','olil_2013_forest','gid','geom',0.0001,'Land use indicated by OLIL 2013.',true,now());				

INSERT INTO gisdata_test.t_config_collection(id,config_function,label,aggregated,catalog_name,schema_name,table_name,column_ident,column_name,description,closed,edit_date) VALUES
(102,500,'PLOTS',false,'contrib_regression','gisdata_test','plots',''::varchar,'geom','Layer points.',true,now());

INSERT INTO gisdata_test.t_config_collection(id,config_function,label,aggregated,description,closed,edit_date,ref_id_layer_points,ref_id_total) VALUES
(103,600,'PLOTS X OLIL-2013',false,'Intersection between PLOTS X OLIL-2013.',true,now(),102,101);

INSERT INTO gisdata_test.t_config_collection(id,config_function,label,aggregated,catalog_name,schema_name,table_name,column_ident,column_name,unit,description,closed,edit_date) VALUES
(104,200,'tcd_2012_100m',false,'contrib_regression','gisdata_test','tcd_2012_100m','rid','rast',0.0001,'Tree Cover Density (TCD) 2012.',true,now());

INSERT INTO gisdata_test.t_config_collection(id,config_function,label,aggregated,description,closed,edit_date,ref_id_layer_points,ref_id_total) VALUES
(105,600,'PLOTS X tcd_2012_100m',false,'Intersection between PLOTS X tcd_2012_100m.',true,now(),102,104);

INSERT INTO gisdata_test.t_config_collection(id,config_function,label,aggregated,catalog_name,schema_name,table_name,column_ident,column_name,unit,description,closed,edit_date) VALUES
(106,200,'dlt_2012_100m',false,'contrib_regression','gisdata_test','dlt_2012_100m','rid','rast',0.0001,'Dominant Leaf Type (DLT) 2012.',true,now());

INSERT INTO gisdata_test.t_config_collection(id,config_function,label,aggregated,description,closed,edit_date) VALUES
(107,300,'OLIL-2013-forest x dlt_2012_100m',false,'OLIL-2013-forest x dlt_2012_100m',true,now());

INSERT INTO gisdata_test.t_config_collection(id,config_function,"label",aggregated,description,closed,edit_date,ref_id_layer_points,ref_id_total) VALUES
(108,600,'PLOTS X dlt_2012_100m',false,'Insersection between PLOTS X dlt_2012_100m.',true,now(),102,106),
(109,600,'PLOTS X OLIL-2013-forest x dlt_2012_100m',false,'Insersection between PLOTS X OLIL-2013-forest x dlt_2012_100m.',true,now(),102,107);

INSERT INTO gisdata_test.t_config_collection(id,config_function,label,aggregated,description,closed,edit_date) VALUES
(110,400,'dlt_2012_100m X tcd_2012_100m',false,'dlt_2012_100m X tcd_2012_100m',true,now());

INSERT INTO gisdata_test.t_config_collection(id,config_function,"label",aggregated,description,closed,edit_date,ref_id_layer_points,ref_id_total) VALUES
(111,600,'PLOTS X dlt_2012_100m X tcd_2012_100m',false,'Insersection between PLOTS X dlt_2012_100m X tcd_2012_100m.',true,now(),102,110);

--
-- Data for Name: t_config; Type: TABLE DATA; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--

INSERT INTO gisdata_test.t_config(id,config_collection,config_query,label,categories,condition,description,edit_date) VALUES
(101,101,100,'forest',null::varchar,null::varchar,'Forest area indicated by OLIL 2013.',now()),
(102,101,300,'non-forest',101,'(olil_2013 IS DISTINCT FROM 100)'::varchar,'Non-forest area indicated by OLIL 2013.',now());

INSERT INTO gisdata_test.t_config(id,config_collection,config_query,label,band,description,edit_date) VALUES
(103,104,100,'tree_cover_density',1,'Tree cover density.',now());

INSERT INTO gisdata_test.t_config(id,config_collection,config_query,label,categories,band,reclass,description,edit_date) VALUES
(104,106,100,'dlt_2012_100m - coniferous',null::varchar,1,1,'dlt_2012_100m - coniferous',now()),
(105,106,100,'dlt_2012_100m - broadleaves',null::varchar,1,2,'dlt_2012_100m - broadleaves',now()),
(106,106,300,'dlt_2012_100m - others','104,105',null::int,null::int,'dlt_2012_100m - others',now());

INSERT INTO gisdata_test.t_config(id,config_collection,config_query,label,complete,categories,vector,raster,description,edit_date) VALUES
(107,107,100,'OLIL-2013-forest x dlt_2012_100m - coniferous',null::varchar,null::varchar,101,104,'OLIL-2013-forest x dlt_2012_100m - coniferous',now()),
(108,107,100,'OLIL-2013-forest x dlt_2012_100m - broadleaves',null::varchar,null::varchar,101,105,'OLIL-2013-forest x dlt_2012_100m - broadleaves',now()),
(109,107,400,'OLIL-2013-forest x dlt_2012_100m - others','101','107,108',null::int,null::int,'OLIL-2013-forest x dlt_2012_100m - others',now()),
(110,107,400,'OLIL-2013-non-forest x dlt_2012_100m - coniferous','104','107',null::int,null::int,'OLIL-2013-non-forest x dlt_2012_100m - coniferous',now()),
(111,107,400,'OLIL-2013-non-forest x dlt_2012_100m - broadleaves','105','108',null::int,null::int,'OLIL-2013-non-forest x dlt_2012_100m - broadleaves',now()),
(112,107,400,'OLIL-2013-non-forest x dlt_2012_100m - others','102','110,111',null::int,null::int,'OLIL-2013-non-forest x dlt_2012_100m - others',now());

INSERT INTO gisdata_test.t_config(id,config_collection,config_query,label,complete,categories,raster,raster_1,description,edit_date) VALUES
(113,110,100,'dlt_2012_100m X tcd_2012_100m - coniferous',null::varchar,null::varchar,104,103,'dlt_2012_100m X tcd_2012_100m - coniferous',now()),
(114,110,100,'dlt_2012_100m X tcd_2012_100m - broadleaves',null::varchar,null::varchar,105,103,'dlt_2012_100m X tcd_2012_100m - broadleaves',now()),
(115,110,400,'dlt_2012_100m X tcd_2012_100m - others','103','113,114',null::integer,null::integer,'dlt_2012_100m X tcd_2012_100m - others',now());

--
-- Data for Name: f_p_plot; Type: TABLE DATA; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--

INSERT INTO gisdata_test.f_p_plot(config_collection,country,strata_set,stratum,panel,cluster,plot,geom)
SELECT
	102 AS config_collection,
	country,
	strata_set,
	stratum,
	panel,
	cluster,
	plot,
	geom
from 
	gisdata_test.plots order by gid;
