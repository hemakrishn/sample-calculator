--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

WITH
w1 AS 	(SELECT * FROM gisdata_test.fn_get_configs4aux_data_app(105,(select max(id) from gisdata_test.c_gui_version),0,false)),
w2 as 	(
	SELECT
	(gisdata_test.fn_get_aux_data_app(config_collection,config,intersects,gid_start,gid_end)) AS res
	FROM w1 WHERE step = 1 ORDER BY config_collection, config
	)
INSERT INTO gisdata_test.t_auxiliary_data(config_collection,config,value,ext_version,gui_version,gid)
select
	(res).config_collection,
	(res).config,
	(res).value,
	(res).ext_version,
	(select max(id) from gisdata_test.c_gui_version) as gui_version,
	(res).gid
FROM
	w2 ORDER BY config_collection, config, gid
;
WITH
w AS	(
	SELECT gid,config,value
	FROM gisdata_test.t_auxiliary_data
	WHERE config_collection IN (105)
	)
SELECT
	f_p_plot.country,
	f_p_plot.strata_set,
	f_p_plot.stratum,
	f_p_plot.panel,
	f_p_plot.cluster,
	f_p_plot.plot,
	t_config.label AS auxiliary_variable_category,
	w.value
FROM w
INNER JOIN gisdata_test.f_p_plot ON w.gid = f_p_plot.gid
INNER JOIN gisdata_test.t_config ON w.config = t_config.id
ORDER BY country, strata_set, stratum, panel, cluster, plot, auxiliary_variable_category
;