--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------;
--	fn_t_config_collection__before_insert
--------------------------------------------------------------------------------;

	DROP FUNCTION IF EXISTS @extschema@.fn_t_config_collection__before_insert() CASCADE;

	CREATE OR REPLACE FUNCTION @extschema@.fn_t_config_collection__before_insert()
	RETURNS trigger AS
	$$
	DECLARE
		_condition	varchar;
	BEGIN
		IF (NEW.config_function = 500)
		THEN
			IF ((NEW.condition IS NULL) OR (trim(NEW.condition) = '')) 
			THEN
				_condition := 'TRUE'::varchar;
			ELSE
				_condition := NEW.condition;
			END IF;
			
			EXECUTE
				'SELECT coalesce(count(*),0) '||
				'FROM '||NEW.schema_name||'.'||NEW.table_name||' '||
				'WHERE '||_condition||';'
			INTO NEW.total_points;

			IF (NEW.total_points = 0)
			THEN
				RAISE EXCEPTION 'Error 01: fn_t_config_collection__before_insert: The value of "total_points" cannot by zero.';
			END IF;
		ELSE
			NEW.total_points := NULL::integer;
		END IF;
		RETURN NEW;
	END;
	$$
	LANGUAGE plpgsql
	VOLATILE
	SECURITY INVOKER;

--------------------------------------------------------------------------------;
-- vlastnik a opravneni
--------------------------------------------------------------------------------;

	ALTER FUNCTION @extschema@.fn_t_config_collection__before_insert()
	OWNER TO adm_nfiesta_gisdata;

	GRANT ALL
	ON FUNCTION @extschema@.fn_t_config_collection__before_insert()
	TO adm_nfiesta_gisdata;

	GRANT EXECUTE
	ON FUNCTION @extschema@.fn_t_config_collection__before_insert()
	TO app_nfiesta_gisdata;

	GRANT ALL
	ON FUNCTION @extschema@.fn_t_config_collection__before_insert()
	TO public;

--------------------------------------------------------------------------------;
-- dokumentace
--------------------------------------------------------------------------------;

	COMMENT ON FUNCTION @extschema@.fn_t_config_collection__before_insert()
	IS
	'Funkce triggeru, před vložením záznamu do tabulky t_config_collection provede výpočet počtu bodů v bodové vrstvě a zapíše výsledek do sloupce total_points.';

--------------------------------------------------------------------------------;