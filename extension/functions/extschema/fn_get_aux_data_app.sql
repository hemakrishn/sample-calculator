--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_aux_data_app(integer, integer, boolean, integer, integer)
  
CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_data_app
(
	_config_collection 	integer,
	_config			integer,
	_intersects		boolean,
	_gid_start		integer,
	_gid_end		integer
)
RETURNS TABLE
(
	config_collection		integer,
	config				integer,
	gid				integer,
	value				double precision,
	ext_version			integer
) AS
$BODY$
DECLARE
	_ext_version_label_system			text;
	_ext_version_current				integer;
	_config_function_600				integer;
	_ref_id_layer_points				integer;
	_config_collection_4_config			integer;
	_config_query					integer;
	_vector						integer;			
	_raster						integer;
	_raster_1					integer;
	_band						integer;
	_reclass					integer;		
	_condition					character varying;
	_config_function				integer;
	_schema_name					character varying;
	_table_name					character varying;
	_column_ident					character varying;
	_column_name					character varying;
	_unit						double precision;
	_q						text;
	_ids4comb					integer[];
	_config_collection_transform_comb_i		integer;
	_config_collection_transform_comb		integer[];
	_check_gid					integer;
	_complete					character varying;
	_categories					character varying;
	_config_ids_text_complete			text;
	_complete_length				integer;
	_config_ids_complete				integer[];
	_config_ids4check_complete			integer[];
	_complete_exists				boolean[];
	_config_ids_text_categories			text;
	_categories_length				integer;
	_config_ids_categories				integer[];
	_config_ids4check_categories			integer[];
	_categories_exists				boolean[];
	_config_ids					integer[];
	_complete_categories_exists			boolean[];
	_config_collection_transform_i			integer;
	_aggregated					boolean;
	_config_collection_transform			integer[];
	_check_count_config_ids_i			integer;
	_check_count_config_ids				integer[];
	_check_gid_i					integer;
	_config_ids_4_complete_array			integer[];
	_config_collection_4_complete_array		integer[];
	_config_ids_4_categories_array			integer[];
	_config_collection_4_categories_array		integer[];
	_aggregated_200					boolean;
	_check_categories_i				double precision[];


	-- NEW --
	_ext_version_valid_from				integer;
	_ext_version_valid_until			integer;
	_ext_version_valid_from_label			character varying;
	_ext_version_valid_until_label			character varying;
	_config_function4version_interval		integer;		
BEGIN 
	-------------------------------------------------------------
	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_data_app: Vstupni argument _config_collection nesmi byt NULL!';
	END IF;

	IF _config IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_data_app: Vstupni argument _config nesmi byt NULL!';
	END IF;

	IF _intersects IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_data_app: Vstupni argument _intersects nesmi byt NULL!';
	END IF;

	IF _gid_start IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_data_app: Vstupni argument _gid_start nesmi byt NULL!';
	END IF;

	IF _gid_end IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_aux_data_app: Vstupni argument _gid_end nesmi byt NULL!';
	END IF;
	-------------------------------------------------------------
	-------------------------------------------------------------
	-- proces ziskani aktualni extenze nfiesta_gisdata
	SELECT extversion FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_aux_data_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_aux_data_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;	
	-------------------------------------------------------------
	-------------------------------------------------------------	
	-- ziskani promennych z konfigurace tabulky t_config_collection pro _config_collection
	SELECT
		tcc.config_function,
		tcc.ref_id_layer_points
	FROM
		@extschema@.t_config_collection AS tcc
	WHERE
		id = _config_collection
	INTO
		_config_function_600,
		_ref_id_layer_points;
	-------------------------------------------------------------
	-- kontrola ziskanych promennych jestli nejsou NULL
	IF _config_function_600 IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_get_aux_data_app: Pro vstupni argument config_collection = % nenalezen zaznam config_function v tabulce t_config_collection!',_config_collection;
	END IF;

	IF _ref_id_layer_points IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_aux_data_app: Pro vstupni argument config_collection = % nenalezen zaznam ref_id_layer_points v tabulce t_config_collection!',_config_collection;
	END IF;
	-------------------------------------------------------------
	-- kontrola, ze config_function musi byt hodnota 600
	IF _config_function_600 IS DISTINCT FROM 600
	THEN
		RAISE EXCEPTION 'Chyba 10: fn_get_aux_data_app: Pro vstupni argument config_collection = % nalezen v tabulce t_config_collection zaznam config_function, ktery ale neni hodnota 600!',_config_collection;
	END IF;	
	-------------------------------------------------------------
	-- ziskani promennych z konfigurace tabulky t_config pro _config
	SELECT
		tc.config_collection,
		tc.config_query,
		tc.vector,
		tc.raster,
		tc.raster_1,
		tc.band,
		tc.reclass,		
		tc.condition
	FROM
		@extschema@.t_config AS tc
	WHERE
		tc.id = _config
	INTO
		_config_collection_4_config,
		_config_query,
		_vector,
		_raster,
		_raster_1,
		_band,
		_reclass,
		_condition;
	-------------------------------------------------------------
	-- kontrola ziskanych promennych jestli nejsou NULL
	IF _config_collection_4_config IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 11: fn_get_aux_data_app: Pro vstupni argument config = % nenalezen zaznam config_collection v tabulce t_config!',_config;
	END IF;	
		
	IF _config_query IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 12: fn_get_aux_data_app: Pro vstupni argument config = % nenalezen zaznam config_query v tabulce t_config!',_config;
	END IF;	
	-------------------------------------------------------------
	-- ziskani promennych z konfigurace tabulky t_config_collection pro _config_collection_4_config
	SELECT
		tcc.config_function,
		tcc.schema_name,
		tcc.table_name,
		tcc.column_ident,
		tcc.column_name,
		tcc.unit
	FROM
		@extschema@.t_config_collection AS tcc
	WHERE
		tcc.id = _config_collection_4_config
	INTO
		_config_function,
		_schema_name,
		_table_name,
		_column_ident,
		_column_name,
		_unit;
	-------------------------------------------------------------
	-- kontrola ziskanych promennych jestli nejsou NULL
	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 13: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam config_function v tabulce t_config_collection!',_config_collection_4_config;
	END IF;
	-------------------------------------------------------------
	-- NEW --
	IF NOT(_config_function = ANY(array[100,200,300,400]))
	THEN
		RAISE EXCEPTION 'Error 14: fn_get_aux_data_app: Interni promenna _config_function = % musi byt hodnota 100, 200, 300 nebo 400!',_config_function;
	END IF;

	IF _config_function = ANY(array[100,200,400])
	THEN
		IF _config_function = 400 THEN _config_function4version_interval := 200; ELSE _config_function4version_interval := _config_function; END IF;

		-- zjisteni ext_version_valid_from a ext_version_valid_until
		SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
		FROM @extschema@.cm_ext_config_function AS cmcf
		WHERE cmcf.active = TRUE
		AND cmcf.config_function = _config_function4version_interval
		INTO
			_ext_version_valid_from,
			_ext_version_valid_until;		
	ELSE
		-- varianta VxR => _config_function = 300

		WITH
		w1 AS	(
			SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
			FROM @extschema@.cm_ext_config_function AS cmcf
			WHERE cmcf.active = TRUE
			AND cmcf.config_function = 100
			),
		w2 AS	(
			SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
			FROM @extschema@.cm_ext_config_function AS cmcf
			WHERE cmcf.active = TRUE
			AND cmcf.config_function = 200
			),
		w3 AS	(
			SELECT * FROM w1 UNION ALL
			SELECT * FROM w2
			)
		SELECT
			max(ext_version_valid_from),
			max(ext_version_valid_until)
		FROM
			w3
		INTO
			_ext_version_valid_from,
			_ext_version_valid_until;
	END IF;	
	
	IF (_ext_version_valid_from IS NULL OR _ext_version_valid_until IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 15: fn_get_aux_data_app: Pro interni promennou _config_function = % nenalezeno ext_version_valid_from nebo ext_version_valid_until v tabulce cm_ext_config_function!',_config_function;
	END IF;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_from  INTO _ext_version_valid_from_label;
	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_until INTO _ext_version_valid_until_label;
	-------------------------------------------------------------
	-------------------------------------------------------------	 
	CASE
	WHEN _config_query = 100 -- vetev pro zakladni protinani
	THEN
		-- kontrola ziskanych promennych pro protinani
		IF _config_function = ANY(array[100,200])
		THEN
			-- _schema_name
			IF ((_schema_name IS NULL) OR (trim(_schema_name) = ''))
			THEN
				RAISE EXCEPTION 'Chyba 16: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam schema_name v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- _table_name
			IF ((_table_name IS NULL) OR (trim(_table_name) = ''))
			THEN
				RAISE EXCEPTION 'Chyba 17: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam table_name v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- _column_ident
			IF (_column_ident IS NULL) OR (trim(_column_ident) = '')
			THEN
				RAISE EXCEPTION 'Chyba 18: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam column_ident v tabulce t_config_collection!',_config_collection_4_config;
			END IF;
			
			-- _column_name
			IF (_column_name IS NULL) OR (trim(_column_name) = '')
			THEN
				RAISE EXCEPTION 'Chyba 19: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam column_name v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- unit
			IF _unit IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 20: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam unit v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- pokud unit neni NULL => vynasobit vzdy hodnotou 10000
			_unit := _unit * 10000.0;
		END IF;
		-----------------------------------------------------------------------------------
		CASE
			WHEN (_config_function = 100)	-- VECTOR
			THEN
				-- raise notice 'PRUCHOD CQ 100 VEKTOR';
				
				-- kontrola, ze _intersects je opravdu nastaveno na TRUE
				IF _intersects = TRUE
				THEN
					_q := @extschema@.fn_sql_aux_data_vector_app(_config_collection,_config,_schema_name,_table_name,_column_ident,_column_name,_condition,_gid_start,_gid_end);
				ELSE
					RAISE EXCEPTION 'Chyba 21: fn_get_aux_data_app: Jde-li se o konfiguraci, ktera je protinaci (config_query = 100), pak vstupni argument _intersects musi byt TRUE!';
				END IF;

			WHEN (_config_function = 200)	-- RASTER
			THEN
				-- raise notice 'PRUCHOD CQ 100 RASTER';
			
				-- kontrola, ze _intersects je opravdu nastaveno na TRUE
				IF _intersects = TRUE
				THEN			
					_q := @extschema@.fn_sql_aux_data_raster_app(_config_collection,_config,_schema_name,_table_name,_column_ident,_column_name,_band,_reclass,_condition,_unit,_gid_start,_gid_end);
				ELSE
					RAISE EXCEPTION 'Chyba 22: fn_get_aux_data_app: Jde-li se o konfiguraci, ktera je protinaci (config_query = 100), pak vstupni argument _intersects musi byt TRUE!';
				END IF;

			WHEN (_config_function IN (300,400))	-- KOMBINACE
			THEN
				-- raise notice 'PRUCHOD CQ 100 KOMBINACE';	
							
				-- spojeni hodnot _vector,_raster,_raster_1 do pole a odstraneni NULL hodnoty z pole
				SELECT array_agg(t.ids)
				FROM (SELECT unnest(array[_vector,_raster,_raster_1]) AS ids) AS t
				WHERE t.ids IS NOT NULL
				INTO _ids4comb;

				IF array_length(_ids4comb,1) != 2
				THEN
					RAISE EXCEPTION 'Chyba 23: fn_get_aux_data_app: Pocet prvku v interni promenne (_ids4comb = %) musi byt 2! Respektive jde-li v konfiguraci o interakci, pak v tabulce t_config u id = % muze byt vyplnena hodnota jen pro kombinaci vector a raster nebo raster a raster_1.',_ids4comb, _config;
				END IF;

				-- proces ziskani ID config_collection pro vector a raster z neagregovane skupiny config_collection 600
				-- a zaroven kontrola PROVEDITELNOSTI, ze pro kombinaci jsou potrebna data jiz ulozena v DB
				
				FOR i IN 1..array_length(_ids4comb,1)
				LOOP
					-- zde v teto casti jde o CQ 100 a kombinaci, proto neni nutne
					-- se ohlizet na aggregated, ale vzdy se musi provest TRANSFORMACE config_collection
					-- pro VxR nebo pro RxR [obecne receno data pro provedeni kombinace uz musi existovat
					-- v 600vkove skupine]
					
					-- proces nalezeni transformovane config_collection
					SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
					WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _ids4comb[i])
					AND tcc.ref_id_layer_points = _ref_id_layer_points
					INTO _config_collection_transform_comb_i; 

					IF _config_collection_transform_comb_i IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 24: fn_get_aux_data_app: Pro provedeni interakce (konfigurace config_collection = % a config = %) nejsou v tabulce t_auxiliary_data ulozena pozadovana data!',_config_collection,_config;
					END IF;

					-- kontrola proveditelnoti kombinace
					IF	(
						SELECT count(t.gid) = 0
						FROM
							(
							SELECT DISTINCT tad.gid
							FROM @extschema@.t_auxiliary_data AS tad
							WHERE tad.config_collection = _config_collection_transform_comb_i
							AND tad.config = _ids4comb[i]
							AND (tad.ext_version >= _ext_version_valid_from AND tad.ext_version <= _ext_version_valid_until)
							AND (tad.gid >= _gid_start AND tad.gid <= _gid_end)
							) AS t
						)
					THEN
						RAISE EXCEPTION 'Chyba 25: fn_get_aux_data_app: Pro provedeni interakce (konfigurace config_collection = % a config = %) nejsou v tabulce t_auxiliary_data ulozena pozadovana data!',_config_collection,_config;
					END IF;

					IF i = 1
					THEN
						_config_collection_transform_comb := array[_config_collection_transform_comb_i];
					ELSE
						_config_collection_transform_comb := _config_collection_transform_comb || array[_config_collection_transform_comb_i];
					END IF;
				END LOOP;
				
				-- kontrola stejnych gid pro kombinaci
				WITH
				w1a AS	(
					SELECT
					tad1.*,
					max(tad1.est_date) OVER (PARTITION BY tad1.gid, tad1.config_collection, tad1.config) as max_est_date
					FROM @extschema@.t_auxiliary_data AS tad1
					WHERE tad1.config_collection = _config_collection_transform_comb[1]
					AND tad1.config = _ids4comb[1]
					AND (tad1.ext_version >= _ext_version_valid_from AND tad1.ext_version <= _ext_version_valid_until)
					AND (tad1.gid >= _gid_start AND tad1.gid <= _gid_end)
					),
				w1 AS	(
					SELECT w1a.gid FROM w1a WHERE w1a.est_date = w1a.max_est_date
					),
				-------------------------------------------------------------------				
				w2a AS	(
					SELECT
					tad2.*,
					max(tad2.est_date) OVER (PARTITION BY tad2.gid, tad2.config_collection, tad2.config) as max_est_date
					FROM @extschema@.t_auxiliary_data AS tad2
					WHERE tad2.config_collection = _config_collection_transform_comb[2]
					AND tad2.config = _ids4comb[2]
					AND (tad2.ext_version >= _ext_version_valid_from AND tad2.ext_version <= _ext_version_valid_until)
					AND (tad2.gid >= _gid_start AND tad2.gid <= _gid_end)
					),
				w2 AS	(
					SELECT w2a.gid FROM w2a WHERE w2a.est_date = w2a.max_est_date					
					),
				-------------------------------------------------------------------					
				w3 AS	(SELECT w1.gid FROM w1 EXCEPT SELECT w2.gid FROM w2),
				w4 AS	(SELECT w2.gid FROM w2 EXCEPT SELECT w1.gid FROM w1),
				w5 AS	(SELECT w3.gid FROM w3 UNION ALL SELECT w4.gid FROM w4)
				SELECT
					count(w5.gid) FROM w5 INTO _check_gid;

				IF _check_gid > 0
				THEN
					RAISE EXCEPTION 'Chyba 26: fn_get_aux_data_app: Pro konfiguraci config_collection = % a config = % nejsou v tabulce t_auxiliary_data vsechna potrebna data!',_config_collection,_config;
				END IF;

				_q :=
					'				
					WITH
					w1a AS	(
						SELECT
						tad1.*,
						max(tad1.est_date) OVER (PARTITION BY tad1.gid, tad1.config_collection, tad1.config) as max_est_date
						FROM @extschema@.t_auxiliary_data AS tad1
						WHERE tad1.config = $1
						AND tad1.config_collection = $6
						AND (tad1.ext_version >= $14 AND tad1.ext_version <= $15)
						AND (tad1.gid >= $12 AND tad1.gid <= $13)
						),
					w1 AS	(
						SELECT w1a.gid, w1a.value FROM w1a WHERE w1a.est_date = w1a.max_est_date
						),
					-----------------------------------------------------------					
					w2a AS	(
						SELECT
						tad2.*,
						max(tad2.est_date) OVER (PARTITION BY tad2.gid, tad2.config_collection, tad2.config) as max_est_date
						FROM @extschema@.t_auxiliary_data AS tad2
						WHERE tad2.config = $2
						AND tad2.config_collection = $11
						AND (tad2.ext_version >= $14 AND tad2.ext_version <= $15)
						AND (tad2.gid >= $12 AND tad2.gid <= $13)
						),
					w2 AS	(
						SELECT w2a.gid, w2a.value FROM w2a WHERE w2a.est_date = w2a.max_est_date
						),
					-----------------------------------------------------------				
					w3a AS	(
						SELECT
						tad3.*,
						max(tad3.est_date) OVER (PARTITION BY tad3.gid, tad3.config_collection, tad3.config) as max_est_date
						FROM @extschema@.t_auxiliary_data AS tad3
						WHERE tad3.config = $4
						AND tad3.config_collection = $3
						AND (tad3.ext_version >= $14 AND tad3.ext_version <= $15)
						AND (tad3.gid >= $12 AND tad3.gid <= $13)
						),
					w3 AS	(
						SELECT w3a.gid AS gid_except FROM w3a WHERE w3a.est_date = w3a.max_est_date
						),
					-----------------------------------------------------------						
					w4 AS	(
						SELECT
							$3 AS config_collection,
							$4 AS config,
							w1.gid,
							(w1.value * w2.value)::double precision AS value,
							$5 AS ext_version,
							CASE WHEN w3.gid_except IS NULL THEN FALSE ELSE TRUE END AS gid_exists
						FROM
								w1
						INNER JOIN 	w2 ON w1.gid = w2.gid
						LEFT  JOIN	w3 ON w1.gid = w3.gid_except
						)
					-----------------------------------------------------------	
					SELECT
						w4.config_collection,
						w4.config,
						w4.gid,
						w4.value,
						w4.ext_version
					FROM
						w4 WHERE w4.gid_exists = FALSE
					';
			ELSE
				RAISE EXCEPTION 'Chyba 27: fn_get_aux_data_app: Neznama hodnota _config_function = %!',_config_function;
		END CASE;
		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------
	WHEN _config_query IN (200,300,400,500) -- vetev pro soucet, doplnky nebo referenci
	THEN	
		IF _config_query = 500
		THEN
			-- raise notice 'PRUCHOD CQ 500';
			
			RAISE EXCEPTION 'Chyba 28: fn_get_aux_data_app: Jde-li o konfiguraci, ktera je referenci (config_query = %), tak u ni se protinani bodu neprovadi!',_config_query;
		END IF;

		---------------------------------------------------------------
		-- KONTROLA PROVEDITELNOSTI
		---------------------------------------------------------------
		-- pro config_query 200,300,400 => kontrola ze v DB jsou ulozena data pro complete a categories

		-- pokud se jedna o konfiguraci souctu nebo doplnku => pak nutna kontrola,
		-- ze complete nebo categories pro danou konfiguraci jsou jiz v DB pritomny
		-- a odpovidaji verzi spadajici do povoleneho intervalu VERZI [poznamka: tato
		-- kontrola je stejna jak pro vetev vypocet, tak i pro vetev prepocet]

		-- pokud nejde o agregovanou skupinu => pak se jako config_collection pouzije VSTUPNI config_collection
		-- pokud   jde o agregovanou skupinu => pak se jako config_collection pouzije transformace

		--------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------
		-- zjisteni complete a categories
		SELECT
			tc.complete,
			tc.categories
		FROM
			@extschema@.t_config AS tc
		WHERE
			tc.id = _config
		INTO
			_complete,
			_categories;
		-------------------------------------------
		-- _complete muze byt NULL
		-------------------------------------------
		IF _categories IS NULL	-- categories nesmi byt nikdy NULL
		THEN
			RAISE EXCEPTION 'Chyba 29: fn_get_aux_data_app: Pro vstupni argument _config = % nenalezen v tabulce t_config zaznam categories!',_config;
		END IF;
		-------------------------------------------
		-- _complete
		IF _complete IS NOT NULL
		THEN
			-- pridani ke _complete textu array[]
			_config_ids_text_complete := concat('array[',_complete,']');

			-- zjisteni poctu idecek tvorici _complete v
			EXECUTE 'SELECT array_length('||_config_ids_text_complete||',1)'
			INTO _complete_length;

			-- zjisteni existujicich konfiguracnich idecek pro _complete
			EXECUTE
			'SELECT array_agg(tc.id ORDER BY tc.id)
			FROM @extschema@.t_config AS tc
			WHERE tc.id in (select unnest('||_config_ids_text_complete||'))
			'
			INTO _config_ids_complete;

			-- splneni podminky stejneho poctu idecek
			IF _complete_length != array_length(_config_ids_complete,1)
			THEN
				RAISE EXCEPTION 'Chyba 30: fn_get_aux_data_app: Pro nektere nakonfigurovane complete = % neexistuje konfigurace (ID) v tabulce t_config!',_complete;
			END IF;

			-- kontrola zda i obsah je stejny
			EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text_complete||') AS ids) AS t'
			INTO _config_ids4check_complete;

			IF _config_ids4check_complete != _config_ids_complete
			THEN
				RAISE EXCEPTION 'Chyba 31: fn_get_aux_data_app: Pro nektere nakonfigurovane complete = % neexistuje konfigurace (ID) v tabulce t_config!',_complete;
			END IF;

			-- sestaveni _complete_exists
			SELECT array_agg(t.complete_exists) FROM (SELECT TRUE AS complete_exists, generate_series(1,_complete_length) AS id) AS t
			INTO _complete_exists;
			
		ELSE
			_complete_length := 0;
			_config_ids_complete := NULL::integer[];
			_complete_exists := NULL::boolean[];
		END IF;
		-------------------------------------------
		-- _categories
		
		-- bude se pracovat zvlast s _categories
		_config_ids_text_categories := concat('array[',_categories,']');

		-- zjisteni poctu idecek tvorici _categories v
		EXECUTE 'SELECT array_length('||_config_ids_text_categories||',1)'
		INTO _categories_length;	-- TOTO

		-- zjisteni existujicich konfiguracnich idecek pro _categories
		EXECUTE
		'SELECT array_agg(tc.id ORDER BY tc.id)
		FROM @extschema@.t_config AS tc
		WHERE tc.id in (select unnest('||_config_ids_text_categories||'))
		'
		INTO _config_ids_categories;

		-- splneni podminky stejneho poctu idecek
		IF _categories_length != array_length(_config_ids_categories,1)
		THEN
			RAISE EXCEPTION 'Chyba 32: fn_get_aux_data_app: Pro nektere nakonfigurovane categories = % neexistuje konfigurace (ID) v tabulce t_config!',_categories;
		END IF;

		-- kontrola zda i obsah je stejny
		EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text_categories||') AS ids) AS t'
		INTO _config_ids4check_categories;

		IF _config_ids4check_categories != _config_ids_categories
		THEN
			RAISE EXCEPTION 'Chyba 33: fn_get_aux_data_app: Pro nektere nakonfigurovane categories = % neexistuje konfigurace (ID) v tabulce t_config!',_categories;
		END IF;

		-- sestaveni _categories_exists
		SELECT array_agg(t.categories_exists) FROM (SELECT FALSE AS categories_exists, generate_series(1,_categories_length) AS id) AS t
		INTO _categories_exists;
		-------------------------------------------
		-- spojeni konfiguracnich idecek _complete/_categories
		IF _complete_length = 0
		THEN
			-- jen categories
			_config_ids := _config_ids_categories;
			_complete_categories_exists := _categories_exists;
		ELSE
			-- _complete || _categories
			_config_ids := _config_ids_complete || _config_ids_categories;
			_complete_categories_exists := _complete_exists || _categories_exists;
		END IF;
		--------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------

		------------------------------------------------------------------------------------------------------
		-- CYKLUS KONTROLY PROVEDITELNOSTI => cast zjisteni _config_collection_transform_i do POLE a naplneni promenne _complete_categories boolean[]
		------------------------------------------------------------------------------------------------------
		FOR i IN 1..array_length(_config_ids,1)
		LOOP
			-------------------------------------------------
			-------------------------------------------------
			-- proces zjisteni promenne _config_collection_tranform_i:
			IF _complete_categories_exists[i] = TRUE
			THEN			
				-- proces nalezeni transformovane config_collection [TRANSFORMACE]
				SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
				WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _config_ids[i])
				AND tcc.ref_id_layer_points = _ref_id_layer_points
				INTO _config_collection_transform_i;

			ELSE
				SELECT tcc.aggregated FROM @extschema@.t_config_collection AS tcc
				WHERE tcc.id =	(
						SELECT tc.config_collection
						FROM @extschema@.t_config AS tc
						WHERE tc.id = _config
						)
				INTO _aggregated;

				IF _aggregated = TRUE
				THEN
					-- proces nalezeni transformovane config_collection
					SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
					WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _config_ids[i])
					AND tcc.ref_id_layer_points = _ref_id_layer_points
					INTO _config_collection_transform_i;
				ELSE
					_config_collection_transform_i := _config_collection;
				END IF;
			END IF;
			-------------------------------------------------
			-------------------------------------------------
			-- proces sestaveni _config_collection_transform
			IF i = 1
			THEN
				_config_collection_transform := array[_config_collection_transform_i];
			ELSE
				_config_collection_transform := _config_collection_transform || array[_config_collection_transform_i];
			END IF;
			
		END LOOP;
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------

		------------------------------------------------------------------------------------------------------
		-- CYKLUS KONTROLY PROVEDITELNOSTI --
		------------------------------------------------------------------------------------------------------
		FOR i IN 1..array_length(_config_ids,1)	-- pole _config_ids je spojeni complete/categories, pocet prvku odpovida poctu prvku v _config_collection_transform i _complete_categories
		LOOP										
			SELECT count(t.gid)
			FROM	(
				SELECT DISTINCT tad.gid FROM @extschema@.t_auxiliary_data AS tad
				WHERE tad.config_collection = _config_collection_transform[i]
				AND tad.config = _config_ids[i]
				AND (tad.ext_version >= _ext_version_valid_from AND tad.ext_version <= _ext_version_valid_until)
				AND (tad.gid >= _gid_start AND tad.gid <= _gid_end)
				) AS t
			INTO
				_check_count_config_ids_i;

			IF _check_count_config_ids_i = 0
			THEN
				RAISE EXCEPTION 'Chyba 34: fn_get_aux_data_app: V tabulce t_auxiliary_data pro [nektere complete nebo categories] = % a pro config_collection = % schazi data!',_config_ids[i],_config_collection_transform[i];
			END IF;

			IF i = 1
			THEN
				_check_count_config_ids := array[_check_count_config_ids_i];
			ELSE
				_check_count_config_ids := _check_count_config_ids || array[_check_count_config_ids_i];
			END IF;
			
		END LOOP;
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------

		------------------------------------------------------------------------------------------------------
		-- kontrola stejneho poctu bodu u complete a categories
		------------------------------------------------------------------------------------------------------
		IF	(
			SELECT count(tt.ccci) IS DISTINCT FROM 1
			FROM (SELECT DISTINCT t.ccci FROM (SELECT unnest(_check_count_config_ids) AS ccci) AS t) AS tt
			)
		THEN
			RAISE EXCEPTION 'Chyba 35: fn_get_aux_data_app: Pocty bodu v tabulce t_auxiliary_data se pro [nektere complete nebo nektere categories] neshoduji! Jde o vstupni konfiguraci config_collection = % a config = %.',_config_collection,_config;
		END IF;
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------

		------------------------------------------------------------------------------------------------------
		-- CYKLUS KONTROLY PROVEDITELNOSTI => cast kontroly stejnych gid
		------------------------------------------------------------------------------------------------------
		IF array_length(_config_ids,1) > 1
		THEN
			-- kontrola stejnych gid pro _config_ids
			FOR i IN 1..(array_length(_config_ids,1) - 1)
			LOOP
				WITH
				w1a AS	(
					SELECT
						tad1.*,
						max(tad1.est_date) OVER (PARTITION BY tad1.gid, tad1.config_collection, tad1.config) as max_est_date
					FROM @extschema@.t_auxiliary_data AS tad1
					WHERE tad1.config_collection = _config_collection_transform[1]
					AND tad1.config = _config_ids[1]
					AND (tad1.ext_version >= _ext_version_valid_from AND tad1.ext_version <= _ext_version_valid_until)
					AND (tad1.gid >= _gid_start AND tad1.gid <= _gid_end)
					),
				w1 AS	(
					SELECT w1a.gid FROM w1a WHERE w1a.est_date = w1a.max_est_date				
					),					
				-------------------------------------------------------------------
				w2a AS	(
					SELECT
						tad2.*,
						max(tad2.est_date) OVER (PARTITION BY tad2.gid, tad2.config_collection, tad2.config) as max_est_date 
					FROM @extschema@.t_auxiliary_data AS tad2
					WHERE tad2.config_collection = _config_collection_transform[i+1]
					AND tad2.config = _config_ids[i+1]
					AND (tad2.ext_version >= _ext_version_valid_from AND tad2.ext_version <= _ext_version_valid_until)
					AND (tad2.gid >= _gid_start AND tad2.gid <= _gid_end)
					),
				w2 AS	(
					SELECT w2a.gid FROM w2a WHERE w2a.est_date = w2a.max_est_date
					),
				-------------------------------------------------------------------
				w3 AS	(SELECT w1.gid FROM w1 EXCEPT SELECT w2.gid FROM w2),
				w4 AS	(SELECT w2.gid FROM w2 EXCEPT SELECT w1.gid FROM w1),
				w5 AS	(SELECT w3.gid FROM w3 UNION ALL SELECT w4.gid FROM w4)
				SELECT
					count(w5.gid) FROM w5
				INTO
					_check_gid_i;

				IF _check_gid_i IS DISTINCT FROM 0
				THEN
					RAISE EXCEPTION 'Chyba 36: fn_get_aux_data_app: U konfigurace config_collection = % a config = % se u kontroly proveditelnosti (cast shoda identifikace bodu indent_point) v tabulce t_auxiliary_data u nektereho complete nebo u nektereho categories neshoduji identifikace bodu gid!',_config_collection, _config;
				END IF;
			END LOOP;
		END IF;
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------

		------------------------------------------------------------------------------------------------------------------------------
		-- proces rozhozeni pole _config_ids a pole _config_collection_transform zpetne na cast complete a categories
		------------------------------------------------------------------------------------------------------------------------------
		FOR i IN 1..array_length(_complete_categories_exists,1)
		LOOP
			IF _complete_categories_exists[i] = TRUE	-- complete
			THEN
				IF i = 1
				THEN
					_config_ids_4_complete_array := array[_config_ids[i]];
					_config_collection_4_complete_array := array[_config_collection_transform[i]];
				ELSE
					_config_ids_4_complete_array := _config_ids_4_complete_array || array[_config_ids[i]];
					_config_collection_4_complete_array := _config_collection_4_complete_array || array[_config_collection_transform[i]];
				END IF;
			ELSE						-- categories
				IF i = 1
				THEN
					_config_ids_4_categories_array := array[_config_ids[i]];
					_config_collection_4_categories_array := array[_config_collection_transform[i]];
				ELSE
					_config_ids_4_categories_array := _config_ids_4_categories_array || array[_config_ids[i]];
					_config_collection_4_categories_array := _config_collection_4_categories_array || array[_config_collection_transform[i]];
				END IF;
			END IF;
		END LOOP;
		---------------------------------------------------------------
		-- kontrola ze delka pole _config_ids_4_complete_array odpovida _complete_length
		-- kontrola ze delka pole _config_collection_4_complete_array odpovida _complete_length
		IF _config_collection_4_complete_array IS NOT NULL
		THEN
			IF array_length(_config_ids_4_complete_array,1) != _complete_length
			THEN
				RAISE EXCEPTION 'Chyba 37: fn_get_aux_data_app: Pocet prvku v interni promenne _config_ids_4_complete_array neodpovida interni promenne _complete_length!';
			END IF;
		
			IF array_length(_config_collection_4_complete_array,1) != _complete_length
			THEN
				RAISE EXCEPTION 'Chyba 38 fn_get_aux_data_app: Pocet prvku v interni promenne _config_collection_4_complete_array neodpovida interni promenne _complete_length!';
			END IF;
		ELSE
			_config_collection_4_complete_array := NULL::integer[];
		END IF;
		---------------------------------------------------------------
		-- kontrola ze delka pole _config_ids_4_categories_array odpovida _categories_length
		-- kontrola ze delka pole _config_collection_4_categories_array odpovida _categories_length
		IF array_length(_config_ids_4_categories_array,1) != _categories_length
		THEN
			RAISE EXCEPTION 'Chyba 39: fn_get_aux_data_app: Pocet prvku v interni promenne _config_ids_4_categories_array neodpovida interni promenne _categories_length!';
		END IF;
			
		IF array_length(_config_collection_4_categories_array,1) != _categories_length
		THEN
			RAISE EXCEPTION 'Chyba 40: fn_get_aux_data_app: Pocet prvku v interni promenne _config_collection_4_categories_array neodpovida interni promenne _categories_length!';
		END IF;
		---------------------------------------------------------------	
		------------------------------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------------------------------

		
		---------------------------------------------------------------
		-- CONFIG_QUERY 200 --
										
		IF _config_query = 200
		THEN
			-- raise notice 'PRUCHOD CQ 200';

			---------------------------------------------
			-- kontrola, ze CQ 200 je agregace
			SELECT aggregated FROM @extschema@.t_config_collection
			WHERE id = (SELECT ref_id_total FROM @extschema@.t_config_collection WHERE id = _config_collection)
			INTO _aggregated_200;

			IF _aggregated_200 IS DISTINCT FROM TRUE
			THEN
				RAISE EXCEPTION 'Chyba 41: fn_get_aux_data_app: Jde-li o konfiguraci souctu [config_query = 200], pak musi jit vzdy o agregacni skupinu, respektive v konfiguraci musi platit aggregated = TRUE! Jde o vstupni konfiguraci config_collection = % a config = %.',_config_collection,_config;
			END IF;
			---------------------------------------------		

			-- provedeni sumy za categories pro jednotlive body
			_q :=
				'
				WITH
				w2a AS	(
					SELECT
						tad.gid,
						tad.value,
						tad.config_collection,
						tad.config,
						tad.ext_version,
						tad.est_date,
						max(tad.est_date) OVER (PARTITION BY tad.gid, tad.config_collection, tad.config) as max_est_date
					FROM
						@extschema@.t_auxiliary_data AS tad
					WHERE
						tad.config_collection IN (SELECT unnest($10))
					AND
						tad.config IN (SELECT unnest($9))
					AND
						(tad.ext_version >= $14 AND	tad.ext_version <= $15)
					AND
						(tad.gid >= $12 AND tad.gid <= $13)
					),
				w2 AS	(
					SELECT w2a.gid, w2a.value FROM w2a WHERE w2a.est_date = w2a.max_est_date
					),
				-------------------------------------------------------------------						
				w3 AS 	(
					SELECT
						w2.gid,
						sum(w2.value)::double precision AS value
					FROM
						w2
					GROUP
						BY w2.gid
					),
				-------------------------------------------------------------------	
				w4a AS	(
					SELECT
					tad1.*,
					max(tad1.est_date) OVER (PARTITION BY tad1.gid, tad1.config_collection, tad1.config) as max_est_date
					FROM @extschema@.t_auxiliary_data AS tad1
					WHERE tad1.config_collection = $3
					AND tad1.config = $4
					AND (tad1.ext_version >= $14 AND tad1.ext_version <= $15)
					AND (tad1.gid >= $12 AND tad1.gid <= $13)
					),
				w4 AS	(
					SELECT w4a.gid AS gid_except FROM w4a WHERE w4a.est_date = w4a.max_est_date
					),
				-------------------------------------------------------------------					
				w5 AS	(
					SELECT
						$3 AS config_collection,
						$4 AS config,
						w3.gid,
						w3.value,
						$5 AS ext_version,
						CASE WHEN w4.gid_except IS NULL THEN FALSE ELSE TRUE END AS gid_exists
					FROM
						w3 LEFT JOIN w4 ON w3.gid = w4.gid_except
					)
				-------------------------------------------------------------------
				SELECT
					w5.config_collection,
					w5.config,
					w5.gid,
					w5.value,
					w5.ext_version
				FROM
					w5 WHERE w5.gid_exists = FALSE
				';			
		END IF;		
		---------------------------------------------------------------
		-- CONFIG_QUERY 300 --
		IF _config_query = 300
		THEN
			-- raise notice 'PRUCHOD CQ 300';	
			-- kontrola, ze u vsech categories jsou opravdu hodnoty v intervalu 0 az 1
			FOR i IN 1..array_length(_config_ids_4_categories_array,1)
			LOOP		
				SELECT array_agg(t.value ORDER BY t.value)
				FROM	(
					SELECT DISTINCT tad.value::double precision
					FROM @extschema@.t_auxiliary_data AS tad
					WHERE tad.config_collection = _config_collection_4_categories_array[i]
					AND tad.config = _config_ids_4_categories_array[i]
					AND (tad.ext_version >= _ext_version_valid_from AND tad.ext_version <= _ext_version_valid_until)
					AND (tad.gid >= _gid_start AND tad.gid <= _gid_end)
					) AS t
				INTO
					_check_categories_i;

				IF _check_categories_i IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 42 fn_get_aux_data_app: Pro categories = % u konfigurace _config_collection = % a _config = % a _ext_version = % schazi data v tabulce t_auxiliary_data!',_config_ids_4_categories_array[i],_config_collection_4_categories_array[i],_config,_ext_version_current;
				END IF;

				FOR i IN 1..array_length(_check_categories_i,1)
				LOOP
					IF	(
						_check_categories_i[i] < 0.0	OR
						_check_categories_i[i] > 1.0
						)
					THEN
						RAISE EXCEPTION 'Chyba 43: fn_get_aux_data_app: Hodnoty values u categories = % u konfigurace _config_collection = % a _config = % a _ext_version = % nejsou v intervalu <0.0;1.0>!',_config_ids_4_categories_array[i],_config_collection_4_categories_array[i],_config,_ext_version_current;
					END IF;
				END LOOP;
				
			END LOOP;
			-- provedeni doplnku pro jednotlive body
			_q :=
				'
				WITH
				w2a AS	(
					SELECT
						tad.gid,
						round(tad.value::numeric,2) AS value,
						tad.config_collection,
						tad.config,
						tad.ext_version,
						tad.est_date,
						max(tad.est_date) OVER (PARTITION BY tad.gid, tad.config_collection, tad.config) as max_est_date
					FROM
						gisdata_test.t_auxiliary_data AS tad
					WHERE
						tad.config_collection IN (SELECT unnest($10))
					AND
						tad.config IN (SELECT unnest($9))
					AND
						(tad.ext_version >= $14 AND	tad.ext_version <= $15)
					AND
						(tad.gid >= $12 AND tad.gid <= $13)						
					),
				w2 AS	(
					SELECT w2a.* FROM w2a WHERE w2a.est_date = w2a.max_est_date
					),
				-------------------------------------------------------------------
				w3 AS	(
					SELECT
						w2.gid,
						(sum(w2.value))::double precision AS value
					FROM
						w2
					GROUP
						BY w2.gid
					),
				-------------------------------------------------------------------
				w4a AS	(
					SELECT
					tad1.*,
					max(tad1.est_date) OVER (PARTITION BY tad1.gid, tad1.config_collection, tad1.config) as max_est_date
					FROM @extschema@.t_auxiliary_data AS tad1
					WHERE tad1.config_collection = $3
					AND tad1.config = $4
					AND (tad1.ext_version >= $14 AND tad1.ext_version <= $15)
					AND (tad1.gid >= $12 AND tad1.gid <= $13)
					),
				w4 AS	(
					SELECT w4a.gid as gid_except FROM w4a WHERE w4a.est_date = w4a.max_est_date
					),
				-------------------------------------------------------------------
				w5 AS	(			
					SELECT
						$3 AS config_collection,
						$4 AS config,
						w3.gid,
						(1.0::double precision - w3.value) AS value,
						$5 AS ext_version,
						CASE WHEN w4.gid_except IS NULL THEN FALSE ELSE TRUE END AS gid_exists
					FROM
						w3 LEFT JOIN w4 ON w3.gid = w4.gid_except
					)
				-------------------------------------------------------------------
				SELECT
					w5.config_collection,
					w5.config,
					w5.gid,
					w5.value,
					w5.ext_version
				FROM
					w5 WHERE w5.gid_exists = FALSE
				';
		END IF;
		---------------------------------------------------------------
		-- CONFIG_QUERY 400 --
		IF _config_query = 400
		THEN
			-- raise notice 'PRUCHOD CQ 400';		

			IF _complete IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 44: fn_get_aux_data_app: Pro konfiguraci kategorie _config = % nenalezena hodnota complete v tabulce t_config!',_config;
			END IF;

			-- provedeni doplnku pro jednotlive body
			_q :=
				'
				WITH
				-------------------------------------------------------------------
				-- categories
				w2a AS	(
					SELECT
						tad.gid,
						(tad.value)::numeric AS value,
						tad.config_collection,
						tad.config,
						tad.ext_version,
						tad.est_date,
						max(tad.est_date) OVER (PARTITION BY tad.gid, tad.config_collection, tad.config) as max_est_date
					FROM
						@extschema@.t_auxiliary_data AS tad
					WHERE
						tad.config_collection IN (SELECT unnest($10))
					AND
						tad.config IN (SELECT unnest($9))
					AND
						(tad.ext_version >= $14 AND	tad.ext_version <= $15)
					AND
						(tad.gid >= $12 AND tad.gid <= $13)
					),
				w2 AS	(
					SELECT w2a.gid, w2a.value FROM w2a WHERE w2a.est_date = w2a.max_est_date
					),
				-------------------------------------------------------------------
				-- complete
				w4a AS	(
					SELECT
						tad.gid,
						(tad.value)::numeric AS value,
						tad.config_collection,
						tad.config,
						tad.ext_version,
						tad.est_date,
						max(tad.est_date) OVER (PARTITION BY tad.gid, tad.config_collection, tad.config) as max_est_date
					FROM
						@extschema@.t_auxiliary_data AS tad
					WHERE
						tad.config_collection IN (SELECT unnest($8))
					AND
						tad.config IN (SELECT unnest($7))
					AND
						(tad.ext_version >= $14 AND	tad.ext_version <= $15)
					AND
						(tad.gid >= $12 AND tad.gid <= $13)
					),
				w4 AS	(
					SELECT w4a.gid, w4a.value FROM w4a WHERE w4a.est_date = w4a.max_est_date					
					),		
				-------------------------------------------------------------------
				w5 AS	(
					SELECT
						w2.gid,
						sum(w2.value) AS value
					FROM
						w2
					GROUP
						BY w2.gid
					),
				-------------------------------------------------------------------
				w6 AS	(
					SELECT
						w4.gid,
						sum(w4.value) AS value
					FROM
						w4
					GROUP
						BY w4.gid					
					),
				-------------------------------------------------------------------
				w7a AS	(
					SELECT
					tad1.*,
					max(tad1.est_date) OVER (PARTITION BY tad1.gid, tad1.config_collection, tad1.config) as max_est_date
					FROM @extschema@.t_auxiliary_data AS tad1
					WHERE tad1.config_collection = $3
					AND tad1.config = $4
					AND (tad1.ext_version >= $14 AND tad1.ext_version <= $15)
					AND (tad1.gid >= $12 AND tad1.gid <= $13)
					),
				w7 AS	(
					SELECT w7a.gid AS gid_except FROM w7a WHERE w7a.est_date = w7a.max_est_date
					),					
				-----------------------------------------------
				w8 AS	(
					SELECT
						$3 AS config_collection,
						$4 AS config,
						w6.gid,
						((w6.value - w5.value))::double precision AS value,
						$5 AS ext_version,
						CASE WHEN w7.gid_except IS NULL THEN FALSE ELSE TRUE END AS gid_exists
					FROM
							w6
					INNER JOIN	w5 ON w6.gid = w5.gid
					LEFT  JOIN	w7 ON w6.gid = w7.gid_except
					)
				-----------------------------------------------
				SELECT
					w8.config_collection,
					w8.config,
					w8.gid,
					w8.value,
					w8.ext_version
				FROM
					w8 WHERE w8.gid_exists = FALSE
				';	
		
		END IF;
		---------------------------------------------------------------	
	ELSE
		RAISE EXCEPTION 'Chyba 45: fn_get_aux_data_app: Neznama hodnota _config_query = %!',_config_query;
	END CASE;
	-------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||''
	USING
		_ids4comb[1],				-- $1
		_ids4comb[2],				-- $2
		_config_collection,			-- $3
		_config,				-- $4
		_ext_version_current,			-- $5
		_config_collection_transform_comb[1],	-- $6
		_config_ids_4_complete_array,		-- $7
		_config_collection_4_complete_array,	-- $8
		_config_ids_4_categories_array,		-- $9
		_config_collection_4_categories_array,	-- $10
		_config_collection_transform_comb[2],	-- $11
		_gid_start,				-- $12
		_gid_end,				-- $13
		_ext_version_valid_from,		-- $14
		_ext_version_valid_until;		-- $15		
	-------------------------------------------------------------------------------------------
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


ALTER FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean,integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean,integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean,integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean,integer,integer) IS
'Funkce vraci data pro insert do tabulky t_auxiliary_data.';