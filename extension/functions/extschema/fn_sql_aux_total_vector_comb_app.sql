--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------;
--	fn_sql_aux_total_vector_comb_app
--------------------------------------------------------------------------------;

DROP FUNCTION IF EXISTS @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
(
	config_id			integer,
	schema_name			character varying,
	table_name			character varying,
	column_name			character varying,
	condition			character varying,
	unit				double precision,
	schema_name_1			character varying,
	table_name_1			character varying,
	column_name_1			character varying,
	band_1				integer,
	reclass_1			integer,
	condition_1			character varying,
	unit_1				double precision,
	gid				character varying
)
RETURNS text
LANGUAGE plpgsql
IMMUTABLE
SECURITY INVOKER
AS
$$
DECLARE
	reclass_text		text;
	result			text;
BEGIN
	-----------------------------------------------------------------------------------
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_total_vector_comb_app: fn_sql_aux_total_vector_comb_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_total_vector_comb_app: fn_sql_aux_total_vector_comb_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_name
	IF (column_name IS NULL) OR (trim(column_name) = '')
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_total_vector_comb_app: fn_sql_aux_total_vector_comb_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- condition
	-- povoleno NULL
	IF (condition IS NULL) OR (trim(condition) = '')
	THEN
		condition := NULL;
	END IF;

	-- unit
	IF (unit IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_total_vector_comb_app: Hodnota parametru unit nesmí být NULL.';
	END IF;

	-- schema_name_1
	IF ((schema_name_1 IS NULL) OR (trim(schema_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_total_vector_comb_app: Hodnota parametru schema_name_1 nesmí být NULL.';
	END IF;

	-- table_name_1
	IF ((table_name_1 IS NULL) OR (trim(table_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_total_vector_comb_app: Hodnota parametru table_name_1 nesmí být NULL.';
	END IF;

	-- column_name_1
	IF ((column_name_1 IS NULL) OR (trim(column_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_sql_aux_total_vector_comb_app: Hodnota parametru column_name_1 nesmí být NULL.';
	END IF;

	-- band_1
	IF (band_1 IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_sql_aux_total_vector_comb_app: Hodnota parametru band_1 nesmí být NULL.';
	END IF;

	-- condition_1 [povoleno NULL]
	IF ((condition_1 IS NULL) OR (trim(condition_1) = ''))
	THEN
		condition_1 := NULL;
	END IF;

	-- unit_1
	IF (unit_1 IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_sql_aux_total_vector_comb_app: Hodnota parametru unit_1 nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro vypocet uhrnu
	result :=
	'
	WITH
	--------------------------------------------------------------- 
	w_ku_olil AS materialized

			(-- propojeni tabulek olilu a katastru na zaklade jejich pruniku, vektorová operace
			SELECT
				t1.gid, 
				t1.estimation_cell,
				t1.geom as geom_cell,
				t2.#COLUMN_NAME#,
				ST_Intersection(t1.geom, t2.#COLUMN_NAME#) as intersect_ku_olil  -- vysledna geometrie protnuti geometrie GIDu a OLILu
			FROM
				@extschema@.f_a_cell AS t1
			LEFT JOIN
				#SCHEMA_NAME#.#TABLE_NAME# AS t2
			ON
				ST_Intersects(t2.#COLUMN_NAME#,t1.geom) 
			AND 
				#CONDITION#
			WHERE 
				t1.gid = #GID#
			)
	---------------------------------------------------------------
	---------------------------------------------------------------
	,w_ndsm_olil_katastr_0 AS	(
					SELECT
						t1.*,
						t2.*,
						CASE
							WHEN t2.#COLUMN_NAME_1# IS NULL THEN t2.#COLUMN_NAME_1#
							ELSE #RECLASS_1#
						END
							AS rast_reclass
					FROM
						w_ku_olil AS t1
					LEFT
					JOIN	#SCHEMA_NAME_1#.#TABLE_NAME_1# AS t2

					ON	t1.intersect_ku_olil && ST_Convexhull(t2.#COLUMN_NAME_1#)

					AND	ST_Intersects(t1.intersect_ku_olil,ST_Convexhull(t2.#COLUMN_NAME_1#))
					)
	,w_ndsm_olil_katastr AS		(
					SELECT
						gid,
						estimation_cell,
						rast_reclass,
						intersect_ku_olil,
						CASE WHEN rast_reclass IS NULL THEN false ELSE true END AS ident_null
					FROM
						w_ndsm_olil_katastr_0 as A
					WHERE
						#CONDITION_1#
					AND
						(
						ST_GeometryType(A.intersect_ku_olil) = ''ST_Polygon'' OR
						ST_GeometryType(A.intersect_ku_olil) = ''ST_MultiPolygon''
						)
					)
	---------------------------------------------------------------
	---------------------------------------------------------------
	,w_covers AS materialized
			(
			SELECT
				gid, 
				estimation_cell, 
				rast_reclass, 
				intersect_ku_olil,
				ST_Covers(intersect_ku_olil, ST_Convexhull(rast_reclass)) AS covers
			FROM
				w_ndsm_olil_katastr
			WHERE
				ident_null
			)
	---------------------------------------------------------------
	,w_uhrn AS	(
			SELECT
				estimation_cell,
				(ST_ValueCount(rast_reclass, #BAND_1#, true)) AS val_count,
				(ST_PixelWidth(rast_reclass) * ST_PixelHeight(rast_reclass))*#UNIT_1# AS pixarea
			FROM
				w_covers
			WHERE
				covers = true
			)
	---------------------------------------------------------------
	,w_uhrn_not AS	(
			SELECT
				estimation_cell,
				ST_Intersection(intersect_ku_olil,rast_reclass) AS vector_intersect_record
			FROM
				w_covers
			WHERE
				covers = false
			)
	---------------------------------------------------------------
	,w_sum AS	(
			SELECT
				SUM(((ST_Area((vector_intersect_record).geom))*#UNIT#)*((vector_intersect_record).val)) AS sum_part
			FROM 
				w_uhrn_not
			-------------------
			UNION ALL
			-------------------
			SELECT 
				SUM((val_count).value*(val_count).count*pixarea) AS sum_part
			FROM 
				w_uhrn 
			-------------------			
			UNION ALL
			-------------------
			SELECT
				0.0 AS sum_part
			FROM
				w_ndsm_olil_katastr
			WHERE
				ident_null = false
			)
	---------------------------------------------------------------
	SELECT 
		sum(coalesce((sum_part),0)) AS aux_total_#CONFIG_ID#_#GID#
	FROM 
		w_sum;
	';
	-----------------------------------------------------------------------------------
	-- nastaveni pripadne reklasifikace
	IF (reclass_1 IS NULL)
	THEN
		reclass_text := 't2.#COLUMN_NAME_1#';
	ELSE
		reclass_text := '@extschema@.fn_get_reclass_app(t2.#COLUMN_NAME_1#,#BAND_1#,ST_BandPixelType(t2.#COLUMN_NAME_1#,#BAND_1#),#RECLASS_VALUE_1#)';
	END IF;
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	result := replace(result, '#RECLASS_1#', reclass_text);
	result := replace(result, '#CONFIG_ID#', config_id::character varying);
	result := replace(result, '#SCHEMA_NAME#', schema_name);
	result := replace(result, '#TABLE_NAME#', table_name);
	result := replace(result, '#COLUMN_NAME#', column_name);
	result := replace(result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	result := replace(result, '#UNIT#', unit::character varying);
	result := replace(result, '#SCHEMA_NAME_1#', schema_name_1);
	result := replace(result, '#TABLE_NAME_1#', table_name_1);
	result := replace(result, '#COLUMN_NAME_1#', column_name_1);
	result := replace(result, '#BAND_1#', band_1::character varying);
	result := replace(result, '#CONDITION_1#', coalesce(condition_1::character varying, 'TRUE'));
	result := replace(result, '#UNIT_1#', unit_1::character varying);
	result := replace(result, '#GID#', gid);

	IF (reclass_1 IS NOT NULL)
	THEN
		result := replace(result, '#RECLASS_VALUE_1#', reclass_1::character varying);
	END IF;
	-----------------------------------------------------------------------------------
	RETURN result;
	-----------------------------------------------------------------------------------
END;
$$;

--------------------------------------------------------------------------------;

ALTER FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
OWNER TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO app_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_total_vector_comb_app
	(
	integer,
	character varying, character varying, character varying, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
IS
'Funkce vrací SQL textový řetězec pro výpočet úhrnu pomocné proměnné z rasterové kategorie v rámci vektorové vrstvy.';

--------------------------------------------------------------------------------;

