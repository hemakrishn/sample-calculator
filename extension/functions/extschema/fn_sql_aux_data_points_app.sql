--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_sql_aux_data_points_app(integer,integer,integer,integer,integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_data_points_app
(
	_ref_id_layer_points		integer,
	_gid_start			integer,
	_gid_end			integer,
	_config_collection		integer,
	_config				integer
)
RETURNS text AS
$BODY$
DECLARE
	_ext_version_label_system	text;
	_ext_version_current		integer;
	_condition4plot			text;
	_condition4aux			text;
	_result				text;
	_check				integer;

	-- NEW --
	_config_function				integer;
	_ext_version_valid_from				integer;
	_ext_version_valid_until			integer;
	_ext_version_valid_from_label			character varying;
	_ext_version_valid_until_label			character varying;
BEGIN
	-----------------------------------------------------------------------------------
	-- config_pts
	IF (_ref_id_layer_points IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_data_points_app: Hodnota parametru _ref_id_layer_points nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- gid_start
	IF (_gid_start IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_data_points_app: Hodnota parametru _gid_start nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- gid_end
	IF (_gid_end IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_data_points_app: Hodnota parametru _gid_end nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- config_collection
	IF (_config_collection IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_data_points_app: Hodnota parametru config_collection nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- config
	IF (_config IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_data_points_app: Hodnota parametru config nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- proces ziskani aktualni extenze nfiesta_gisdata
	SELECT extversion FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_data_points_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_sql_aux_data_points_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;
	-----------------------------------------------------------------------------------	
	-- condition4plot
	_condition4plot := concat('(gid >= ',_gid_start,' AND gid <= ',_gid_end,')');
	-----------------------------------------------------------------------------------
	-- condition4aux
	_condition4aux := concat('(gid >= ',_gid_start,' AND gid <= ',_gid_end,')');
	-----------------------------------------------------------------------------------
	-- NEW --
	SELECT config_function FROM @extschema@.t_config_collection
	WHERE id = (SELECT ref_id_total FROM @extschema@.t_config_collection WHERE id = _config_collection)
	INTO _config_function;

	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_sql_aux_data_points_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] nenalezena hodnota _config_function u teto konfigurace!',(SELECT ref_id_total FROM @extschema@.t_config_collection WHERE id = _config_collection);
	END IF;

	IF NOT(_config_function = ANY(array[100,200]))
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_sql_aux_data_points_app: Interni promenna _config_function = % musi byt hodnota 100 nebo 200!',_config_function;
	END IF;

	-- zjisteni ext_version_valid_from a ext_version_valid_until
	SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
	FROM @extschema@.cm_ext_config_function AS cmcf
	WHERE cmcf.active = TRUE
	AND cmcf.config_function = _config_function
	INTO
		_ext_version_valid_from,
		_ext_version_valid_until;

	IF (_ext_version_valid_from IS NULL OR _ext_version_valid_until IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 10: fn_sql_aux_data_points_app: Pro interni promennou _config_function = % nenalezeno ext_version_valid_from nebo ext_version_valid_until v tabulce cm_ext_config_function!',_config_function;
	END IF;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_from  INTO _ext_version_valid_from_label;
	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_until INTO _ext_version_valid_until_label;
	-----------------------------------------------------------------------------------
	-----------------------------------------------------------------------------------
	_result :=
		'
		SELECT
			t3.gid,
			t3.geom
		FROM
			(
			SELECT
				t1.gid,
				t1.geom,
				t2.gid AS ident_point
			FROM
				(
				SELECT
					gid,
					geom
				FROM
					@extschema@.f_p_plot
				WHERE
					config_collection = #CONFIG_COLLECTION_PLOT#
				AND
					#CONDITION4PLOT#
				) AS t1
			LEFT
			JOIN	(
				WITH
				w_1 AS	(
					SELECT
						tad.gid,
						tad.config_collection,
						tad.config,
						tad.ext_version,
						tad.est_date
					FROM @extschema@.t_auxiliary_data AS tad
					WHERE tad.config_collection = #CONFIG_COLLECTION_AUX#
					AND tad.config = #CONFIG#
					AND tad.ext_version >= #EXT_VERSION_VALID_FROM#
					AND tad.ext_version <= #EXT_VERSION_VALID_UNTIL#
					AND #CONDITION4AUX#
					),
				w_2 AS	(
					SELECT w_1.gid, w_1.config_collection, w_1.config, max(w_1.ext_version) AS max_ext_version
					FROM w_1 GROUP BY w_1.gid, w_1.config_collection, w_1.config
					),
				w_3 AS	(
					SELECT w_1.* FROM w_1 INNER JOIN w_2
					ON w_1.gid = w_2.gid AND w_1.config_collection = w_2.config_collection AND w_1.ext_version = w_2.ext_version
					)
				w_4 AS	(
					SELECT w_3.gid, w_3.config_collection, w_3.config, w_3.ext_version, max(w_3.est_date) AS max_est_date
					FROM w_3 GROUP BY  w_3.gid, w_3.config_collection, w_3.config, w_3.ext_version
					)
				w_5 AS	(
					SELECT w_3.* FROM w_3 INNER JOIN w_4
					ON w_3.gid = w_4.gid AND w_3.config_collection = w_4.config_collection AND w_3.config = w_4.config  AND w_3.ext_version = w_4.ext_version AND w_3.est_date = w_4.max_est_date
					)
				SELECT w_5.gid FROM w_5
				) AS t2
			ON
				t1.gid = t2.gid
			) AS t3
		WHERE
			t3.ident_point IS NULL
		';

	-- nahrazeni promennych casti v dotazu
	_result := replace(_result, '#CONFIG_COLLECTION_PLOT#', concat(_ref_id_layer_points));
	_result := replace(_result, '#CONDITION4PLOT#', _condition4plot);
	_result := replace(_result, '#CONFIG_COLLECTION_AUX#', concat(_config_collection));
	_result := replace(_result, '#CONFIG#', concat(_config));
	_result := replace(_result, '#EXT_VERSION_CURRENT#', concat(_ext_version_current));
	_result := replace(_result, '#CONDITION4AUX#', _condition4aux);
	_result := replace(_result, '#EXT_VERSION_VALID_FROM#', concat(_ext_version_valid_from));
	_result := replace(_result, '#EXT_VERSION_VALID_UNTIL#', concat(_ext_version_valid_until));
	-----------------------------------------------------------------------------------
	-- kontrola ze _result vrati alespon nejaky bod pro protinani
	EXECUTE 'WITH w_points AS ('||_result||') SELECT (count(*))::integer FROM w_points'
	INTO _check;
	-----------------------------------------------------------------------------------
	IF (_check = 0 OR _check IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 11: fn_sql_aux_data_points_app: Pro bodove protinani nenalezeny patricne body.';
	END IF;
	-----------------------------------------------------------------------------------
	RETURN _result;
	-----------------------------------------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;

ALTER FUNCTION @extschema@.fn_sql_aux_data_points_app(integer,integer,integer,integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_points_app(integer,integer,integer,integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_points_app(integer,integer,integer,integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_data_points_app(integer,integer,integer,integer,integer)
IS 'Funkce vrací SQL textový řetězec pro ziskani seznamu bodu pro bodove protinani.';