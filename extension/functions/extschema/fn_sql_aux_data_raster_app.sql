--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision, integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_data_raster_app
(
	config_collection		integer,
	config				integer,
	schema_name			character varying,
	table_name			character varying,
	column_ident			character varying,
	column_name			character varying,
	band				integer,
	reclass				integer,
	condition			character varying,
	unit				double precision,
	gid_start			integer,
	gid_end				integer
)
RETURNS text
LANGUAGE plpgsql
IMMUTABLE
SECURITY INVOKER
AS
$$
DECLARE
	_ref_id_layer_points	integer;
	_q			text;
	_column_text		text;
	_result			text;
BEGIN
	-----------------------------------------------------------------------------------
	-- config_collection
	IF config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_data_raster_app: Hodnota parametru config_collection nesmí být NULL.';
	END IF;

	-- config
	IF config IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_data_raster_app: Hodnota parametru config nesmí být NULL.';
	END IF;
	
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_data_raster_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_data_raster_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_ident
	IF ((column_ident IS NULL) OR (trim(column_ident) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_data_raster_app: Hodnota parametru column_ident nesmí být NULL.';
	END IF;

	-- column_name
	IF ((column_name IS NULL) OR (trim(column_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_data_raster_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- band
	IF (band IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_sql_aux_data_raster_app: Hodnota parametru band nesmí být NULL.';
	END IF;

	-- condition [povoleno NULL]
	IF ((condition IS NULL) OR (trim(condition) = ''))
	THEN
		condition := NULL;
	END IF;

	-- unit
	IF (unit IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_sql_aux_data_raster_app: Hodnota parametru unit nesmí být NULL.';
	END IF;

	-- gid_start 
	IF (gid_start IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_sql_aux_data_raster_app: Hodnota parametru gid_start nesmí být NULL.';
	END IF;

	-- gid_end 
	IF (gid_end IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 10: fn_sql_aux_data_raster_app: Hodnota parametru gid_end nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- zjisteni reference pro bodovou vrstvu
	SELECT tcc.ref_id_layer_points FROM @extschema@.t_config_collection AS tcc
	WHERE tcc.id = config_collection
	INTO _ref_id_layer_points;
	-----------------------------------------------------------------------------------
	IF _ref_id_layer_points IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 11: fn_sql_aux_data_raster_app: Nenalezena hodnota ref_id_layer_points pro config_collection = %.',config_collection;
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni vnoreneho dotazu pro vrstvu bodu
	_q := (SELECT * FROM @extschema@.fn_sql_aux_data_points_app(_ref_id_layer_points,gid_start,gid_end,config_collection,config));
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro protnuti bodu z vrstvou
	_result :=
		'
		WITH                                                                    
		w_point_geom AS	('||_q||'),
		-------------------------------------------------------------------
		w_intersects AS
				(
				SELECT
					t1.gid,
					CASE
					WHEN t2.#COLUMN_NAME# IS NULL THEN 0.0
					ELSE #VALUE#
					END AS value
				FROM
					w_point_geom AS t1					
				LEFT JOIN
					#SCHEMA_NAME#.#TABLE_NAME# AS t2
				ON
					ST_Convexhull(t2.#COLUMN_NAME#) && t1.geom
				AND
					ST_Intersects(ST_Convexhull(t2.#COLUMN_NAME#),t1.geom)
				AND
					#CONDITION#
				)
		-------------------------------------------------------------------			
		SELECT
			#CONFIG_COLLECTION# AS config_collection,
			#CONFIG# AS config,
			gid,
			coalesce((value::double precision * #UNIT#),0.0::double precision) AS value,
			$5 AS ext_version
		FROM
			w_intersects;
		';
	-----------------------------------------------------------------------------------
	-- nastaveni pripadne reklasifikace
	IF (reclass IS NULL)
	THEN
		_column_text := 't2.#COLUMN_NAME#';
	ELSE
		_column_text := '@extschema@.fn_get_reclass_app(t2.#COLUMN_NAME#,#BAND#,ST_BandPixelType(t2.#COLUMN_NAME#,#BAND#),#RECLASS_VALUE#)';
	END IF;
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	_result := replace(_result, '#VALUE#', concat('ST_Value(',_column_text,',#BAND#,t1.geom)'));
	_result := replace(_result, '#CONFIG_COLLECTION#', config_collection::character varying);
	_result := replace(_result, '#CONFIG#', config::character varying);
	_result := replace(_result, '#SCHEMA_NAME#', schema_name);
	_result := replace(_result, '#TABLE_NAME#', table_name);
	_result := replace(_result, '#COLUMN_NAME#', column_name);
	_result := replace(_result, '#BAND#', band::character varying);
	_result := replace(_result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	_result := replace(_result, '#UNIT#', unit::character varying);

	IF (reclass IS NOT NULL)
	THEN
		_result := replace(_result, '#RECLASS_VALUE#', reclass::character varying);
	END IF;
	-----------------------------------------------------------------------------------
	RETURN _result;
	-----------------------------------------------------------------------------------
END;
$$;

ALTER FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision, integer, integer)
OWNER TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision, integer, integer)
TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision, integer, integer)
TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision, integer, integer)
IS
'Funkce vrací SQL textový řetězec pro ziskani pomocné proměnné z rasterové vrstvy protinanim bodu.';