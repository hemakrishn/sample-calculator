--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- Function: @extschema@.fn_aux_total_before_insert_app()

-- DROP FUNCTION @extschema@.fn_aux_total_before_insert_app();

CREATE OR REPLACE FUNCTION @extschema@.fn_aux_total_before_insert_app()
  RETURNS trigger AS
$BODY$
	DECLARE
		_ext_version_label_system	text;
		_ext_version_id_system		integer;
		_ext_version_label		character varying;
	BEGIN
		-------------------------------------------------------------------------
		-- cast SYSTEMOVA --
		-------------------------------------------------------------------------
		SELECT extversion FROM pg_extension
		WHERE extname = 'nfiesta_gisdata'
		INTO _ext_version_label_system;
			
		IF _ext_version_label_system IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 01: fn_aux_total_before_insert_app: V systemove tabulce pg_extension nenalezena zadna verze extenze nfiesta_gisdata!';
		END IF;
	
		SELECT id FROM @extschema@.c_ext_version
		WHERE label = _ext_version_label_system
		INTO _ext_version_id_system;
	
		IF _ext_version_id_system IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 02: fn_aux_total_before_insert_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
		END IF;
		-------------------------------------------------------------------------
		-- cast INSERTOVANA
		-------------------------------------------------------------------------
		SELECT label FROM @extschema@.c_ext_version WHERE id = NEW.ext_version
		INTO _ext_version_label;

		IF _ext_version_label IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 03: fn_aux_total_before_insert_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici insertovane verzi % extenze nfiesta_gisdata!',_ext_version_label;
		END IF;
		-------------------------------------------------------------------------
		-- cast KONTROLY
		-------------------------------------------------------------------------
		IF NEW.ext_version > _ext_version_id_system
		THEN
			RAISE EXCEPTION 'Chyba 04: fn_aux_total_before_insert_app: Insertovana verze extenze (ext_version = %) nesmi byt novejsi, nez je prave aktualni systemova verze (%) extenze nfiesta_gisdata!',_ext_version_label,_ext_version_label_system;
		END IF;
		-------------------------------------------------------------------------
		RETURN NEW;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION @extschema@.fn_aux_total_before_insert_app() OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_aux_total_before_insert_app() TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_aux_total_before_insert_app() TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_aux_total_before_insert_app() TO public;

COMMENT ON FUNCTION @extschema@.fn_aux_total_before_insert_app() IS
'Funkce provádí kontrolu importovaného záznamu verze extenze (ext_version). Spouští se triggerem before insert tabulky t_aux_total.';
