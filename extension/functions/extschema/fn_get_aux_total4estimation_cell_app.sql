--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP function @extschema@.fn_get_aux_total4estimation_cell_app (integer,integer,integer,boolean)

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app
	(
	_config_id			integer,
	_estimation_cell		integer,
	_gid				integer,
	_recount			boolean DEFAULT FALSE
	)
RETURNS TABLE
	(
        estimation_cell			integer,
        config				integer, 
        aux_total			double precision,
        cell				integer,
        ext_version			integer
        )
AS
$BODY$
DECLARE
	_estimation_cell_collection		integer;
	_estimation_cell_collection_lowest	integer;
	_check_estimation_cell			boolean;
	_check_pocet				integer;
	_gids4estimation_cell			integer[];
	_gids4estimation_cell_lowest		integer[];
	_gids_in_t_aux_total_check		integer;
	_aux_total				double precision;
	_q					text;

	_ext_version_label_system		text;
	_ext_version_current			integer;

	-- NEW --
	_config_function				integer;
	_ext_version_valid_from				integer;
	_ext_version_valid_until			integer;
	_ext_version_valid_from_label			character varying;
	_ext_version_valid_until_label			character varying;		
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total4estimation_cell_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total4estimation_cell_app: Vstupni argument _estimation_cell nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_total4estimation_cell_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;

	IF _gid != 0
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_total4estimation_cell_app: Vstupni argument _gid musi byt hodnota 0!';
	END IF;

	IF _recount IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_aux_total4estimation_cell_app: Vstupni argument _recount nesmi byt NULL!';
	END IF;	
	-------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------
	-- proces ziskani aktualni systemove extenze nfiesta_gisdata
	
	SELECT extversion FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_aux_total4estimation_cell_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_aux_total4estimation_cell_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;		
	-------------------------------------------------------------------------------------------
	-- NEW --
	-- zjisteni config_function pro zjistene _config_collection potazmo pro vstupni _config_id
	SELECT tcc.config_function FROM @extschema@.t_config_collection AS tcc
	WHERE tcc.id = (SELECT config_collection FROM @extschema@.t_config WHERE id = _config_id)
	INTO _config_function;

	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_get_aux_total4estimation_cell_app: Pro vstupni argument _config_id = % nenalezeno config_function v tabulce t_config_collection!',_config_id;
	END IF;

	IF NOT(_config_function = ANY(array[100,200,300,400]))
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_aux_total4estimation_cell_app: Interni promenna _config_function = % musi byt hodnota 100, 200, 300 nebo 400!',_config_function;
	END IF;

	-- zjisteni ext_version_valid_from a ext_version_valid_until
	SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
	FROM @extschema@.cm_ext_config_function AS cmcf
	WHERE cmcf.active = TRUE
	AND cmcf.config_function = _config_function
	INTO
		_ext_version_valid_from,
		_ext_version_valid_until;

	IF (_ext_version_valid_from IS NULL OR _ext_version_valid_until IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 10: fn_get_gids4aux_total_app: Pro interni promennou _config_function = % nenalezeno ext_version_valid_from nebo ext_version_valid_until v tabulce cm_ext_config_function!',_config_function;
	END IF;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_from  INTO _ext_version_valid_from_label;
	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_until INTO _ext_version_valid_until_label;
	-------------------------------------------------------------------------------------------
	-- kontrola, ze estimation_cell:
	-- 1. nesmi byt zakladni stavebni jednotka [ZSJ] => vyjimka plati jen pro viz. bod 2
	-- 2. muze to byt ZSJ ale pocet geometrii, ktery tvori onu ZSJ musi byt vice nez 1
	--------------------------------------------------------------------------------------------
	-- zjisteni do jake estimation_cell_collection patri zadana vstupni estimation_cell
	SELECT cec.estimation_cell_collection FROM @extschema@.c_estimation_cell AS cec WHERE cec.id = _estimation_cell
	INTO _estimation_cell_collection;
	--------------------------------------------------------------------------------------------
	-- zjisteni estimation_cell_collection_lowest pro zjistenou _estimation_cell_collection
	SELECT cecc.estimation_cell_collection_lowest FROM @extschema@.cm_estimation_cell_collection AS cecc
	WHERE cecc.estimation_cell_collection = _estimation_cell_collection
	INTO _estimation_cell_collection_lowest;
	--------------------------------------------------------------------------------------------
	IF _estimation_cell_collection != _estimation_cell_collection_lowest
	THEN
		-- vstupni estimation_cell neni ZSJ => splnena 1. podminka kontroly
		_check_estimation_cell := TRUE;
	ELSE
		-- vstupni estimation_cell je ZSJ
		
		-- zjisteni poctu geometrii, ktere tvori vstupni estimation_cell, ktera je ZSJ
		SELECT count(fac.gid) FROM @extschema@.f_a_cell AS fac
		WHERE fac.estimation_cell = _estimation_cell
		INTO _check_pocet;

		IF _check_pocet IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 11: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = % nenalezena zadna geometrie v tabulce f_a_cell!',_estimation_cell;
		END IF;

		IF _check_pocet = 1 -- vstupni estimation_cell (ZSJ) uz neni nijak geometricky rozdrobena v f_a_cell na mensi casti
		THEN
			_check_estimation_cell := FALSE;
		ELSE
			_check_estimation_cell := TRUE;
		END IF;
	END IF;
	--------------------------------------------------------------------------------------------
	IF _check_estimation_cell = FALSE
	THEN
		RAISE EXCEPTION 'Chyba 12: fn_get_aux_total4estimation_cell_app: Zadanou estimation_cell = % neni mozno sumarizovat. Jedna se totiz o nejnizsi geografickou uroven, ktera neni geometricky rozdrobena na mensi casti!',_estimation_cell;
	END IF;
	--------------------------------------------------------------------------------------------
	-- kontrola zda jiz pro zadanou estimation_cell, config_id a verzi spadajici do povoleneho
	-- intervalu VERZI jiz existuje hodnota aux_total v tabulce t_aux_total
	-- kontrola se provadi pri VYPOCTU i PREPOCTU [puvodne zde bylo jen pri VYPOCTU]
	-- jelikoz je u vstupnich argumentu ponechan _recount, pak IF jsem upravil nasledovne
	IF _recount = ANY(array[TRUE,FALSE])
	THEN
		IF	(
			SELECT count(tat.*) > 0
			FROM @extschema@.t_aux_total AS tat
			WHERE tat.config = _config_id
			AND tat.estimation_cell = _estimation_cell
			AND tat.ext_version = _ext_version_valid_from
			AND tat.ext_version = _ext_version_valid_until
			)
		THEN
			RAISE EXCEPTION 'Chyba 13: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = %, config_id = % a verzi od % do % jiz existuje hodnota aux_total v tabulce t_aux_total. Sumarizace neni mozna!',_estimation_cell,_config_id,_ext_version_valid_from_label,_ext_version_valid_until_label;
		END IF;
	END IF;
	-------------------------------------------------------------------------------------------
	-- zjisteni gidu (ZSJ) z tabulky f_a_cell, ktere tvori zadanou _estimation_cell
	SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
	WHERE fac.estimation_cell = _estimation_cell
	INTO _gids4estimation_cell;

	IF _gids4estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 14: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = % nenalezeny zadne zaznamy v tabulce f_a_cell!',_estimation_cell;
	END IF;

	-- volani funkce fn_get_lowest_gids [funkce si saha do mapovaci tabulky cm_f_a_cell]
	-- funkce by mela vratit seznam gidu nejnizsi urovne (ZSJ), ktere tvori zadany seznam vyssich gidu
	_gids4estimation_cell_lowest := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell);
	-------------------------------------------------------------------------------------------
	-- pro jistotu provedeni DISTINCTU pro gidy
	SELECT array_agg(t2.gids ORDER BY t2.gids) FROM
	(SELECT DISTINCT t1.gids FROM (SELECT unnest(_gids4estimation_cell_lowest) AS gids) as t1) AS t2
	INTO _gids4estimation_cell_lowest;
	-------------------------------------------------------------------------------------------
	-- kontrola zda v tabulce t_aux_total jsou pro sumarizaci vsechny gidy nejnizsi urovne
	-- pro config_id a pro verzi spadajici do povoleneho intervalu VERZI
	WITH
	w1 AS	(SELECT unnest(_gids4estimation_cell_lowest) AS gids),
	w2 AS	(
		SELECT
			DISTINCT tat.config, tat.cell
		FROM
			@extschema@.t_aux_total AS tat
		WHERE
			tat.config = _config_id
		AND
			tat.cell IN (SELECT gids FROM w1)
		AND
			tat.ext_version >= _ext_version_valid_from
		AND
			tat.ext_version <= _ext_version_valid_until
		),
	w3 AS	(
		SELECT
			w1.gids,
			w2.cell
		FROM
			w1 LEFT JOIN w2
		ON
			w1.gids = w2.cell
		)
	SELECT
		count(*) FROM w3 WHERE w3.cell IS NULL
	INTO
		_gids_in_t_aux_total_check;
	-------------------------------------------------------------------------------------------
	IF _gids_in_t_aux_total_check > 0
	THEN
		RAISE EXCEPTION 'Chyba 15: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = %, config_id = % a verzi od % do % neni v tabulce t_aux_total kompletni seznam hodnot aux_total pro sumarizaci!',_estimation_cell,_config_id,_ext_version_valid_from_label,_ext_version_valid_until_label;
	END IF;
	-------------------------------------------------------------------------------------------
	-- vypocet hodnoty aux_total pro danou estimation_cell
	WITH
	w1 AS	(SELECT unnest(_gids4estimation_cell_lowest) AS gids),
	w2 AS	(
		SELECT
			tat.*,
			max(tat.est_date) OVER (PARTITION BY tat.cell, tat.config) as max_est_date
		FROM
			@extschema@.t_aux_total AS tat
		WHERE
			tat.config = _config_id
		AND
			tat.cell IN (SELECT gids FROM w1)
		AND
			tat.ext_version >= _ext_version_valid_from
		AND
			tat.ext_version <= _ext_version_valid_until			
		),
	w3 AS	(
		SELECT w2.* FROM w2 WHERE w2.est_date = w2.max_est_date		
		)
	SELECT
		sum(w3.aux_total) AS aux_total
	FROM
		w3
	INTO
		_aux_total;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	_q := 'SELECT $1,$2,$3,NULL::integer,$4';
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||'' USING _estimation_cell,_config_id,_aux_total,_ext_version_current;
	--------------------------------------------------------------------------------------------
END ;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) IS
'Funkce vraci hodnotu aux_total pro zadanou estimation_cell a config_id.';