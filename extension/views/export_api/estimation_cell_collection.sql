--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP VIEW IF EXISTS export_api.estimation_cell_collection;

-- DDL
CREATE OR REPLACE VIEW export_api.estimation_cell_collection AS
SELECT
	label, description
FROM
	@extschema@.c_estimation_cell_collection
;

-- authorization
ALTER TABLE export_api.estimation_cell_collection OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.estimation_cell_collection TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.estimation_cell_collection TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.estimation_cell_collection TO public;

-- documentation
COMMENT ON VIEW export_api.estimation_cell_collection IS 'View with list of estimation cell collections.';

