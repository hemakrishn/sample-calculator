--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DDL
create view export_api.variable_hierarchy as
	with w_1 as (
		/*
		1) In case of categorical variable (e.g. OLIL-Forest) this has to sum up to estimation cell area with its related categories from 
		auxiliary_variable group. This principle is utilized when computing category OLIL-other. 
		This case can be conceptually converted to scenario 3) by introducing category "without distinction".
		*/
		select 
			0 as auxiliary_variable_category_sup, 
			array_append(t_config_desc.auxiliary_variable_category_desc, t_config.auxiliary_variable_category) as auxiliary_variable_category
		from @extschema@.t_config 
		inner join @extschema@.c_config_query on (t_config.config_query = c_config_query.id)
		, lateral (select array_agg(tc.auxiliary_variable_category) as auxiliary_variable_category_desc 
				   from @extschema@.t_config as tc 
				   where tc.id in (select unnest(string_to_array(t_config.categories, ','))::int)) as t_config_desc
		where c_config_query.id = 300 
		and t_config.auxiliary_variable_category is not null
	)
	, w_2 as (
		/*
		2) This case covers situation when superior category is created by aggregation of existing categories (e.g. OLIL-other vegetation).
		*/
		select 
			t_config.auxiliary_variable_category as auxiliary_variable_category_sup, 
			t_config_desc.auxiliary_variable_category_desc as auxiliary_variable_category
		from @extschema@.t_config 
		inner join @extschema@.c_config_query on (t_config.config_query = c_config_query.id)
		, lateral (select array_agg(tc.auxiliary_variable_category) as auxiliary_variable_category_desc 
				   from @extschema@.t_config as tc 
				   where tc.id in (select unnest(string_to_array(t_config.categories, ','))::int)) as t_config_desc
		where c_config_query.id = 200 
		and t_config.auxiliary_variable_category is not null
	)
	, w_3 as (
		/*
		3) This is situation resulting from intersection between categorical and continuous categories (e.g. OLIL-Forest x ndsm2016/2107).
		It has to sum up to continuous category without interaction with its related categories from the same auxiliary_variable (ndsm2016/2017).
		*/
		select 
			t_config_complete.auxiliary_variable_category_compl as auxiliary_variable_category_sup, 
			array_append(t_config_desc.auxiliary_variable_category_desc, t_config.auxiliary_variable_category) as auxiliary_variable_category
		from @extschema@.t_config 
		inner join @extschema@.c_config_query on (t_config.config_query = c_config_query.id)
		, lateral (select tc.auxiliary_variable_category as auxiliary_variable_category_compl 
				   from @extschema@.t_config as tc 
				   where tc.id = t_config.complete::int) as t_config_complete
		, lateral (select array_agg(tc.auxiliary_variable_category) as auxiliary_variable_category_desc 
				   from @extschema@.t_config as tc 
				   where tc.id in (select unnest(string_to_array(t_config.categories, ','))::int)) as t_config_desc
		where c_config_query.id = 400
		and t_config.auxiliary_variable_category is not null
	)
	select 
		auxiliary_variable_category_sup, 
		unnest(auxiliary_variable_category) as auxiliary_variable_category
	from w_1
	union all
	select	
		auxiliary_variable_category_sup, 
		unnest(auxiliary_variable_category) as auxiliary_variable_category 
	from w_2
	union all
	select
		auxiliary_variable_category_sup, 
		unnest(auxiliary_variable_category) as auxiliary_variable_category 
	from w_3
;


-- authorization
ALTER TABLE export_api.variable_hierarchy OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.variable_hierarchy TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.variable_hierarchy TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.variable_hierarchy TO public;

-- documentation
comment on view export_api.variable_hierarchy is 'View covers three different scenarios how categories could sum up (for details see view source code). "Intercept" category is hard-wired to code 0.';
COMMENT ON COLUMN export_api.variable_hierarchy.auxiliary_variable_category IS 'Identifier of aux. variable category.';
COMMENT ON COLUMN export_api.variable_hierarchy.auxiliary_variable_category_sup IS 'Identifier of superior aux. variable category.';
