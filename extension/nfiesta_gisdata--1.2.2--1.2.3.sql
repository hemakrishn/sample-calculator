--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------;
-- setting public privileges for tables
--------------------------------------------------------------------------------;

GRANT SELECT ON TABLE @extschema@.c_lai_decrease_bands		TO public;
GRANT SELECT ON TABLE @extschema@.c_lai_decrease_category	TO public;
GRANT SELECT ON TABLE @extschema@.c_s2_ftype_bands		TO public;
GRANT SELECT ON TABLE @extschema@.c_s2_ftype_category		TO public;
GRANT SELECT ON TABLE @extschema@.c_s2_ftype_smooth_bands	TO public;
GRANT SELECT ON TABLE @extschema@.c_s2_species_bands		TO public;
GRANT SELECT ON TABLE @extschema@.c_s2_species_category		TO public;
GRANT SELECT ON TABLE @extschema@.c_s2_species_smooth_bands	TO public;
GRANT SELECT ON TABLE @extschema@.r_ndsm_smooth			TO public;
GRANT SELECT ON TABLE @extschema@.r_s2_ftype_smooth		TO public;
GRANT SELECT ON TABLE @extschema@.r_s2_species_smooth		TO public;

--------------------------------------------------------------------------------;
-- setting public privileges for sequences
--------------------------------------------------------------------------------;

GRANT SELECT ON SEQUENCE @extschema@.cm_estimation_cell_collection_id_seq	TO public;
GRANT SELECT ON SEQUENCE @extschema@.cm_ext_gui_version_id_seq			TO public;
GRANT SELECT ON SEQUENCE @extschema@.cm_f_a_cell_id_seq				TO public;
GRANT SELECT ON SEQUENCE @extschema@.f_a_cell_gid_seq				TO public;
GRANT SELECT ON SEQUENCE @extschema@.r_ndsm_smooth_rid_seq			TO public;
GRANT SELECT ON SEQUENCE @extschema@.r_s2_ftype_smooth_rid_seq			TO public;
GRANT SELECT ON SEQUENCE @extschema@.r_s2_species_smooth_rid_seq		TO public;
GRANT SELECT ON SEQUENCE @extschema@.seq__f_p_plot__gid				TO public;
GRANT SELECT ON SEQUENCE @extschema@.seq__t_aux_total_id			TO public;
GRANT SELECT ON SEQUENCE @extschema@.t_auxiliary_data_id_seq			TO public;

--------------------------------------------------------------------------------;
-- dumps for tables
--------------------------------------------------------------------------------;

SELECT pg_catalog.pg_extension_config_dump('@extschema@.f_p_plot', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_auxiliary_data', '');

--------------------------------------------------------------------------------;
-- dumps for sequences
--------------------------------------------------------------------------------;

SELECT pg_catalog.pg_extension_config_dump('@extschema@.seq__f_p_plot__gid', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_auxiliary_data_id_seq', '');

--------------------------------------------------------------------------------;
-- add new version into c_ext_version and cm_ext_gui_version
--------------------------------------------------------------------------------;

	INSERT INTO @extschema@.c_ext_version(id, label, description)
	VALUES
		(700,'1.2.3','Version 1.2.3 - extension nfiesta_gisdata for auxiliary data. Setting public permission for sequences.');

	INSERT INTO @extschema@.cm_ext_gui_version(ext_version, gui_version)
	VALUES
		(700,500);

--------------------------------------------------------------------------------;