--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
--
-- Data for Name: plots; Type: TABLE DATA; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--
CREATE TABLE gisdata_test.plots 
(
	gid integer NOT NULL,
	country varchar(20),
	strata_set varchar(20),
	stratum varchar(20),
	panel varchar(20),
	cluster varchar(20),
	plot varchar(20),
	geom geometry(POINT, 5514) NOT NULL
);
ALTER TABLE gisdata_test.plots OWNER TO adm_nfiesta_gisdata;
--
-- Name: plots_gid_seq; Type: SEQUENCE; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--
CREATE SEQUENCE gisdata_test.plots_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;
ALTER TABLE gisdata_test.plots_gid_seq OWNER TO adm_nfiesta_gisdata;
--
-- Name: plots_gid_seq; Type: SEQUENCE OWNED BY; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--
ALTER SEQUENCE gisdata_test.plots_gid_seq OWNED BY gisdata_test.plots.gid;
--
-- Name: plots gid; Type: DEFAULT; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--
ALTER TABLE ONLY gisdata_test.plots ALTER COLUMN gid SET DEFAULT nextval('gisdata_test.plots_gid_seq'::regclass);
--
-- Name: plots_pkey; Type: CONSTRAINT; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--
ALTER TABLE ONLY gisdata_test.plots ADD CONSTRAINT plots_pkey PRIMARY KEY (gid);
--
-- Name: plots enforce_dims_geom; Type: CHECK CONSTRAINT; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--
ALTER TABLE gisdata_test.plots ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);
--
-- Name: plots enforce_geotype_geom; Type: CHECK CONSTRAINT; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--
ALTER TABLE gisdata_test.plots ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POINT'::text);
--
-- Name: plots enforce_srid_geom; Type: CHECK CONSTRAINT; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--
ALTER TABLE gisdata_test.plots ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 5514);
--
-- Name: spidx__plots__geom; Type: INDEX; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--
CREATE INDEX spidx__plots__geom ON gisdata_test.plots USING gist (geom);
INSERT INTO gisdata_test.plots(country,strata_set,stratum,panel,cluster,plot,geom)
SELECT
	country,
	strata_set,
	stratum,
	panel,
	cluster,
	plot,
	ST_GeomFromEWKT(plot_geometry) as geom
FROM
	csv.plots;
