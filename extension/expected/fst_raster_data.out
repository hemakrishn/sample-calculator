----------------------------------------DATA LICENCE DISCLAIMER
/*

1. Following data are comming from Copernicus pan Eropean forest monitoring program
https://land.copernicus.eu/pan-european/high-resolution-layers/forests.

Namely two layers:
* Dominant Leaf Type (DLT) 2012,
* Tree Cover Density (TCD) 2012.

See
https://land.copernicus.eu/pan-european/high-resolution-layers/forests/forest-type-1/status-maps/2012?tab=metadata 
for the original Constraints related to access and use.

2. This project is not officially endorsed by the Union.

3. Original rasters are clipped to the area of interest -- Moravia region of Czech Republic.

4. The data remain the sole property of the European Union.
*/
----------------------------------------DERIVATION PROCEDURE----------------------------------------
/*
download
--------
COPERNICUS

* Forest type 2012, 2015, 2018 https://land.copernicus.eu/pan-european/high-resolution-layers/forests/forest-type-1/status-map
* Tree Cover Density 2012, 2015, 2018 https://land.copernicus.eu/pan-european/high-resolution-layers/forests/tree-cover-density/status-maps
* Dominant Leaf Type 2012, 2015, 2018 https://land.copernicus.eu/pan-european/high-resolution-layers/forests/dominant-leaf-type/status-maps

clip
----

Area Of Interest
```sql
create materialized view aoi as
select 1 as gid, st_buffer(st_union(geom), 100) as geom
from nfiesta_test.t_stratum;
```

convert to geopackage in QGIS

clip rasters using gdalwarp

Win:
PS D:\> & 'C:\Program Files\QGIS 3.18\bin\gdalwarp.exe' -cutline fst_aoi.gpkg -crop_to_cutline -srcnodata 255 -dstnodata 0 -s_srs EPSG:3035 -t_srs EPSG:5514 -tr 100 100 .\DLT_2012_020m_eu_03035_d03_Full\DLT_2012_020m_eu_03035_d03_full.tif DLT_2012_020m_eu_03035_d03_aoi_100m.tif

Linux:
gdalwarp -cutline fst_aoi.gpkg -crop_to_cutline -srcnodata 255 -dstnodata 0 -s_srs EPSG:3035 -t_srs EPSG:5514 -tr 100 100 TCD_2012_020m_eu_03035_d03_Full/FTY_2012_020m_eu_03035_d03_full.tif FTY_2012_020m_eu_03035_d03_aoi_50m.tif

load
----

raster2pgsql -s 5514 -N 0 -t 2x2 -d -Y -e -I -C DLT_2012_020m_eu_03035_d03_aoi_100m.tif gisdata_test.dlt_2012_100m | psql contrib_regression_data
raster2pgsql -s 5514 -N 0 -t 2x2 -d -Y -e -I -C TCD_2012_020m_eu_03035_d03_aoi_100m.tif gisdata_test.tcd_2012_100m | psql contrib_regression_data

vacuum full ANALYZE gisdata_test.dlt_2012_100m;
vacuum full ANALYZE gisdata_test.tcd_2012_100m;

dump
----
pg_dump --format plain --table gisdata_test.dlt_2012_100m --table gisdata_test.tcd_2012_100m --dbname contrib_regression_data --schema-only --file fst_raster_schema.sql
pg_dump --format plain --table gisdata_test.dlt_2012_100m --table gisdata_test.tcd_2012_100m --dbname contrib_regression_data --data-only --file fst_raster_data.sql

vim fst_raster_data.sql ... add header

tar -czf fst_raster_data.sql.tar.gz fst_raster_data.sql
*/
----------------------------------------DATA----------------------------------------
--
-- PostgreSQL database dump
--
-- Dumped from database version 12.6 (Debian 12.6-1.pgdg100+1)
-- Dumped by pg_dump version 12.6 (Debian 12.6-1.pgdg100+1)
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
 set_config 
------------
 
(1 row)

SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;
--
-- Data for Name: dlt_2012_100m; Type: TABLE DATA; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--
COPY gisdata_test.dlt_2012_100m (rid, rast) FROM stdin;
--
-- Data for Name: tcd_2012_100m; Type: TABLE DATA; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--
COPY gisdata_test.tcd_2012_100m (rid, rast) FROM stdin;
--
-- Name: dlt_2012_100m_rid_seq; Type: SEQUENCE SET; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--
SELECT pg_catalog.setval('gisdata_test.dlt_2012_100m_rid_seq', 405880, true);
 setval 
--------
 405880
(1 row)

--
-- Name: tcd_2012_100m_rid_seq; Type: SEQUENCE SET; Schema: gisdata_test; Owner: adm_nfiesta_gisdata
--
SELECT pg_catalog.setval('gisdata_test.tcd_2012_100m_rid_seq', 405880, true);
 setval 
--------
 405880
(1 row)

--
-- PostgreSQL database dump complete
--
