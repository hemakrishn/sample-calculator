--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------;
CREATE SCHEMA export_api;
ALTER SCHEMA export_api OWNER TO adm_nfiesta_gisdata;
GRANT USAGE ON SCHEMA export_api TO app_nfiesta_gisdata;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- VIEWS --
---------------------------------------------------------------------------------------------------;

-- v_estimation_cell_hierarchy

-- <view name="v_estimation_cell_hierarchy" schema="export_api" src="views/export_api/v_estimation_cell_hierarchy.sql">
create or replace view export_api.v_estimation_cell_hierarchy as
	with w_data as (
	select
		c_estimation_cell.id as c_estimation_cell__id,
		c_estimation_cell.label as c_estimation_cell__label,
		c_estimation_cell_sup.id as c_estimation_cell_sup__id,
		c_estimation_cell_sup.label as c_estimation_cell_sup__label,
		c_estimation_cell_sup.estimation_cell_collection as c_estimation_cell_sup__estimation_cell_collection
	from @extschema@.cm_f_a_cell
	inner join @extschema@.f_a_cell 						on cm_f_a_cell.cell = f_a_cell.gid
	inner join @extschema@.c_estimation_cell 					ON c_estimation_cell.id = f_a_cell.estimation_cell
	inner join @extschema@.f_a_cell 		as f_a_cell_sup 		on cm_f_a_cell.cell_sup = f_a_cell_sup.gid
	inner join @extschema@.c_estimation_cell 	as c_estimation_cell_sup 	ON c_estimation_cell_sup.id = f_a_cell_sup.estimation_cell
	group by 
		c_estimation_cell.id, 
		c_estimation_cell.label, 
		c_estimation_cell_sup.id, 
		c_estimation_cell_sup.label, 
		c_estimation_cell_sup.estimation_cell_collection
	)
	select 
		c_estimation_cell__id as cell,
		/*c_estimation_cell__label,
		c_estimation_cell_sup__estimation_cell_collection,
		array_agg(c_estimation_cell_sup__id) as agg__c_estimation_cell_sup__id,
		array_agg(c_estimation_cell_sup__label) as agg__c_estimation_cell_sup__label,*/
		(array_agg(c_estimation_cell_sup__id))[1] as cell_superior
	from w_data
	group by 
		c_estimation_cell__id, 
		c_estimation_cell__label, 
		c_estimation_cell_sup__estimation_cell_collection
	having count(*) = 1
	order by c_estimation_cell__id, c_estimation_cell_sup__estimation_cell_collection
;

ALTER TABLE export_api.v_estimation_cell_hierarchy OWNER TO adm_nfiesta_gisdata;
COMMENT ON VIEW export_api.v_estimation_cell_hierarchy IS 'View providing hierarchy of estimation cells to nfi_esta ETL process.';

GRANT SELECT ON TABLE export_api.v_estimation_cell_hierarchy TO app_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.v_estimation_cell_hierarchy TO adm_nfiesta_gisdata;

COMMENT ON COLUMN export_api.v_estimation_cell_hierarchy.cell IS 'Foreign key to c_estimation cell.';
COMMENT ON COLUMN export_api.v_estimation_cell_hierarchy.cell_superior IS 'Foreign key to superior c_estimation cell.';


-- </view>
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- add ext_version 1.1.0
---------------------------------------------------------------------------------------------------;
INSERT INTO @extschema@.c_ext_version(id, label, description)
VALUES
	(200,'1.1.0','Verze 1.1.0 - extenze nfiesta_gisdata pro pomocná data. Vytvoření schematu export_api a pohledu v_estimation_cell_hierarchy.');
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- add ext_version 1.1.0 into cm_ext_gui_version
---------------------------------------------------------------------------------------------------;
INSERT INTO @extschema@.cm_ext_gui_version(ext_version, gui_version)
VALUES
	(200,400);
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;






