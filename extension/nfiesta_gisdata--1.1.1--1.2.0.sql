--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
/*
	extension of the extension by a part of the point intersection => Issue 4 on GitLab
*/

---------------------------------------------------------------------------------------------------;
-- add ext_version 1.2.0
---------------------------------------------------------------------------------------------------;
INSERT INTO @extschema@.c_ext_version(id, label, description)
VALUES
	(400,'1.2.0','Verze 1.2.0 - extenze nfiesta_gisdata pro pomocná data. Rozšíření extenze o část bodové protínání.');
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- add gui_version 1.1.0
---------------------------------------------------------------------------------------------------;
INSERT INTO @extschema@.c_gui_version(id, label, description)
VALUES
	(500,'1.1.0','Verze 1.1.0 - GUI aplikace pro pomocná data. Rozšíření aplikace o část bodové protínání.');
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- add ext_version 1.2.0  and gui_version 1.1.0 into cm_ext_gui_version
---------------------------------------------------------------------------------------------------;
INSERT INTO @extschema@.cm_ext_gui_version(ext_version, gui_version)
VALUES
	(400,500);
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- add new records into c_config_function
---------------------------------------------------------------------------------------------------;
INSERT INTO @extschema@.c_config_function(id, label, description)
VALUES
	(500,'Konfigurace bodové vrstvy', NULL),
	(600,'Konfigurace kombinace bodové vrstvy a úhrnu', NULL);
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- t_config_collection
---------------------------------------------------------------------------------------------------;

-- columns

	-- condition
	ALTER TABLE @extschema@.t_config_collection
	DROP COLUMN IF EXISTS condition;

	ALTER TABLE @extschema@.t_config_collection
	ADD COLUMN condition character varying NULL;

	COMMENT ON COLUMN @extschema@.t_config_collection.condition
	IS 'Výběrová podmínka z GIS vrstvy bodů';


	-- total_points
	ALTER TABLE @extschema@.t_config_collection
	DROP COLUMN IF EXISTS total_points;

	ALTER TABLE @extschema@.t_config_collection
	ADD COLUMN total_points integer NULL;

	COMMENT ON COLUMN @extschema@.t_config_collection.total_points
	IS 'Počet bodů vybraných z GIS vrstvy';


	-- ref_id_layer_points
	ALTER TABLE @extschema@.t_config_collection
	DROP COLUMN IF EXISTS ref_id_layer_points;

	ALTER TABLE @extschema@.t_config_collection
	ADD COLUMN ref_id_layer_points integer NULL;

	COMMENT ON COLUMN @extschema@.t_config_collection.ref_id_layer_points
	IS 'Identifikační číslo konfigurace bodové vrstvy pro protínání';


	-- ref_id_total
	ALTER TABLE @extschema@.t_config_collection
	DROP COLUMN IF EXISTS ref_id_total;

	ALTER TABLE @extschema@.t_config_collection
	ADD COLUMN ref_id_total integer NULL;

	COMMENT ON COLUMN @extschema@.t_config_collection.ref_id_total
	IS 'Identifikační číslo konfigurace úhrnu pro protínání';


-- foreign keys

	-- fkey__t_config_collection__ref_id_layer_points
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS fkey__t_config_collection__ref_id_layer_points;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT fkey__t_config_collection__ref_id_layer_points
		FOREIGN KEY (ref_id_layer_points)
		REFERENCES @extschema@.t_config_collection(id)
		MATCH SIMPLE
		ON UPDATE NO ACTION
		ON DELETE NO ACTION
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT fkey__t_config_collection__ref_id_layer_points
		ON @extschema@.t_config_collection
	IS 'Cizí klíč, identifikační číslo konfigurace bodové vrstvy pro protínání';


	-- fkey__t_config_collection__ref_id_total
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS fkey__t_config_collection__ref_id_total;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT fkey__t_config_collection__ref_id_total
		FOREIGN KEY (ref_id_total)
		REFERENCES @extschema@.t_config_collection(id)
		MATCH SIMPLE
		ON UPDATE NO ACTION
		ON DELETE NO ACTION
		NOT DEFERRABLE
		INITIALLY IMMEDIATE;

	COMMENT ON CONSTRAINT fkey__t_config_collection__ref_id_total
		ON @extschema@.t_config_collection
	IS 'Cizí klíč, identifikační číslo konfigurace úhrnu pro protínání';


-- indexes

	-- fki__t_config_collection__ref_id_layer_points
	DROP INDEX IF EXISTS @extschema@.fki__t_config_collection__ref_id_layer_points;
	CREATE INDEX fki__t_config_collection__ref_id_layer_points
		ON @extschema@.t_config_collection
		USING btree(ref_id_layer_points);

	COMMENT ON INDEX @extschema@.fki__t_config_collection__ref_id_layer_points
	IS 'Index přes cizí klíč fkey__t_config_collection__ref_id_layer_points';


	-- fki__t_config_collection__ref_id_total
	DROP INDEX IF EXISTS @extschema@.fki__t_config_collection__ref_id_total;
	CREATE INDEX fki__t_config_collection__ref_id_total
		ON @extschema@.t_config_collection
		USING btree(ref_id_total);

	COMMENT ON INDEX @extschema@.fki__t_config_collection__ref_id_total
	IS 'Index přes cizí klíč fkey__t_config_collection__ref_id_total';


-- check-constraints


	-- id (NOT NULL vzdy)

	-- auxiliary_variable (dobrovolne, povoleno NULL)

	-- config_function (NOT NULL vzdy)

	-- label (NOT NULL vzdy)

	-- aggregated (NOT NULL vzdy)


	-- catalog_name
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS check__t_config_collection__not_null__catalog_name;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT check__t_config_collection__not_null__catalog_name
	CHECK
		(((config_function = 100) AND (NOT aggregated) 	AND (catalog_name IS NOT NULL)) OR
		 ((config_function = 100) AND     (aggregated) 	AND (catalog_name IS     NULL)) OR
		 ((config_function = 200) AND (NOT aggregated) 	AND (catalog_name IS NOT NULL)) OR
		 ((config_function = 200) AND     (aggregated) 	AND (catalog_name IS     NULL)) OR
		 ((config_function = 300) 			AND (catalog_name IS     NULL)) OR
		 ((config_function = 400)			AND (catalog_name IS     NULL)) OR
		 ((config_function = 500)			AND (catalog_name IS NOT NULL)) OR
		 ((config_function = 600)			AND (catalog_name IS     NULL)));

	COMMENT ON CONSTRAINT check__t_config_collection__not_null__catalog_name
		ON @extschema@.t_config_collection
	IS 'Vyplněnost a nevyplněnost catalog_name, musí být vyplněno u konfigurací GIS vrstev.';


	-- schema_name
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS check__t_config_collection__not_null__schema_name;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT check__t_config_collection__not_null__schema_name
	CHECK
		(((config_function = 100) AND (NOT aggregated) 	AND (schema_name IS NOT NULL)) OR
		 ((config_function = 100) AND     (aggregated) 	AND (schema_name IS     NULL)) OR
		 ((config_function = 200) AND (NOT aggregated) 	AND (schema_name IS NOT NULL)) OR
		 ((config_function = 200) AND     (aggregated) 	AND (schema_name IS     NULL)) OR
		 ((config_function = 300) 			AND (schema_name IS     NULL)) OR
		 ((config_function = 400)			AND (schema_name IS     NULL)) OR
		 ((config_function = 500)			AND (schema_name IS NOT NULL)) OR
		 ((config_function = 600)			AND (schema_name IS     NULL)));

	COMMENT ON CONSTRAINT check__t_config_collection__not_null__schema_name
		ON @extschema@.t_config_collection
	IS 'Vyplněnost a nevyplněnost schema_name, musí být vyplněno u konfigurací GIS vrstev.';


	-- table_name
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS check__t_config_collection__not_null__table_name;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT check__t_config_collection__not_null__table_name
	CHECK
		(((config_function = 100) AND (NOT aggregated) 	AND (table_name IS NOT NULL)) OR
		 ((config_function = 100) AND     (aggregated) 	AND (table_name IS     NULL)) OR
		 ((config_function = 200) AND (NOT aggregated) 	AND (table_name IS NOT NULL)) OR
		 ((config_function = 200) AND     (aggregated) 	AND (table_name IS     NULL)) OR
		 ((config_function = 300) 			AND (table_name IS     NULL)) OR
		 ((config_function = 400)			AND (table_name IS     NULL)) OR
		 ((config_function = 500)			AND (table_name IS NOT NULL)) OR
		 ((config_function = 600)			AND (table_name IS     NULL)));

	COMMENT ON CONSTRAINT check__t_config_collection__not_null__table_name
		ON @extschema@.t_config_collection
	IS 'Vyplněnost a nevyplněnost table_name, musí být vyplněno u konfigurací GIS vrstev.';


	-- column_ident
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS check__t_config_collection__not_null__column_ident;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT check__t_config_collection__not_null__column_ident
	CHECK
		(((config_function = 100) AND (NOT aggregated) 	AND (column_ident IS NOT NULL)) OR
		 ((config_function = 100) AND     (aggregated) 	AND (column_ident IS     NULL)) OR
		 ((config_function = 200) AND (NOT aggregated) 	AND (column_ident IS NOT NULL)) OR
		 ((config_function = 200) AND     (aggregated) 	AND (column_ident IS     NULL)) OR
		 ((config_function = 300) 			AND (column_ident IS     NULL)) OR
		 ((config_function = 400)			AND (column_ident IS     NULL)) OR
		 ((config_function = 500)			AND (column_ident IS NOT NULL)) OR
		 ((config_function = 600)			AND (column_ident IS     NULL)));

	COMMENT ON CONSTRAINT check__t_config_collection__not_null__column_ident
		ON @extschema@.t_config_collection
	IS 'Vyplněnost a nevyplněnost column_ident, musí být vyplněno u konfigurací GIS vrstev.';


	-- column_name
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS check__t_config_collection__not_null__column_name;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT check__t_config_collection__not_null__column_name
	CHECK
		(((config_function = 100) AND (NOT aggregated) 	AND (column_name IS NOT NULL)) OR
		 ((config_function = 100) AND     (aggregated) 	AND (column_name IS     NULL)) OR
		 ((config_function = 200) AND (NOT aggregated) 	AND (column_name IS NOT NULL)) OR
		 ((config_function = 200) AND     (aggregated) 	AND (column_name IS     NULL)) OR
		 ((config_function = 300) 			AND (column_name IS     NULL)) OR
		 ((config_function = 400)			AND (column_name IS     NULL)) OR
		 ((config_function = 500)			AND (column_name IS NOT NULL)) OR
		 ((config_function = 600)			AND (column_name IS     NULL)));

	COMMENT ON CONSTRAINT check__t_config_collection__not_null__column_name
		ON @extschema@.t_config_collection
	IS 'Vyplněnost a nevyplněnost column_name, musí být vyplněno u konfigurací GIS vrstev.';


	-- unit
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS check__t_config_collection__not_null__unit;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT check__t_config_collection__not_null__unit
	CHECK
		(((config_function = 100) AND (NOT aggregated) 	AND (unit IS NOT NULL)) OR
		 ((config_function = 100) AND     (aggregated) 	AND (unit IS     NULL)) OR
		 ((config_function = 200) AND (NOT aggregated) 	AND (unit IS NOT NULL)) OR
		 ((config_function = 200) AND     (aggregated) 	AND (unit IS     NULL)) OR
		 ((config_function = 300) 			AND (unit IS     NULL)) OR
		 ((config_function = 400)			AND (unit IS     NULL)) OR
		 ((config_function = 500)			AND (unit IS     NULL)) OR
		 ((config_function = 600)			AND (unit IS     NULL)));

	COMMENT ON CONSTRAINT check__t_config_collection__not_null__unit
		ON @extschema@.t_config_collection
	IS 'Vyplněnost a nevyplněnost unit, musí být vyplněno u konfigurací GIS vrstev, kromě bodových vrstev.';


	-- tag (dobrovolne, povoleno NULL)

	-- description (NOT NULL vzdy)

	-- closed (NOT NULL vzdy)

	-- edit_date (NOT NULL vzdy)


	-- condition
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS check__t_config_collection__not_null__condition;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT check__t_config_collection__not_null__condition
	CHECK
		(((config_function = 100) AND (NOT aggregated) 	AND (condition IS     NULL)) OR
		 ((config_function = 100) AND     (aggregated) 	AND (condition IS     NULL)) OR
		 ((config_function = 200) AND (NOT aggregated) 	AND (condition IS     NULL)) OR
		 ((config_function = 200) AND     (aggregated) 	AND (condition IS     NULL)) OR
		 ((config_function = 300) 			AND (condition IS     NULL)) OR
		 ((config_function = 400)			AND (condition IS     NULL)) OR
		 ((config_function = 500)			AND (condition IS NOT NULL)) OR
		 ((config_function = 600)			AND (condition IS     NULL)));

	COMMENT ON CONSTRAINT check__t_config_collection__not_null__condition
		ON @extschema@.t_config_collection
	IS 'Vyplněnost a nevyplněnost condition, musí být vyplněno u konfigurací bodových vrstev.';


	-- total_points
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS check__t_config_collection__not_null__total_points;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT check__t_config_collection__not_null__total_points
	CHECK
		(((config_function = 100) AND (NOT aggregated) 	AND (total_points IS     NULL)) OR
		 ((config_function = 100) AND     (aggregated) 	AND (total_points IS     NULL)) OR
		 ((config_function = 200) AND (NOT aggregated) 	AND (total_points IS     NULL)) OR
		 ((config_function = 200) AND     (aggregated) 	AND (total_points IS     NULL)) OR
		 ((config_function = 300) 			AND (total_points IS     NULL)) OR
		 ((config_function = 400)			AND (total_points IS     NULL)) OR
		 ((config_function = 500)			AND (total_points IS NOT NULL)) OR
		 ((config_function = 600)			AND (total_points IS     NULL)));

	COMMENT ON CONSTRAINT check__t_config_collection__not_null__total_points
		ON @extschema@.t_config_collection
	IS 'Vyplněnost a nevyplněnost total_points, musí být vyplněno u konfigurací bodových vrstev.';


	-- ref_id_layer_points
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS check__t_config_collection__not_null__ref_id_layer_points;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT check__t_config_collection__not_null__ref_id_layer_points
	CHECK
		(((config_function = 100) AND (NOT aggregated) 	AND (ref_id_layer_points IS     NULL)) OR
		 ((config_function = 100) AND     (aggregated) 	AND (ref_id_layer_points IS     NULL)) OR
		 ((config_function = 200) AND (NOT aggregated) 	AND (ref_id_layer_points IS     NULL)) OR
		 ((config_function = 200) AND     (aggregated) 	AND (ref_id_layer_points IS     NULL)) OR
		 ((config_function = 300) 			AND (ref_id_layer_points IS     NULL)) OR
		 ((config_function = 400)			AND (ref_id_layer_points IS     NULL)) OR
		 ((config_function = 500)			AND (ref_id_layer_points IS     NULL)) OR
		 ((config_function = 600)			AND (ref_id_layer_points IS NOT NULL)));

	COMMENT ON CONSTRAINT check__t_config_collection__not_null__ref_id_layer_points
		ON @extschema@.t_config_collection
	IS 'Vyplněnost a nevyplněnost ref_id_layer_points, musí být vyplněno u konfigurací kombinace bodové vrstvy a úhrnu.';


	-- ref_id_total
	ALTER TABLE @extschema@.t_config_collection
	DROP CONSTRAINT IF EXISTS check__t_config_collection__not_null__ref_id_total;

	ALTER TABLE @extschema@.t_config_collection ADD
	CONSTRAINT check__t_config_collection__not_null__ref_id_total
	CHECK
		(((config_function = 100) AND (NOT aggregated) 	AND (ref_id_total IS     NULL)) OR
		 ((config_function = 100) AND     (aggregated) 	AND (ref_id_total IS     NULL)) OR
		 ((config_function = 200) AND (NOT aggregated) 	AND (ref_id_total IS     NULL)) OR
		 ((config_function = 200) AND     (aggregated) 	AND (ref_id_total IS     NULL)) OR
		 ((config_function = 300) 			AND (ref_id_total IS     NULL)) OR
		 ((config_function = 400)			AND (ref_id_total IS     NULL)) OR
		 ((config_function = 500)			AND (ref_id_total IS     NULL)) OR
		 ((config_function = 600)			AND (ref_id_total IS NOT NULL)));

	COMMENT ON CONSTRAINT check__t_config_collection__not_null__ref_id_total
		ON @extschema@.t_config_collection
	IS 'Vyplněnost a nevyplněnost ref_id_total, musí být vyplněno u konfigurací kombinace bodové vrstvy a úhrnu.';
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- create trigger function fn_t_config_collection__before_insert
---------------------------------------------------------------------------------------------------;
-- <function name="fn_t_config_collection__before_insert" schema="@extschema@" src="functions/extschema/fn_t_config_collection__before_insert.sql">
--------------------------------------------------------------------------------;
--	fn_t_config_collection__before_insert
--------------------------------------------------------------------------------;

	DROP FUNCTION IF EXISTS @extschema@.fn_t_config_collection__before_insert() CASCADE;

	CREATE OR REPLACE FUNCTION @extschema@.fn_t_config_collection__before_insert()
	RETURNS trigger AS
	$$
	BEGIN
		IF (NEW.config_function = 500)
		THEN
			EXECUTE
				'SELECT coalesce(count('||NEW.column_ident||'),0) '||
				'FROM '||NEW.schema_name||'.'||NEW.table_name||' '||
				'WHERE ('||NEW.condition||');'
			INTO NEW.total_points;

			IF NEW.total_points = 0
			THEN
				RAISE EXCEPTION 'Error 01: fn_t_config_collection__before_insert: The value of "total_points" cannot by zero.';
			END IF;
		ELSE
			NEW.total_points := NULL;
		END IF;
		RETURN NEW;
	END;
	$$
	LANGUAGE plpgsql
	VOLATILE
	SECURITY INVOKER;

--------------------------------------------------------------------------------;
-- vlastnik a opravneni
--------------------------------------------------------------------------------;

	ALTER FUNCTION @extschema@.fn_t_config_collection__before_insert()
	OWNER TO adm_nfiesta_gisdata;

	GRANT ALL
	ON FUNCTION @extschema@.fn_t_config_collection__before_insert()
	TO adm_nfiesta_gisdata;

	GRANT EXECUTE
	ON FUNCTION @extschema@.fn_t_config_collection__before_insert()
	TO app_nfiesta_gisdata;

	GRANT ALL
	ON FUNCTION @extschema@.fn_t_config_collection__before_insert()
	TO public;

--------------------------------------------------------------------------------;
-- dokumentace
--------------------------------------------------------------------------------;

	COMMENT ON FUNCTION @extschema@.fn_t_config_collection__before_insert()
	IS
	'Funkce triggeru, před vložením záznamu do tabulky t_config_collection provede výpočet počtu bodů v bodové vrstvě a zapíše výsledek do sloupce total_points.';

--------------------------------------------------------------------------------;
-- </function>
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- create trigger function fn_t_config_collection__before_update
---------------------------------------------------------------------------------------------------;
-- <function name="fn_t_config_collection__before_update" schema="@extschema@" src="functions/extschema/fn_t_config_collection__before_update.sql">
--------------------------------------------------------------------------------;
--	fn_t_config_collection__before_update
--------------------------------------------------------------------------------;

	DROP FUNCTION IF EXISTS @extschema@.fn_t_config_collection__before_update() CASCADE;

	CREATE FUNCTION @extschema@.fn_t_config_collection__before_update()
	RETURNS trigger
	AS $$
	BEGIN
		IF (NEW.config_function = 500)
		THEN
			EXECUTE
				'SELECT coalesce(count('||NEW.column_ident||'),0) '||
				'FROM '||NEW.schema_name||'.'||NEW.table_name||' '||
				'WHERE ('||NEW.condition||');'
			INTO NEW.total_points;

			IF (NEW.total_points = 0)
			THEN
				RAISE EXCEPTION 'Error 01: fn_t_config_collection__before_update: The value of "total_points" cannot by zero.';
			END IF;
		ELSE
			NEW.total_points := NULL;
		END IF;
		RETURN NEW;
	END;
	$$
	LANGUAGE plpgsql
	VOLATILE
	SECURITY INVOKER;

--------------------------------------------------------------------------------;
-- vlastnik a opravneni
--------------------------------------------------------------------------------;

	ALTER FUNCTION @extschema@.fn_t_config_collection__before_update()
	OWNER TO adm_nfiesta_gisdata;

	GRANT ALL
	ON FUNCTION @extschema@.fn_t_config_collection__before_update()
	TO adm_nfiesta_gisdata;

	GRANT EXECUTE
	ON FUNCTION @extschema@.fn_t_config_collection__before_update()
	TO app_nfiesta_gisdata;

	GRANT ALL
	ON FUNCTION @extschema@.fn_t_config_collection__before_update()
	TO public;

--------------------------------------------------------------------------------;
-- dokumentace
--------------------------------------------------------------------------------;

	COMMENT ON FUNCTION @extschema@.fn_t_config_collection__before_update()
	IS
	'Funkce triggeru, před změnou záznamu v tabulce t_config_collection provede výpočet počtu bodů v bodové vrstvě a zapíše výsledek do sloupce total_points.';
	
-- </function>
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- create BEFORE INSERT trigger for table t_config_collection
---------------------------------------------------------------------------------------------------;
	DROP TRIGGER IF EXISTS trg__t_config_collection__before_insert
	ON @extschema@.t_config_collection;

	CREATE TRIGGER trg__t_config_collection__before_insert
	BEFORE INSERT
	ON @extschema@.t_config_collection
	FOR EACH ROW
	EXECUTE PROCEDURE
	@extschema@.fn_t_config_collection__before_insert();

	COMMENT ON TRIGGER trg__t_config_collection__before_insert
	ON @extschema@.t_config_collection
	IS 'Trigger spouští před vložením nového záznamu do tabulky t_config_collection funkci fn_t_config_collection__before_insert, '
	'která provede výpočet počtu bodů v bodové vrstvě a zapíše výsledek do sloupce total_points.';
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- create BEFORE UPDATE trigger for table t_config_collection
---------------------------------------------------------------------------------------------------;
	DROP TRIGGER IF EXISTS trg__t_config_collection__before_update
	ON @extschema@.t_config_collection;

	CREATE TRIGGER trg__t_config_collection__before_update
	BEFORE UPDATE
	ON @extschema@.t_config_collection
	FOR EACH ROW
	EXECUTE PROCEDURE
	@extschema@.fn_t_config_collection__before_update();

	COMMENT ON TRIGGER trg__t_config_collection__before_update
	ON @extschema@.t_config_collection
	IS 'Trigger spouští před změnou záznamu v tabulce t_config_collection funkci fn_t_config_collection__before_update, '
	'která provede výpočet počtu bodů v bodové vrstvě a zapíše výsledek do sloupce total_points.';
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- create table t_auxiliary_data
---------------------------------------------------------------------------------------------------;
CREATE TABLE @extschema@.t_auxiliary_data
(
	id			serial 			NOT NULL,
	config_collection	integer			NOT NULL,
	config			integer			NOT NULL,
	ident_point		integer			NOT NULL,	--  nasledne bude predelano na PID
	value			double precision	NOT NULL,
	ext_version		integer			NOT NULL,
	gui_version		integer			NOT NULL,
	CONSTRAINT pkey__t_auxiliary_data PRIMARY KEY (id)
);

COMMENT ON TABLE @extschema@.t_auxiliary_data IS 'Table with the auxiliary plot data.';
COMMENT ON COLUMN @extschema@.t_auxiliary_data.id IS 'Identifikační číslo záznamu.';
COMMENT ON COLUMN @extschema@.t_auxiliary_data.config_collection IS 'Cizí klíč na pole id do tabulky t_config_collection.';
COMMENT ON COLUMN @extschema@.t_auxiliary_data.config IS 'Cizí klíč na pole id do tabulky t_config.';
COMMENT ON COLUMN @extschema@.t_auxiliary_data.ident_point IS 'Identifikační záznam bodu v rámci sloupce config_collection a config.';
COMMENT ON COLUMN @extschema@.t_auxiliary_data.value IS 'Hodnota pomocné veličiny.';
COMMENT ON COLUMN @extschema@.t_auxiliary_data.ext_version IS 'Cizí klíč na pole id do tabulky c_ext_version.';
COMMENT ON COLUMN @extschema@.t_auxiliary_data.gui_version IS 'Cizí klíč na pole id do tabulky c_gui_version.';

COMMENT ON CONSTRAINT pkey__t_auxiliary_data ON @extschema@.t_auxiliary_data IS 'Jednoznačný identifikátor, primární klíč tabulky';

ALTER TABLE @extschema@.t_auxiliary_data 
	ADD CONSTRAINT ukey__t_auxiliary_data
	UNIQUE (config_collection, config, ident_point, ext_version);
COMMENT ON CONSTRAINT ukey__t_auxiliary_data ON @extschema@.t_auxiliary_data
IS 'Jednoznačný identifikátor, pro každou konfiguraci, pro každou konfiguraci kategorie úhrnu, pro každou identifikaci bodu bodové vrstvy a pro každou verzi extenze nfiesta_gisdata může existovat pouze jeden platný záznam pomocné proměnné.';

ALTER TABLE @extschema@.t_auxiliary_data
	ADD CONSTRAINT fkey__t_auxiliary_data__t_config_collection FOREIGN KEY (config_collection)
	REFERENCES @extschema@.t_config_collection (id) MATCH SIMPLE
	ON UPDATE CASCADE ON DELETE CASCADE;
COMMENT ON CONSTRAINT fkey__t_auxiliary_data__t_config_collection ON @extschema@.t_auxiliary_data
IS 'Cizí klíč do tabulky t_config_collection.';

ALTER TABLE @extschema@.t_auxiliary_data
	ADD CONSTRAINT fkey__t_auxiliary_data__t_config FOREIGN KEY (config)
	REFERENCES @extschema@.t_config (id) MATCH SIMPLE
	ON UPDATE CASCADE ON DELETE CASCADE;
COMMENT ON CONSTRAINT fkey__t_auxiliary_data__t_config ON @extschema@.t_auxiliary_data
IS 'Cizí klíč do tabulky t_config.';

ALTER TABLE @extschema@.t_auxiliary_data
	ADD CONSTRAINT fkey__t_auxiliary_data__c_ext_version FOREIGN KEY (ext_version)
	REFERENCES @extschema@.c_ext_version (id) MATCH SIMPLE
	ON UPDATE CASCADE ON DELETE CASCADE;
COMMENT ON CONSTRAINT fkey__t_auxiliary_data__c_ext_version ON @extschema@.t_auxiliary_data
IS 'Cizí klíč do tabulky c_ext_version.';

ALTER TABLE @extschema@.t_auxiliary_data
	ADD CONSTRAINT fkey__t_auxiliary_data__c_gui_version FOREIGN KEY (gui_version)
	REFERENCES @extschema@.c_gui_version (id) MATCH SIMPLE
	ON UPDATE CASCADE ON DELETE CASCADE;
COMMENT ON CONSTRAINT fkey__t_auxiliary_data__c_gui_version ON @extschema@.t_auxiliary_data
IS 'Cizí klíč do tabulky c_gui_version.';

CREATE INDEX fki__t_auxiliary_data__t_config_collection
  ON @extschema@.t_auxiliary_data
  USING btree
  (config_collection);
COMMENT ON INDEX @extschema@.fki__t_auxiliary_data__t_config_collection
  IS 'Index přes cizí klíč fkey__t_auxiliary_data__t_config_collection.';

CREATE INDEX fki__t_auxiliary_data__t_config
  ON @extschema@.t_auxiliary_data
  USING btree
  (config);
COMMENT ON INDEX @extschema@.fki__t_auxiliary_data__t_config
  IS 'Index přes cizí klíč fkey__t_auxiliary_data__t_config.';

CREATE INDEX fki__t_auxiliary_data__c_ext_version
  ON @extschema@.t_auxiliary_data
  USING btree
  (ext_version);
COMMENT ON INDEX @extschema@.fki__t_auxiliary_data__c_ext_version
  IS 'Index přes cizí klíč fkey__t_auxiliary_data__c_ext_version.';

CREATE INDEX fki__t_auxiliary_data__c_gui_version
  ON @extschema@.t_auxiliary_data
  USING btree
  (gui_version);
COMMENT ON INDEX @extschema@.fki__t_auxiliary_data__c_gui_version
  IS 'Index přes cizí klíč fkey__t_auxiliary_data__c_gui_version.';

CREATE INDEX idx__t_auxiliary_data__ident_point
  ON @extschema@.t_auxiliary_data
  USING btree
  (ident_point);
COMMENT ON INDEX @extschema@.idx__t_auxiliary_data__ident_point
  IS 'B-tree index na sloupci ident_point.';
  
ALTER TABLE @extschema@.t_auxiliary_data OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE @extschema@.t_auxiliary_data TO adm_nfiesta_gisdata;
GRANT SELECT, UPDATE, INSERT, TRUNCATE, DELETE ON TABLE @extschema@.t_auxiliary_data TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE @extschema@.t_auxiliary_data TO public;
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- FUNKCE --
---------------------------------------------------------------------------------------------------;

-- <function name="fn_get_configs4aux_data_app" schema="@extschema@" src="functions/extschema/fn_get_configs4aux_data_app.sql">
----------------------------------------------------------------------
----------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_configs4aux_data_app(integer, integer, boolean)

CREATE OR REPLACE FUNCTION @extschema@.fn_get_configs4aux_data_app
(
	_config_collection	integer,		-- cizi klic na pole id do tabulky t_config_collection, jde o idecko konfigurace bodoveho protinani [config_function musi byt 600]
	_gui_version		integer,
	_recount		boolean DEFAULT FALSE
)
RETURNS TABLE
(
	step			integer,
	config_collection	integer,
	config			integer,
	intersects		boolean
)
AS
$BODY$
DECLARE
	_ref_id_layer_points		integer;
	_ref_id_total			integer;
	_ref_id_total_categories	integer[];
	_ref_id_total_config_query	integer[];
	_config_function		integer;
	_aggregated			boolean;
	_ext_version_current		integer;
	_ext_version_current_label	text;
	_intersects			boolean;
	_config_id_reference		integer;
	_config_id_base			integer;
	_config_collection_base		integer;
	_check				integer;
	_check_exists			boolean;
	_complete			character varying;
	_categories			character varying;
	_config_ids_text		text;
	_config_ids_length		integer;
	_config_ids			integer[];
	_config_ids4check		integer[];
	_check_count_config_ids_i	integer;
	_check_count_config_ids		integer[];
	_check_ident_point_i		integer;
	_res_step			integer[];
	_res_config_collection		integer[];
	_res_config			integer[];
	_res_intersects			boolean[];
	_res_config_query		integer[];
	_res_check_exists		boolean[];
	_q				text;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_configs4aux_data_app: Vstupni argument _config_collection nesmi byt NULL!';
	END IF;

	IF _gui_version IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_configs4aux_data_app: Vstupni argument _gui_version nesmi byt NULL!';
	END IF;

	IF _recount IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_configs4aux_data_app: Vstupni argument _recount nesmi byt NULL!';
	END IF;
	-------------------------------------------------------------------------------------------
	-- zjisteni referencnich ID pro vrstvu bodu a konfiguraci uhrnu
	SELECT
		tcc.ref_id_layer_points,
		tcc.ref_id_total
	FROM
		@extschema@.t_config_collection AS tcc
	WHERE
		tcc.id = _config_collection
	INTO
		_ref_id_layer_points,	-- cizi klic na pole id do t_config_collection => jde o konfiguraci bodove vrstvy [config_function = 500]
		_ref_id_total;		-- cizi klic na pole id do t_config_collection => jde o konfiguraci UHRNU
	-------------------------------------------------------------------------------------------
	IF _ref_id_layer_points IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_configs4aux_data_app: Pro vstupni argument _config_collection = % nenalezena v konfiguraci reference na konfiguraci bodove vrstvy!',_config_collection;
	END IF;
	-------------------------------------------------------------------------------------------
	IF _ref_id_total IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_configs4aux_data_app: Pro vstupni argument _config_collection = % nenalezena v konfiguraci reference na konfiguraci uhrnu!',_config_collection;
	END IF;
	-------------------------------------------------------------------------------------------
	-- zjisteni konfiguracnich KATEGORII a hodnot CONFIG_QUERY jednotlivych konfiguracnich
	-- kategorii pro REFERENCNI IDecko UHRNU [respektive pro interni promennou _ref_id_total]
	SELECT
		array_agg(tc.id ORDER BY tc.id) AS id,
		array_agg(tc.config_query ORDER BY tc.id) AS config_query
	FROM
		@extschema@.t_config AS tc
	WHERE
		tc.config_collection = _ref_id_total
	INTO
		_ref_id_total_categories,	-- seznam konfiguracnich kategorii referencniho idecka uhrnu [jde o seznam cizich klicu tabulky t_config]
		_ref_id_total_config_query;	-- seznam hodnot config_query u jednotlivych konfiguracnich kategorii referencniho idecka uhrnu
	-------------------------------------------------------------------------------------------
	IF _ref_id_total_categories IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % (ID konfigurace uhrnu) nenalezena v konfiguraci zadna jeji kategorie (ID v tabulce t_config)!',_ref_id_total;
	END IF;
	-------------------------------------------------------------------------------------------
	IF _ref_id_total_config_query IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % (ID konfigurace uhrnu) nenalezena v konfiguraci zadna hodnota config_query v tabulce t_config!',_ref_id_total;
	END IF;
	-------------------------------------------------------------------------------------------
	-- zjisteni config_function a aggregated pro _ref_id_total
	SELECT
		config_function,
		aggregated
	FROM
		@extschema@.t_config_collection
	WHERE
		id = _ref_id_total
	INTO
		_config_function,	-- hodnota config_function referencniho idecka uhrnu [muze nabyt hodnot 100,200,300,400]
		_aggregated;		-- identifikace, zda referencni idecko je nebo neni agregovana skupina
	-------------------------------------------------------------------------------------------
	-- nalezeni nejaktualnejsi verze extenze pro vstupni verzi GUI aplikace
	select max(ext_version) FROM @extschema@.cm_ext_gui_version
	WHERE gui_version = _gui_version
	INTO _ext_version_current;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_current
	INTO _ext_version_current_label;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	-- CYKLUS po jednotlivych konfiguracnich KATEGORIICH referencniho idecka uhrnu
	FOR i IN 1..array_length(_ref_id_total_categories,1)
	LOOP
		-----------------------------------------------------------------------------------
		-- konrola vyplnenosti hodnoty config_query u konfiguracni kategorie
		IF _ref_id_total_config_query[i] IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 08: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % (ID konfigurace uhrnu) neni u nektere jeji kategorie vyplnena hodnota config_query v tabulce t_config!',_ref_id_total;
		END IF;
		-----------------------------------------------------------------------------------
		-- proces naplneni promenne _intersects
		IF	(
			_config_function = ANY(array[100,200])	AND
			_ref_id_total_config_query[i] = 100 AND
			_aggregated = FALSE
			)
		THEN
			_intersects := TRUE;	-- urcuje tzv. protinaci konfiguracni kategorii [pujde bud o konfiguracni kategorii vektoru nebo rastru a hlavne musi jit o config_query = 100, neni to ani soucet ani doplnek]
		ELSE
			_intersects := FALSE;
		END IF;
		-----------------------------------------------------------------------------------
		-- proces zjisteni hodnoty _config_id_base a hodnoty _config_collection_base
		IF _ref_id_total_config_query[i] = 500 -- konfiguracni kategorie uhrnu je REFERENCE
		THEN
			-- config_id_base
			SELECT tc.categories::integer -- konfiguracni idecko uhrnu neagregovane, zakladni skupiny
			FROM @extschema@.t_config AS tc
			WHERE tc.id = _ref_id_total_categories[i]
			INTO _config_id_reference;

			IF _config_id_reference IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 09: fn_get_configs4aux_data_app: U argumentu _ref_id_total = % (ID konfigurace uhrnu), je jeho kategorie _ref_id_total_config_query[i] = % config_query 500 (reference) a v konfiguraci [v tabulce t_config] nema nakonfigurovanou referenci!',_ref_id_total,_ref_id_total_config_query[i];
			END IF;

			_config_id_base := _config_id_reference; -- [idecko konfiguracni kategorie zakladni skupiny] => dulezite pro hledani v t_axiliary_data

			-- config_collection_base
			SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
			WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _config_id_base)
			AND tcc.ref_id_layer_points = _ref_id_layer_points
			INTO _config_collection_base; 	-- [idecko konfigurace bodoveho protinani, jehoz referencni idecko ref_id_total odpovida idecku,
							-- ktere ma v sobe v konfiguraci zahrnutou konfiguracni kategorii uhrnu]
							-- => dulezite pro hledani v t_auxiliary_data
		ELSE
			-- vetev, kdy konfiguracni kategorie uhrnu NENI REFERENCE
			_config_id_base := _ref_id_total_categories[i]; -- dulezite pro hledani v t_auxiliary_data
			_config_collection_base := _config_collection; -- jde o vstupni idecko config_collection, jde zde o idecko konfigurace bodoveho protinani, dulezite pro hledani v t_auxiliary_data
		END IF;
		-----------------------------------------------------------------------------------
		-- 1. KONTROLA EXISTENCE v DB
		-----------------------------------------------------------------------------------
		IF _recount = FALSE
		
		THEN	-- kontrola => vetev pro VYPOCET bodu
		
			-- => kontrola, ze dane _config_collection, dane _config a pro danou _ext_version_current JE nebo NENI v DB vypocitano [poznamka: verze u vsech bodu bude vzdy stejna !!!]
			WITH
			w1 AS	(
				SELECT tad.* FROM @extschema@.t_auxiliary_data AS tad
				WHERE tad.config_collection = _config_collection_base
				AND tad.config = _config_id_base
				AND tad.ext_version = _ext_version_current
				)
			SELECT count(*) FROM w1
			INTO _check;

			-- pokud protnuti pro danou konfiguraci, kategorii a verzi v DB    existuje => do vystupu funkce tato konfigurace NE-PUJDE
			-- pokud protnuti pro danou konfiguraci, kategorii a verzi v DB NE-existuje => do vystupu funkce tato konfigurace    PUJDE
			
			IF _check > 0
			THEN
				-- vetev kdy uz to v DB existuje
				_check_exists := TRUE;
			ELSE
				IF _ref_id_total_config_query[i] = 500	-- REFERENCE
				THEN
					RAISE EXCEPTION 'Chyba 10: fn_get_configs4aux_data_app: U argumentu _ref_id_total = % (ID konfigurace uhrnu), je jeho kategorie _ref_id_total_config_query[i] = % reference, pro kterou ale neni doposud proveden vypocet kategorie z neagregovane konfigurace uhrhu!',_ref_id_total,_ref_id_total_config_query[i];
				ELSE
					_check_exists := FALSE;
				END IF;
			END IF;
		ELSE
			/*
			-- kontrola => vetev pro PREPOCET bodu
		
			-- => kontrola, ze dane _config_id je pro jakoukoliv ext_version uz v DB vypocitano a musi odpovidat poctu bodu dane nakonfigurovane vrstvy
			SELECT max(tad.ext_version) FROM gisdata.t_auxiliary_data AS tad
			WHERE tad.config = _config_id_base
			INTO _max_ext_version;

			IF _max_ext_version IS NULL	-- pokud neni nalezena ext_version pro dane config_id_base tak je jasne ze jeste dane config_id_base nebylo pocitano
			THEN
				IF _config_query = 500
				THEN
					RAISE EXCEPTION 'Chyba xx: fn_get_configs4aux_data_app: Vstupni _config_id = % je reference na _config_id = % a pro neho jeste v tabulce t_auxiliary_data neexistuje hodnota value, prepocet neni mozny!',_config_id,_config_id_base;
				ELSE
					RAISE EXCEPTION 'Chyba xx: fn_get_configs4aux_data_app: Vstupni _config_id = % je soucet nebo doplnek a pro konfiguraci [z complete/categories] = % a pro neho jeste v tabulce t_auxiliary_data neexistuje hodnota value, prepocet neni mozny!',_config_id,_config_id_base;
				END IF;		
			END IF;

			-- kontrola na pocet bodu [mozna je tato kontrola zde navic]
			SELECT count(tad.*) FROM gisdata.t_auxiliary_data AS tad
			WHERE tad.config = _config_id_base
			AND tad.ext_version = _max_ext_version
			INTO _check;

			IF _check != _config_points
			THEN
				IF _config_query = 500
				THEN
					RAISE EXCEPTION 'Chyba xx: fn_get_configs4aux_data_app: Vstupni _config_id = % je reference na _config_id = % a pro prepocet nebylo pro ext_version = % vypocitan pozadovy pocet bodu = %!',_config_id,_config_id_base,_max_ext_version,_config_points;
				ELSE
					RAISE EXCEPTION 'Chyba xx: fn_get_configs4aux_data_app: Vstupni _config_id = % je soucet nebo doplnek a pro konfiguraci [z complete/categories] = % a pro prepocet nebylo pro ext_version = % vypocitan pozadovy pocet bodu = %!',_config_id,_config_id_base,_max_ext_version,_config_points;
				END IF;
			END IF;
			*/
		END IF;
		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------


		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------
		-- sestaveni dat do vysledneho _q pro RETURN QUERY
		IF i = 1
		THEN
			_res_step := array[i];
			_res_config_collection := array[_config_collection];
			_res_config := array[_ref_id_total_categories[i]];
			_res_intersects := array[_intersects];
			_res_config_query := array[_ref_id_total_config_query[i]];
			_res_check_exists := array[_check_exists];
		ELSE
			_res_step := _res_step || array[i];
			_res_config_collection := _res_config_collection || array[_config_collection];
			_res_config := _res_config || array[_ref_id_total_categories[i]];
			_res_intersects := _res_intersects || array[_intersects];
			_res_config_query := _res_config_query || array[_ref_id_total_config_query[i]];
			_res_check_exists := _res_check_exists || array[_check_exists];
		END IF;
								
	END LOOP;

	-------------------------------------------------------------------------------------------
	-- sestaveni vysledneho _q pro RETURN QUERY
	-------------------------------------------------------------------------------------------
	_q :=	'
		WITH
		w AS	(
			SELECT
				unnest($1) AS step,
				unnest($2) AS config_collection,
				unnest($3) AS config,
				unnest($4) AS intersects,
				unnest($5) AS config_query,
				unnest($6) AS check_exists
			)
		SELECT
			w.step,
			w.config_collection,
			w.config,
			w.intersects
		FROM
			w
		WHERE
			w.config_query != 500 AND w.check_exists = FALSE
		ORDER
			BY w.step, w.config
		';
	-------------------------------------------------------------------------------------------		
	-------------------------------------------------------------------------------------------
	-- !!! pozor, protinaji se jen tyto konfigurace: (_config_query = 100 a _config_function = ANY(array[100,200]) AND _aggregated = false)
	-------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||''
	USING
		_res_step,			-- $1
		_res_config_collection,		-- $2
		_res_config,			-- $3
		_res_intersects,		-- $4
		_res_config_query,		-- $5
		_res_check_exists;		-- $6
	--------------------------------------------------------------------------------------------
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,boolean) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,boolean) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,boolean) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,boolean) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,boolean) IS
'Funkce vraci seznam konfiguraci pro funkci fn_get_aux_data_app.';


-- </function>


-- <function name="fn_get_aux_data_app" schema="@extschema@" src="functions/extschema/fn_get_aux_data_app.sql">
----------------------------------------------------------------------
----------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_aux_data_app(integer, integer, integer, boolean)
  
CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_data_app
(
	_config_collection 	integer,
	_config			integer,
	_intersects		boolean		-- intersects nam zde rika u jake konfigurace se bude nebo nebude provadet protinani s vrstvou
)
RETURNS TABLE
(
	config_collection		integer,
	config				integer,
	ident_point			integer,
	value				double precision,
	ext_version			integer
) AS
$BODY$
DECLARE
	_ext_version_label_system			text;
	_ext_version_current				integer;
	_config_function_600				integer;
	_ref_id_layer_points				integer;
	_config_collection_4_config			integer;
	_config_query					integer;
	_vector						integer;			
	_raster						integer;
	_raster_1					integer;
	_band						integer;
	_reclass					integer;		
	_condition					character varying;
	_config_function				integer;
	_schema_name					character varying;
	_table_name					character varying;
	_column_ident					character varying;
	_column_name					character varying;
	_unit						double precision;
	_q						text;
	_ids4comb					integer[];
	_config_collection_transform_comb_i		integer;
	_config_collection_transform_comb		integer[];
	_check_ident_point				integer;
	_complete					character varying;
	_categories					character varying;
	_config_ids_text_complete			text;
	_complete_length				integer;
	_config_ids_complete				integer[];
	_config_ids4check_complete			integer[];
	_complete_exists				boolean[];
	_config_ids_text_categories			text;
	_categories_length				integer;
	_config_ids_categories				integer[];
	_config_ids4check_categories			integer[];
	_categories_exists				boolean[];
	_config_ids					integer[];
	_complete_categories_exists			boolean[];
	_config_collection_transform_i			integer;
	_aggregated					boolean;
	_config_collection_transform			integer[];
	_check_count_config_ids_i			integer;
	_check_count_config_ids				integer[];
	_check_ident_point_i				integer;
	_config_ids_4_complete_array			integer[];
	_config_collection_4_complete_array		integer[];
	_config_ids_4_categories_array			integer[];
	_config_collection_4_categories_array		integer[];
	_aggregated_200					boolean;
	_check_categories_i				double precision[];	
BEGIN 
	-------------------------------------------------------------
	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_data_app: Vstupni argument _config_collection nesmi byt NULL!';
	END IF;

	IF _config IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_data_app: Vstupni argument _config nesmi byt NULL!';
	END IF;

	IF _intersects IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_data_app: Vstupni argument _intersects nesmi byt NULL!';
	END IF;	
	-------------------------------------------------------------
	-------------------------------------------------------------
	-- proces ziskani aktualni extenze nfiesta_gisdata
	SELECT extversion FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_data_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_aux_data_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;	
	-------------------------------------------------------------
	-------------------------------------------------------------	
	-- ziskani promennych z konfigurace tabulky t_config_collection pro _config_collection
	SELECT
		tcc.config_function,
		tcc.ref_id_layer_points
	FROM
		@extschema@.t_config_collection AS tcc
	WHERE
		id = _config_collection
	INTO
		_config_function_600,
		_ref_id_layer_points;
	-------------------------------------------------------------
	-- kontrola ziskanych promennych jestli nejsou NULL
	IF _config_function_600 IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_aux_data_app: Pro vstupni argument config_collection = % nenalezen zaznam config_function v tabulce t_config_collection!',_config_collection;
	END IF;

	IF _ref_id_layer_points IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_aux_data_app: Pro vstupni argument config_collection = % nenalezen zaznam ref_id_layer_points v tabulce t_config_collection!',_config_collection;
	END IF;
	-------------------------------------------------------------
	-- kontrola, ze config_function musi byt hodnota 600
	IF _config_function_600 IS DISTINCT FROM 600
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_get_aux_data_app: Pro vstupni argument config_collection = % nalezen v tabulce t_config_collection zaznam config_function, ktery ale neni hodnota 600!',_config_collection;
	END IF;	
	-------------------------------------------------------------
	-- ziskani promennych z konfigurace tabulky t_config pro _config
	SELECT
		tc.config_collection,
		tc.config_query,
		tc.vector,
		tc.raster,
		tc.raster_1,
		tc.band,
		tc.reclass,		
		tc.condition
	FROM
		@extschema@.t_config AS tc
	WHERE
		tc.id = _config
	INTO
		_config_collection_4_config,
		_config_query,
		_vector,
		_raster,
		_raster_1,
		_band,
		_reclass,
		_condition;
	-------------------------------------------------------------
	-- kontrola ziskanych promennych jestli nejsou NULL
	IF _config_collection_4_config IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_aux_data_app: Pro vstupni argument config = % nenalezen zaznam config_collection v tabulce t_config!',_config;
	END IF;	
		
	IF _config_query IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 10: fn_get_aux_data_app: Pro vstupni argument config = % nenalezen zaznam config_query v tabulce t_config!',_config;
	END IF;	
	-------------------------------------------------------------
	-- ziskani promennych z konfigurace tabulky t_config_collection pro _config_collection_4_config
	SELECT
		tcc.config_function,
		tcc.schema_name,
		tcc.table_name,
		tcc.column_ident,
		tcc.column_name,
		tcc.unit
	FROM
		@extschema@.t_config_collection AS tcc
	WHERE
		tcc.id = _config_collection_4_config
	INTO
		_config_function,
		_schema_name,
		_table_name,
		_column_ident,
		_column_name,
		_unit;
	-------------------------------------------------------------
	-- kontrola ziskanych promennych jestli nejsou NULL
	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 11: fn_get_aux_data_app: Pro vstupni argument config_collection = % nenalezen zaznam config_function v tabulce t_config_collection!',_config_collection_4_config;
	END IF;
	-------------------------------------------------------------
	-------------------------------------------------------------
	-------------------------------------------------------------	 
	CASE
	WHEN _config_query = 100 -- vetev pro zakladni protinani
	THEN
		-- kontrola ziskanych promennych pro protinani
		IF _config_function = ANY(array[100,200])
		THEN
			-- _schema_name
			IF ((_schema_name IS NULL) OR (trim(_schema_name) = ''))
			THEN
				RAISE EXCEPTION 'Chyba 12: fn_get_aux_data_app: Pro vstupni argument config_collection = % nenalezen zaznam schema_name v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- _table_name
			IF ((_table_name IS NULL) OR (trim(_table_name) = ''))
			THEN
				RAISE EXCEPTION 'Chyba 13: fn_get_aux_data_app: Pro vstupni argument config_collection = % nenalezen zaznam table_name v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- _column_ident
			IF (_column_ident IS NULL) OR (trim(_column_ident) = '')
			THEN
				RAISE EXCEPTION 'Chyba 14: fn_get_aux_data_app: Pro vstupni argument config_collection = % nenalezen zaznam column_ident v tabulce t_config_collection!',_config_collection_4_config;
			END IF;
			
			-- _column_name
			IF (_column_name IS NULL) OR (trim(_column_name) = '')
			THEN
				RAISE EXCEPTION 'Chyba 15: fn_get_aux_data_app: Pro vstupni argument config_collection = % nenalezen zaznam column_name v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- unit
			IF _unit IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 16: fn_get_aux_data_app: Pro vstupni argument config_collection = % nenalezen zaznam unit v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- pokud unit neni NULL => vynasobit vzdy hodnotou 10000 [proc?]
			_unit := _unit * 10000.0;
		END IF;
		-----------------------------------------------------------------------------------
		CASE
			WHEN (_config_function = 100)	-- VECTOR
			THEN
				-- raise notice 'PRUCHOD CQ 100 VEKTOR';
				
				-- kontrola, ze _intersects je opravdu nastaveno na TRUE
				IF _intersects = TRUE
				THEN
					_q := @extschema@.fn_sql_aux_data_vector_app(_config_collection,_config,_schema_name,_table_name,_column_ident,_column_name,_condition);
				ELSE
					RAISE EXCEPTION 'Chyba 17: fn_get_aux_data_app: Jde-li o konfiguraci, ktera je protinaci (config_query = 100), pak vstupni argument _intersects musi byt hodnota TRUE!';
				END IF;

			WHEN (_config_function = 200)	-- RASTER
			THEN
				-- raise notice 'PRUCHOD CQ 100 RASTER';
				
				-- kontrola, ze _intersects je opravdu nastaveno na TRUE
				IF _intersects = TRUE
				THEN			
					_q := @extschema@.fn_sql_aux_data_raster_app(_config_collection,_config,_schema_name,_table_name,_column_ident,_column_name,_band,_reclass,_condition,_unit);
				ELSE
					RAISE EXCEPTION 'Chyba 18: fn_get_aux_data_app: Jde-li o konfiguraci, ktera je protinaci (config_query = 100), pak vstupni argument _intersects musi byt hodnota TRUE!';
				END IF;

			WHEN (_config_function IN (300,400))	-- KOMBINACE
			THEN
				-- raise notice 'PRUCHOD CQ 100 KOMBINACE';
				
				-- zde pouzit jiz ulozena data v tabulce t_auxiliary_data
				-- u kombinaci jde vzdy o SOUCIN dat
				-- vyuzit k tomu promenne _vector,raster,raster_1
				--select * from nfi_results4web.t_config where config_collection in (select id from nfi_results4web.t_config_collection
				--where config_function in (300,400)) and config_query = 100 order by id;
				
				-- spojeni hodnot _vector,_raster,_raster_1 do pole a odstraneni NULL hodnoty z pole
				SELECT array_agg(t.ids)
				FROM (SELECT unnest(array[_vector,_raster,_raster_1]) AS ids) AS t
				WHERE t.ids IS NOT NULL
				INTO _ids4comb;

				IF array_length(_ids4comb,1) != 2
				THEN
					RAISE EXCEPTION 'Chyba 19: fn_get_aux_data_app: Pocet prvku v interni promenne (_ids4comb = %) musi byt 2! Respektive jde-li v konfiguraci o interakci, pak v tabulce t_config u id = % muze byt vyplnena hodnota jen pro kombinaci vector a raster nebo raster a raster_1.',_ids4comb, _config;
				END IF;

				-- proces ziskani ID config_collection pro vector a raster z neagregovane skupiny config_collection 600
				-- a zaroven
				-- kontrola PROVEDITELNOSTI, ze pro kombinaci jsou potrebna data jiz ulozena v DB
				
				FOR i IN 1..array_length(_ids4comb,1)
				LOOP
					-- zde v teto casti jde o CQ 100 a kombinaci, proto neni nutne
					-- se ohlizet na aggregated, ale vzdy se musi provest TRANSFORMACE config_collection
					-- pro VxR nebo pro RxR [obecne receno data pro provedeni kombinace uz musi existovat
					-- v 600vkove skupine]
					
					-- proces nalezeni transformovane config_collection
					SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
					WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _ids4comb[i])
					AND tcc.ref_id_layer_points = _ref_id_layer_points
					INTO _config_collection_transform_comb_i; 

					IF _config_collection_transform_comb_i IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 20: fn_get_aux_data_app: Pro provedeni interakce (konfigurace config_collection = % a config = %) nejsou v tabulce t_auxiliary_data ulozena pozadovana data!',_config_collection,_config;
					END IF;

					-- kontrola proveditelnoti kombinace
					IF	(
						SELECT count(tad.ident_point) = 0
						FROM @extschema@.t_auxiliary_data AS tad
						WHERE tad.config_collection = _config_collection_transform_comb_i
						AND tad.config = _ids4comb[i]
						AND tad.ext_version = _ext_version_current
						)
					THEN
						RAISE EXCEPTION 'Chyba 21: fn_get_aux_data_app: Pro provedeni interakce (konfigurace config_collection = % a config = %) nejsou v tabulce t_auxiliary_data ulozena pozadovana data!',_config_collection,_config;
					END IF;

					IF i = 1
					THEN
						_config_collection_transform_comb := array[_config_collection_transform_comb_i];
					ELSE
						_config_collection_transform_comb := _config_collection_transform_comb || array[_config_collection_transform_comb_i];
					END IF;
				END LOOP;
				
				-- kontrola stejnych ident_point pro kombinaci
				WITH
				w1 AS	(SELECT tad1.ident_point FROM @extschema@.t_auxiliary_data AS tad1 WHERE tad1.config_collection = _config_collection_transform_comb[1] AND tad1.config = _ids4comb[1] AND tad1.ext_version = _ext_version_current),
				w2 AS	(SELECT tad2.ident_point FROM @extschema@.t_auxiliary_data AS tad2 WHERE tad2.config_collection = _config_collection_transform_comb[2] AND tad2.config = _ids4comb[2] AND tad2.ext_version = _ext_version_current),
				w3 AS	(SELECT w1.ident_point FROM w1 EXCEPT SELECT w2.ident_point FROM w2),
				w4 AS	(SELECT w2.ident_point FROM w2 EXCEPT SELECT w1.ident_point FROM w1),
				w5 AS	(SELECT w3.ident_point FROM w3 UNION ALL SELECT w4.ident_point FROM w4)
				SELECT
					count(w5.ident_point) FROM w5 INTO _check_ident_point;

				IF _check_ident_point > 0
				THEN
					RAISE EXCEPTION 'Chyba 22: fn_get_aux_data_app: Pro konfiguraci config_collection = % a config = % nejsou v tabulce t_auxiliary_data vsechna potrebna data!',_config_collection,_config;
				END IF;

				_q :=
					'				
					WITH
					w1 AS	(SELECT tad1.ident_point, tad1.value FROM @extschema@.t_auxiliary_data AS tad1 WHERE tad1.config = $1 AND tad1.config_collection = $6  AND tad1.ext_version = $5),
					w2 AS	(SELECT tad2.ident_point, tad2.value FROM @extschema@.t_auxiliary_data AS tad2 WHERE tad2.config = $2 AND tad2.config_collection = $11 AND tad2.ext_version = $5)

					SELECT
						$3 AS config_collection,
						$4 AS config,
						w1.ident_point,
						(w1.value * w2.value)::double precision AS value,
						$5 AS ext_version
					FROM
						w1 INNER JOIN w2 ON w1.ident_point = w2.ident_point
					';
			ELSE
				RAISE EXCEPTION 'Chyba 23: fn_get_aux_data_app: Neznama hodnota _config_function = %!',_config_function;
		END CASE;
		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------
	WHEN _config_query IN (200,300,400,500) -- vetev pro soucet, doplnky nebo referenci
	THEN	
		IF _config_query = 500
		THEN
			-- raise notice 'PRUCHOD CQ 500';
			
			RAISE EXCEPTION 'Chyba 24: fn_get_aux_data_app: Jde-li o konfiguraci, ktera je referenci (config_query = %), tak u ni se protinani bodu neprovadi!',_config_query;
		END IF;

		---------------------------------------------------------------
		-- KONTROLA PROVEDITELNOSTI
		---------------------------------------------------------------
		-- pro config_query 200,300,400 => kontrola ze v DB jsou ulozena data pro complete a categories

		-- pokud se jedna o konfiguraci souctu nebo doplnku => pak nutna kontrola,
		-- ze complete nebo categories pro danou konfiguraci jsou jiz v DB pritomny
		-- a odpovidaji pochopitelne pozadovane ext_version_current [poznamka: tato
		-- kontrola je stejna jak pro vetev vypocet, tak i pro vetev prepocet]

		-- pokud nejde o agregovanou skupinu => pak se jako config_collection pouzije VSTUPNI config_collection
		-- pokud   jde o agregovanou skupinu => pak se jako config_collection pouzije transformace

		--------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------
		-- zjisteni complete a categories		-- select * from nfi_results4web.t_config order by id;
		SELECT
			tc.complete,
			tc.categories
		FROM
			@extschema@.t_config AS tc
		WHERE
			tc.id = _config
		INTO
			_complete,				-- v dalsim vyvoji pocitat s tim, ze v complete nemusi byt jen jedna hodnota
			_categories;
		-------------------------------------------
		-- _complete muze byt NULL
		-------------------------------------------
		IF _categories IS NULL	-- categories nesmi byt nikdy NULL
		THEN
			RAISE EXCEPTION 'Chyba 25: fn_get_aux_data_app: Pro vstupni argument _config = % nenalezen v tabulce t_config zaznam categories!',_config;
		END IF;
		-------------------------------------------
		-- _complete
		IF _complete IS NOT NULL
		THEN
			-- pridani ke _complete textu array[]
			_config_ids_text_complete := concat('array[',_complete,']');

			-- zjisteni poctu idecek tvorici _complete v
			EXECUTE 'SELECT array_length('||_config_ids_text_complete||',1)'
			INTO _complete_length;	-- TOTO

			-- zjisteni existujicich konfiguracnich idecek pro _complete
			EXECUTE
			'SELECT array_agg(tc.id ORDER BY tc.id)
			FROM @extschema@.t_config AS tc
			WHERE tc.id in (select unnest('||_config_ids_text_complete||'))
			'
			INTO _config_ids_complete;	-- TOTO [pole idecek complete serazenych podle id]

			-- splneni podminky stejneho poctu idecek
			IF _complete_length != array_length(_config_ids_complete,1)
			THEN
				RAISE EXCEPTION 'Chyba 26: fn_get_aux_data_app: Pro nektere nakonfigurovane complete = % neexistuje konfigurace (ID) v tabulce t_config!',_complete;
			END IF;

			-- kontrola zda i obsah je stejny
			EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text_complete||') AS ids) AS t'
			INTO _config_ids4check_complete;

			IF _config_ids4check_complete != _config_ids_complete
			THEN
				RAISE EXCEPTION 'Chyba 27: fn_get_aux_data_app: Pro nektere nakonfigurovane complete = % neexistuje konfigurace (ID) v tabulce t_config!',_complete;
			END IF;

			-- sestaveni _complete_exists
			SELECT array_agg(t.complete_exists) FROM (SELECT TRUE AS complete_exists, generate_series(1,_complete_length) AS id) AS t
			INTO _complete_exists;
			
		ELSE
			_complete_length := 0;
			_config_ids_complete := NULL::integer[];
			_complete_exists := NULL::boolean[];
		END IF;
		-------------------------------------------
		-- _categories
		
		-- bude se pracovat zvlast s _categories
		_config_ids_text_categories := concat('array[',_categories,']');

		-- zjisteni poctu idecek tvorici _categories v
		EXECUTE 'SELECT array_length('||_config_ids_text_categories||',1)'
		INTO _categories_length;	-- TOTO

		-- zjisteni existujicich konfiguracnich idecek pro _categories
		EXECUTE
		'SELECT array_agg(tc.id ORDER BY tc.id)
		FROM @extschema@.t_config AS tc
		WHERE tc.id in (select unnest('||_config_ids_text_categories||'))
		'
		INTO _config_ids_categories;	-- TOTO [pole idecek categories serazenych podle id]

		-- splneni podminky stejneho poctu idecek
		IF _categories_length != array_length(_config_ids_categories,1)
		THEN
			RAISE EXCEPTION 'Chyba 28: fn_get_aux_data_app: Pro nektere nakonfigurovane categories = % neexistuje konfigurace (ID) v tabulce t_config!',_categories;
		END IF;

		-- kontrola zda i obsah je stejny
		EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text_categories||') AS ids) AS t'
		INTO _config_ids4check_categories;

		IF _config_ids4check_categories != _config_ids_categories
		THEN
			RAISE EXCEPTION 'Chyba 29: fn_get_aux_data_app: Pro nektere nakonfigurovane categories = % neexistuje konfigurace (ID) v tabulce t_config!',_categories;
		END IF;

		-- sestaveni _categories_exists
		SELECT array_agg(t.categories_exists) FROM (SELECT FALSE AS categories_exists, generate_series(1,_categories_length) AS id) AS t
		INTO _categories_exists;
		-------------------------------------------
		-- spojeni konfiguracnich idecek _complete/_categories
		IF _complete_length = 0
		THEN
			-- jen categories
			_config_ids := _config_ids_categories;
			_complete_categories_exists := _categories_exists;
		ELSE
			-- _complete || _categories
			_config_ids := _config_ids_complete || _config_ids_categories;
			_complete_categories_exists := _complete_exists || _categories_exists;
		END IF;
		--------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------

		-- _config_ids
		-- _complete_categories_exists => pole s hodnotami true nebo false rikajici zda prvek v _config_ids je complete nebo categories


		------------------------------------------------------------------------------------------------------
		-- CYKLUS KONTROLY PROVEDITELNOSTI => cast zjisteni _config_collection_transform_i do POLE a naplneni promenne _complete_categories boolean[]
		------------------------------------------------------------------------------------------------------
		FOR i IN 1..array_length(_config_ids,1)
		LOOP
			-------------------------------------------------
			-------------------------------------------------
			-- proces zjisteni promenne _config_collection_tranform_i:
			IF _complete_categories_exists[i] = TRUE -- v config_ids je na i-te pozici idecko complete
			THEN
				-- poznamka: complete je v konfiguracich prozatim jen u CQ 400
				
				-- proces nalezeni transformovane config_collection [TRANSFORMACE]
				SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
				WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _config_ids[i])
				AND tcc.ref_id_layer_points = _ref_id_layer_points
				INTO _config_collection_transform_i;

			ELSE	-- v config_ids je na i-te pozici idecko categories
				-- categories se meni podle aggregated	[poznamka: categories mohou byt ze stejne nebo z ruznych config_collection, dale ziskani informace o agregated vychazi podle vstupniho _config]
				-- takze nejprve zjisteni aggregated
				-- zde jde v konfiguracich o CQ 200 nebo 300
				SELECT tcc.aggregated FROM @extschema@.t_config_collection AS tcc
				WHERE tcc.id =	(
						SELECT tc.config_collection
						FROM @extschema@.t_config AS tc
						WHERE tc.id = _config
						)
				INTO _aggregated;

				IF _aggregated = TRUE -- [TRANSFORMACE CC]
				THEN
					-- proces nalezeni transformovane config_collection
					SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
					WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _config_ids[i])
					AND tcc.ref_id_layer_points = _ref_id_layer_points
					INTO _config_collection_transform_i;
				ELSE -- vstupni cc
					_config_collection_transform_i := _config_collection;
				END IF;
			END IF;
			-------------------------------------------------
			-------------------------------------------------
			-- proces sestaveni _config_collection_transform
			IF i = 1
			THEN
				_config_collection_transform := array[_config_collection_transform_i];
			ELSE
				_config_collection_transform := _config_collection_transform || array[_config_collection_transform_i];
			END IF;
			
		END LOOP;
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------

		-- _config_ids
		-- _complete_categories_exists
		-- _config_collection_transform

		------------------------------------------------------------------------------------------------------
		-- CYKLUS KONTROLY PROVEDITELNOSTI => cast zjisteni chybejicich dat v DB [nesmi byt 0]
		------------------------------------------------------------------------------------------------------
		FOR i IN 1..array_length(_config_ids,1)	-- pole _config_ids je spojeni complete/categories, pocet prvku odpovida poctu prvku v _config_collection_transform i _complete_categories
		LOOP										
			SELECT count(tad.id) FROM @extschema@.t_auxiliary_data AS tad
			WHERE tad.config_collection = _config_collection_transform[i]
			AND tad.config = _config_ids[i]
			AND tad.ext_version = _ext_version_current
			INTO _check_count_config_ids_i;

			IF _check_count_config_ids_i = 0
			THEN
				RAISE EXCEPTION 'Chyba 30: fn_get_aux_data_app: V tabulce t_auxiliary_data pro [nektere complete nebo categories] = % a pro config_collection = % schazi data!',_config_ids[i],_config_collection_transform[i];
			END IF;

			IF i = 1
			THEN
				_check_count_config_ids := array[_check_count_config_ids_i];
			ELSE
				_check_count_config_ids := _check_count_config_ids || array[_check_count_config_ids_i];
			END IF;
			
		END LOOP;
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------


		------------------------------------------------------------------------------------------------------
		-- kontrola stejneho poctu bodu u complete a categories
		------------------------------------------------------------------------------------------------------
		IF	(
			SELECT count(tt.ccci) IS DISTINCT FROM 1
			FROM (SELECT DISTINCT t.ccci FROM (SELECT unnest(_check_count_config_ids) AS ccci) AS t) AS tt
			)
		THEN
			RAISE EXCEPTION 'Chyba 31: fn_get_aux_data_app: Pocty bodu v tabulce t_auxiliary_data se pro [nektere complete nebo nektere categories] neshoduji! Jde o vstupni konfiguraci config_collection = % a config = %.',_config_collection,_config;
		END IF;
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------

		-- _config_ids
		-- _complete_categories_exists
		-- _config_collection_transform

		------------------------------------------------------------------------------------------------------
		-- CYKLUS KONTROLY PROVEDITELNOSTI => cast kontroly stejnych ident_point
		------------------------------------------------------------------------------------------------------
		IF array_length(_config_ids,1) > 1	-- config_ids muze obsahovat complete na prvni pozici nebo nemusi
		THEN
			-- kontrola stejnych ident_point pro _config_ids
			FOR i IN 1..(array_length(_config_ids,1) - 1)
			LOOP
				WITH
				w1 AS	(SELECT tad1.ident_point FROM @extschema@.t_auxiliary_data AS tad1 WHERE tad1.config_collection = _config_collection_transform[1] AND tad1.config = _config_ids[1] AND tad1.ext_version = _ext_version_current),
				w2 AS	(SELECT tad2.ident_point FROM @extschema@.t_auxiliary_data AS tad2 WHERE tad2.config_collection = _config_collection_transform[i+1] AND tad2.config = _config_ids[i+1] AND tad2.ext_version = _ext_version_current),
				w3 AS	(SELECT w1.ident_point FROM w1 EXCEPT SELECT w2.ident_point FROM w2),
				w4 AS	(SELECT w2.ident_point FROM w2 EXCEPT SELECT w1.ident_point FROM w1),
				w5 AS	(SELECT w3.ident_point FROM w3 UNION ALL SELECT w4.ident_point FROM w4)
				SELECT
					count(w5.ident_point) FROM w5
				INTO
					_check_ident_point_i;

				IF _check_ident_point_i IS DISTINCT FROM 0
				THEN
					RAISE EXCEPTION 'Chyba 32: fn_get_aux_data_app: U konfigurace config_collection = % a config = % se u kontroly proveditelnosti (cast shoda identifikace bodu indent_point) v tabulce t_auxiliary_data u nektereho complete nebo u nektereho categories neshoduji identifikace bodu ident_point!',_config_collection, _config;
				END IF;
			END LOOP;
		END IF;	
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------

		-- _config_ids			=> pole idecek jen categories nebo pole idecek complete + categories
		-- _config_collection_transform	=> pole idecek config_collection pro jednotlive prvky pole _config_ids
		-- _complete_categories_exists	=> pole identifikace true nebo false zda idecko v poli _config_ids je complete nebo categories

		------------------------------------------------------------------------------------------------------------------------------
		-- proces rozhozeni pole _config_ids a pole _config_collection_transform zpetne na cast complete a categories
		------------------------------------------------------------------------------------------------------------------------------
		FOR i IN 1..array_length(_complete_categories_exists,1)
		LOOP
			IF _complete_categories_exists[i] = TRUE	-- complete
			THEN
				IF i = 1
				THEN
					_config_ids_4_complete_array := array[_config_ids[i]];
					_config_collection_4_complete_array := array[_config_collection_transform[i]];
				ELSE
					_config_ids_4_complete_array := _config_ids_4_complete_array || array[_config_ids[i]];
					_config_collection_4_complete_array := _config_collection_4_complete_array || array[_config_collection_transform[i]];
				END IF;
			ELSE						-- categories
				IF i = 1
				THEN
					_config_ids_4_categories_array := array[_config_ids[i]];
					_config_collection_4_categories_array := array[_config_collection_transform[i]];
				ELSE
					_config_ids_4_categories_array := _config_ids_4_categories_array || array[_config_ids[i]];
					_config_collection_4_categories_array := _config_collection_4_categories_array || array[_config_collection_transform[i]];
				END IF;
			END IF;
		END LOOP;
		---------------------------------------------------------------
		-- kontrola ze delka pole _config_ids_4_complete_array odpovida _complete_length
		-- kontrola ze delka pole _config_collection_4_complete_array odpovida _complete_length
		IF _config_collection_4_complete_array IS NOT NULL
		THEN
			IF array_length(_config_ids_4_complete_array,1) != _complete_length
			THEN
				RAISE EXCEPTION 'Chyba 33: fn_get_aux_data_app: Pocet prvku v interni promenne _config_ids_4_complete_array neodpovida interni promenne _complete_length!';
			END IF;
		
			IF array_length(_config_collection_4_complete_array,1) != _complete_length
			THEN
				RAISE EXCEPTION 'Chyba 34 fn_get_aux_data_app: Pocet prvku v interni promenne _config_collection_4_complete_array neodpovida interni promenne _complete_length!';
			END IF;
		ELSE
			_config_collection_4_complete_array := NULL::integer[];
		END IF;
		---------------------------------------------------------------
		-- kontrola ze delka pole _config_ids_4_categories_array odpovida _categories_length
		-- kontrola ze delka pole _config_collection_4_categories_array odpovida _categories_length
		IF array_length(_config_ids_4_categories_array,1) != _categories_length
		THEN
			RAISE EXCEPTION 'Chyba 35: fn_get_aux_data_app: Pocet prvku v interni promenne _config_ids_4_categories_array neodpovida interni promenne _categories_length!';
		END IF;
			
		IF array_length(_config_collection_4_categories_array,1) != _categories_length
		THEN
			RAISE EXCEPTION 'Chyba 36: fn_get_aux_data_app: Pocet prvku v interni promenne _config_collection_4_categories_array neodpovida interni promenne _categories_length!';
		END IF;
		---------------------------------------------------------------	
		------------------------------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------------------------------

		-- _config_ids				=> pole idecek jen categories nebo pole idecek complete + categories
		-- _config_collection_transform		=> pole idecek config_collection pro jednotlive prvky pole _config_ids
		-- _complete_categories_exists		=> pole identifikace true nebo false zda idecko v poli _config_ids je complete nebo categories

		-- _config_ids_4_complete_array
		-- _config_collection_4_complete_array

		-- _config_ids_4_categories_array
		-- _config_collection_4_categories_array
		
		---------------------------------------------------------------
		
		-- CONFIG_QUERY 200 => soucty jsou opravdu soucty za categories	-- poznamka SOUCET je v konfiguraci vzdy veden jako AGREGACE !!!
										-- podle me, pro soucet uz musi existovat 600 stovka pro dane categories <= defakto jde o transformaci
										-- config_collection_transform se naplni v casti "kontrola proveditelnosti"
										
		IF _config_query = 200 						-- select * from nfi_results4web.t_config where config_query = 200 order by id;
		THEN								-- select * from nfi_results4web.t_config_collection where id in (select config_collection from nfi_results4web.t_config where config_query = 200) order by id;
			-- raise notice 'PRUCHOD CQ 200';

			---------------------------------------------
			-- kontrola, ze CQ 200 je agregace
			SELECT aggregated FROM @extschema@.t_config_collection
			WHERE id = (SELECT ref_id_total FROM @extschema@.t_config_collection WHERE id = _config_collection)
			INTO _aggregated_200;

			IF _aggregated_200 IS DISTINCT FROM TRUE
			THEN
				RAISE EXCEPTION 'Chyba 37: fn_get_aux_data_app: Jde-li o konfiguraci souctu [config_query = 200], pak musi jit vzdy o agregacni skupinu, respektive v konfiguraci musi platit aggregated = TRUE! Jde o vstupni konfiguraci config_collection = % a config = %.',_config_collection,_config;
			END IF;
			---------------------------------------------		

			-- provedeni sumy za categories pro jednotlive body
			_q :=
				'
				WITH
				w1 AS	(
					SELECT
						unnest($10) AS config_collection,
						unnest($9) AS config
					),
				w2 AS	(
					SELECT
						tad.ident_point,
						tad.value
					FROM
						@extschema@.t_auxiliary_data AS tad
					INNER
					JOIN	w1
					
					ON
						tad.config_collection = w1.config_collection
					AND
						tad.config = w1.config
					AND
						tad.ext_version = $5
					),			
				w3 AS 	(
					SELECT
						w2.ident_point,
						sum(w2.value)::double precision AS value
					FROM
						w2
					GROUP
						BY w2.ident_point
					)
				SELECT
					$3 AS config_collection,
					$4 AS config,
					w3.ident_point,
					w3.value,
					$5 AS ext_version
				FROM
					w3
				';			
		END IF;		
		---------------------------------------------------------------
		-- CONFIG_QUERY 300 => zde je to doplnek bud hodnota 1 nebo 0 !!!	-- select * from nfi_results4web.t_config where config_query = 300 order by id;
		IF _config_query = 300
		THEN
			-- raise notice 'PRUCHOD CQ 300';
					
			-- kontrola, ze u vsech categories jsou opravdu hodnoty v intervalu 0 az 1
			FOR i IN 1..array_length(_config_ids_4_categories_array,1)
			LOOP		
				SELECT array_agg(t.value ORDER BY t.value)
				FROM	(
					SELECT DISTINCT tad.value::double precision
					FROM @extschema@.t_auxiliary_data AS tad
					WHERE tad.config_collection = _config_collection_4_categories_array[i]
					AND tad.config = _config_ids_4_categories_array[i]
					AND tad.ext_version = _ext_version_current
					) AS t
				INTO
					_check_categories_i;

				IF _check_categories_i IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 38: fn_get_aux_data_app: Pro categories = % u konfigurace _config_collection = % a _config = % a _ext_version = % schazi data v tabulce t_auxiliary_data!',_config_ids_4_categories_array[i],_config_collection_4_categories_array[i],_config,_ext_version_current;
				END IF;

				FOR i IN 1..array_length(_check_categories_i,1)
				LOOP
					IF	(
						_check_categories_i[i] < 0.0	OR
						_check_categories_i[i] > 1.0
						)
					THEN
						RAISE EXCEPTION 'Chyba 39: fn_get_aux_data_app: Hodnoty values u categories = % u konfigurace _config_collection = % a _config = % a _ext_version = % nejsou v intervalu <0.0;1.0>!',_config_ids_4_categories_array[i],_config_collection_4_categories_array[i],_config,_ext_version_current;
					END IF;
				END LOOP;
				
				--raise notice '_check_categories_i:%',_check_categories_i;
			END LOOP;

			-- provedeni doplnku pro jednotlive body
			_q :=
				'
				WITH
				w1 AS	(
					SELECT
						unnest($10) AS config_collection,
						unnest($9) AS config
					),
				w2 AS	(
					SELECT
						tad.ident_point,
						round(tad.value::numeric,2) AS value	-- hodnoty categories musi byt v intervalu <0.0;1.0>, proto si zde muzu dovolit zaokrouhledni na 2 desetinna mista, protoze jde defakto o doplnek do hodnoty 1, respektive do 100%
					FROM
						@extschema@.t_auxiliary_data AS tad
					INNER
					JOIN	w1
					
					ON
						tad.config_collection = w1.config_collection
					AND
						tad.config = w1.config
					AND
						tad.ext_version = $5
					),
				w3 AS	(
					SELECT
						w2.ident_point,
						(sum(w2.value))::double precision AS value
					FROM
						w2
					GROUP
						BY w2.ident_point
					)
				SELECT
					$3 AS config_collection,
					$4 AS config,
					w3.ident_point,
					(1.0::double precision - w3.value) AS value,
					$5 AS ext_version
				FROM
					w3
				';
		END IF;
		---------------------------------------------------------------

		-- complete nebo categories nesmi byt REFERENCE ?
		-- complete nebo categories zatim to mame vse jako CQ 100
		-- takze pro CQ 400 uz musi byt data v t_auxiliary_data a jako config_colection se pouzije VSTUPNI CC
		-- doplnek u kombinaci VxR nebo RxR
		-- doplnek do rozlohy existujici kategorie
	
		-- CONFIG_QUERY 400 => zde je to (complete - (suma categories))
		IF _config_query = 400
		THEN
			-- raise notice 'PRUCHOD CQ 400';		-- select * from nfi_results4web.t_config where config_query = 400 order by id;
									-- select * from nfi_results4web.t_config_collection where id in (select config_collection from nfi_results4web.t_config where config_query = 400) order by id;

			IF _complete IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 40: fn_get_aux_data_app: Pro konfiguraci kategorie _config = % nenalezena hodnota complete v tabulce t_config!',_config;
			END IF;

			-- provedeni doplnku pro jednotlive body
			_q :=
				'
				WITH
				-----------------------------------------------
				-- categories
				w1 AS	(
					SELECT
						unnest($10) AS config_collection,
						unnest($9) AS config
					),
				w2 AS	(
					SELECT
						tad.ident_point,
						(tad.value)::numeric AS value
					FROM
						@extschema@.t_auxiliary_data AS tad
					INNER
					JOIN	w1
					
					ON
						tad.config_collection = w1.config_collection
					AND
						tad.config = w1.config
					AND
						tad.ext_version = $5
					),
				-----------------------------------------------
				-- complete
				w3 AS	(
					SELECT
						unnest($8) AS config_collection,
						unnest($7) AS config
					),
				w4 AS	(
					SELECT
						tad.ident_point,
						(tad.value)::numeric AS value
					FROM
						@extschema@.t_auxiliary_data AS tad
					INNER
					JOIN	w3
					
					ON
						tad.config_collection = w3.config_collection
					AND
						tad.config = w3.config
					AND
						tad.ext_version = $5
					),			
				-----------------------------------------------
				w5 AS	(
					SELECT
						w2.ident_point,
						sum(w2.value) AS value
					FROM
						w2
					GROUP
						BY w2.ident_point
					),
				-----------------------------------------------
				w6 AS	(
					SELECT
						w4.ident_point,
						sum(w4.value) AS value
					FROM
						w4
					GROUP
						BY w4.ident_point					
					)
				-----------------------------------------------
				SELECT
					$3 AS config_collection,
					$4 AS config,
					w6.ident_point,
					((w6.value - w5.value))::double precision AS value,
					$5 AS ext_version
				FROM
					w6 INNER JOIN w5 ON w6.ident_point = w5.ident_point
				';	
		
		END IF;
		---------------------------------------------------------------	
		-- 200 (v konfiguraci jde o soucet) => zde z ETL skriptu overit jak to je u bodu	=> soucty jsou opravdu soucty za categories
		-- 300 (v konfiguraci jde o doplnek) => zde z ETL skriptu overit jak to je u bodu	=> zde je to doplnek bud hodnota 1 nebo 0
		-- 400 (v konfiguraci jde o doplnek) => zde z ETL skriptu overit jak to je u bodu	=> zde je to (complete - (suma categories))
		-- 500 (v konfiguraci jde o referenci) => pro referenci funkce nesmi byt poustena

	ELSE
		RAISE EXCEPTION 'Chyba 41: fn_get_aux_data_app: Neznama hodnota _config_query = %!',_config_query;
	END CASE;
	-------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||''
	USING
		_ids4comb[1],				-- $1
		_ids4comb[2],				-- $2
		_config_collection,			-- $3
		_config,				-- $4
		_ext_version_current,			-- $5
		_config_collection_transform_comb[1],	-- $6
		_config_ids_4_complete_array,		-- $7
		_config_collection_4_complete_array,	-- $8
		_config_ids_4_categories_array,		-- $9
		_config_collection_4_categories_array,	-- $10
		_config_collection_transform_comb[2];	-- $11		
	-------------------------------------------------------------------------------------------
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


ALTER FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean) IS 'Funkce vraci data pro insert do tabulky t_auxiliary_data.';

-- </function>


-- <function name="fn_sql_aux_data_points_app" schema="@extschema@" src="functions/extschema/fn_sql_aux_data_points_app.sql">
--------------------------------------------------------------------------------;
--	fn_sql_aux_data_points_app
--------------------------------------------------------------------------------;

-- DROP FUNCTION @extschema@.fn_sql_aux_data_points_app(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_data_points_app
(
	_ref_id_layer_points		integer
)
RETURNS text AS
$BODY$
DECLARE
	_schema_name		character varying;
	_table_name		character varying;
	_column_name		character varying;
	_column_ident		character varying;
	_condition		character varying;
	_result			text;
BEGIN
	-----------------------------------------------------------------------------------
	-- config_pts
	IF (_ref_id_layer_points IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_data_points_app: Hodnota parametru _ref_id_layer_points nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	SELECT
		tcc.schema_name,
		tcc.table_name,
		tcc.column_name,
		tcc.column_ident,
		tcc.condition
	FROM
		@extschema@.t_config_collection AS tcc
	WHERE
		tcc.config_function = 500
	AND
		tcc.id = _ref_id_layer_points
	INTO
		_schema_name,
		_table_name,
		_column_name,
		_column_ident,
		_condition;
	-----------------------------------------------------------------------------------	
	-- schema_name
	IF ((_schema_name IS NULL) OR (trim(_schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_data_points_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- _table_name
	IF ((_table_name IS NULL) OR (trim(_table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_data_points_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- _column_name
	IF ((_column_name IS NULL) OR (trim(_column_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_data_points_app: Hodnota parametru _column_name nesmí být NULL.';
	END IF;

	-- _column_ident
	IF ((_column_ident IS NULL) OR (trim(_column_ident) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_data_points_app: Hodnota parametru _column_ident nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- _condition [povoleno NULL]
	IF (_condition IS NULL) OR (trim(_condition) = '')
	THEN
		_condition := NULL;
	END IF;
	-----------------------------------------------------------------------------------
	_result :=
		'SELECT
			#COLUMN_IDENT# AS column_ident,
			#COLUMN_NAME# AS column_geom
		FROM
			#SCHEMA_NAME#.#TABLE_NAME#
		WHERE
			#CONDITION#';

	-- nahrazeni promennych casti v dotazu
	_result := replace(_result, '#SCHEMA_NAME#', _schema_name);
	_result := replace(_result, '#TABLE_NAME#', _table_name);
	_result := replace(_result, '#COLUMN_NAME#', _column_name);
	_result := replace(_result, '#COLUMN_IDENT#', _column_ident);
	_result := replace(_result, '#CONDITION#', coalesce(_condition::character varying, 'TRUE'));
	-----------------------------------------------------------------------------------
	RETURN _result;
	-----------------------------------------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;

ALTER FUNCTION @extschema@.fn_sql_aux_data_points_app(integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_points_app(integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_points_app(integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_points_app(integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_data_points_app(integer)
IS 'Funkce vrací SQL textový řetězec pro ziskani pomocné proměnné z vektorové vrstvy protnutim inventarizacnich bodu.';

-- </function>


-- <function name="fn_sql_aux_data_raster_app" schema="@extschema@" src="functions/extschema/fn_sql_aux_data_raster_app.sql">
--------------------------------------------------------------------------------;
--	fn_sql_aux_data_raster_app
--------------------------------------------------------------------------------;

-- DROP FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_data_raster_app
(
	config_collection		integer,
	config				integer,
	schema_name			character varying,
	table_name			character varying,
	column_ident			character varying,
	column_name			character varying,
	band				integer,
	reclass				integer,
	condition			character varying,
	unit				double precision
)
RETURNS text
LANGUAGE plpgsql
IMMUTABLE
SECURITY INVOKER
AS
$$
DECLARE
	_ref_id_layer_points	integer;
	_q			text;
	_column_text		text;
	_result			text;
BEGIN
	-----------------------------------------------------------------------------------
	-- config_collection
	IF config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_data_raster_app: Hodnota parametru config_collection nesmí být NULL.';
	END IF;

	-- config
	IF config IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_data_raster_app: Hodnota parametru config nesmí být NULL.';
	END IF;
	
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_data_raster_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_data_raster_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_ident
	IF ((column_ident IS NULL) OR (trim(column_ident) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_data_raster_app: Hodnota parametru column_ident nesmí být NULL.';
	END IF;

	-- column_name
	IF ((column_name IS NULL) OR (trim(column_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_data_raster_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- band
	IF (band IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_sql_aux_data_raster_app: Hodnota parametru band nesmí být NULL.';
	END IF;

	-- condition [povoleno NULL]
	IF ((condition IS NULL) OR (trim(condition) = ''))
	THEN
		condition := NULL;
	END IF;

	-- unit
	IF (unit IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_sql_aux_data_raster_app: Hodnota parametru unit nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- zjisteni reference pro bodovou vrstvu
	SELECT tcc.ref_id_layer_points FROM @extschema@.t_config_collection AS tcc
	WHERE tcc.id = config_collection
	INTO _ref_id_layer_points;
	-----------------------------------------------------------------------------------
	IF _ref_id_layer_points IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_sql_aux_data_raster_app: Nenalezena hodnota ref_id_layer_points pro config_collection = %.',config_collection;
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni vnoreneho dotazu pro vrstvu bodu
	_q := (SELECT * FROM @extschema@.fn_sql_aux_data_points_app(_ref_id_layer_points));
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro protnuti bodu z vrstvou
	_result :=
		'
		WITH                                                                    
		w_point_geom AS	('||_q||'),
		-------------------------------------------------------------------
		w_intersects AS
				(
				SELECT
					t1.column_ident,
					CASE
					WHEN t2.#COLUMN_NAME# IS NULL THEN 0.0
					ELSE #VALUE#
					END AS value
				FROM
					w_point_geom AS t1					
				LEFT JOIN
					#SCHEMA_NAME#.#TABLE_NAME# AS t2
				ON
					ST_Convexhull(t2.#COLUMN_NAME#) && t1.column_geom
				AND
					ST_Intersects(ST_Convexhull(t2.#COLUMN_NAME#),t1.column_geom)
				AND
					#CONDITION#
				)
		-------------------------------------------------------------------			
		SELECT
			#CONFIG_COLLECTION# AS config_collection,
			#CONFIG# AS config,
			column_ident AS ident_point,
			coalesce((value::double precision * #UNIT#),0.0::double precision) AS value,
			$5 AS ext_version
		FROM
			w_intersects;
		';
	-----------------------------------------------------------------------------------
	-- nastaveni pripadne reklasifikace
	IF (reclass IS NULL)
	THEN
		_column_text := 't2.#COLUMN_NAME#';
	ELSE
		_column_text := '@extschema@.fn_get_reclass_app(t2.#COLUMN_NAME#,#BAND#,ST_BandPixelType(t2.#COLUMN_NAME#,#BAND#),#RECLASS_VALUE#)';
	END IF;
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	_result := replace(_result, '#VALUE#', concat('ST_Value(',_column_text,',#BAND#,t1.column_geom)'));
	_result := replace(_result, '#CONFIG_COLLECTION#', config_collection::character varying);
	_result := replace(_result, '#CONFIG#', config::character varying);
	_result := replace(_result, '#SCHEMA_NAME#', schema_name);
	_result := replace(_result, '#TABLE_NAME#', table_name);
	_result := replace(_result, '#COLUMN_NAME#', column_name);
	_result := replace(_result, '#BAND#', band::character varying);
	_result := replace(_result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	_result := replace(_result, '#UNIT#', unit::character varying);

	IF (reclass IS NOT NULL)
	THEN
		_result := replace(_result, '#RECLASS_VALUE#', reclass::character varying);
	END IF;
	-----------------------------------------------------------------------------------
	RETURN _result;
	-----------------------------------------------------------------------------------
END;
$$;

ALTER FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision)
OWNER TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision)
TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision)
TO app_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision)
TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_data_raster_app(integer, integer, character varying, character varying, character varying, character varying, integer, integer, character varying, double precision)
IS
'Funkce vrací SQL textový řetězec pro ziskani pomocné proměnné z rasterové vrstvy protinanim bodu.';

-- </function>


-- <function name="fn_sql_aux_data_vector_app" schema="@extschema@" src="functions/extschema/fn_sql_aux_data_vector_app.sql">
--------------------------------------------------------------------------------;
--	fn_sql_aux_data_vector_app
--------------------------------------------------------------------------------;

-- DROP FUNCTION @extschema@.fn_sql_aux_data_vector_app(integer, integer, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_data_vector_app
(
	config_collection	integer,
	config			integer,
	schema_name		character varying,
	table_name		character varying,
	column_ident		character varying,
	column_name		character varying,
	condition		character varying
)
RETURNS text AS
$BODY$
DECLARE
	_ref_id_layer_points	integer;
	_q			text;
	_result			text;
BEGIN
	-----------------------------------------------------------------------------------
	-- config_collection
	IF (config_collection IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_data_vector_app: Hodnota parametru config_collection nesmí být NULL.';
	END IF;
		
	-- config
	IF (config IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_data_vector_app: Hodnota parametru config nesmí být NULL.';
	END IF;
	
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_data_vector_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_data_vector_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_ident
	IF (column_ident IS NULL) OR (trim(column_ident) = '')
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_data_vector_app: Hodnota parametru column_ident nesmí být NULL.';
	END IF;

	-- column_name
	IF (column_name IS NULL) OR (trim(column_name) = '')
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_data_vector_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- condition [povoleno NULL]
	IF (condition IS NULL) OR (trim(condition) = '')
	THEN
		condition := NULL;
	END IF;
	-----------------------------------------------------------------------------------
	-- zjisteni reference pro bodovou vrstvu
	SELECT tcc.ref_id_layer_points FROM @extschema@.t_config_collection AS tcc
	WHERE tcc.id = config_collection
	INTO _ref_id_layer_points;
	-----------------------------------------------------------------------------------
	IF _ref_id_layer_points IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_sql_aux_data_vector_app: Nenalezena hodnota ref_id_layer_points pro config_collection = %.',config_collection;
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni vnoreneho dotazu pro vrstvu bodu
	_q := (SELECT * FROM @extschema@.fn_sql_aux_data_points_app(_ref_id_layer_points));
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro protnuti bodu z vrstvou
	_result :=
		'
		WITH                                                                    
		w_point_geom AS	('||_q||'),
		w_intersects AS
				(
				SELECT
					t1.column_ident,
					CASE WHEN t2.#COLUMN_IDENT# IS NOT NULL THEN 1 ELSE 0 END AS value	-- toto bude platit jen v pripade, ze pri protina vektorove vrstvy s body chceme vysledek ten, ze bod bud lezi v podminkove vrstve (1) nebo nelezi (0)
				FROM
					w_point_geom AS t1
				LEFT JOIN	
					#SCHEMA_NAME#.#TABLE_NAME# AS t2
				ON
					ST_Intersects(t2.#COLUMN_NAME#,t1.column_geom)
				AND
					#CONDITION#
				)			
		SELECT
			#CONFIG_COLLECTION# AS config_collection,
			#CONFIG# AS config,
			column_ident AS ident_point,
			coalesce(value::double precision,0.0::double precision) AS value,
			$5 AS ext_version
		FROM
			w_intersects;
		';
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	_result := replace(_result, '#CONFIG_COLLECTION#', config_collection::character varying);
	_result := replace(_result, '#CONFIG#', config::character varying);
	_result := replace(_result, '#SCHEMA_NAME#', schema_name);
	_result := replace(_result, '#TABLE_NAME#', table_name);
	_result := replace(_result, '#COLUMN_IDENT#', column_name);
	_result := replace(_result, '#COLUMN_NAME#', column_name);
	_result := replace(_result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	-----------------------------------------------------------------------------------
	RETURN _result;
	-----------------------------------------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;

ALTER FUNCTION @extschema@.fn_sql_aux_data_vector_app(integer, integer, character varying, character varying, character varying, character varying, character varying) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_vector_app(integer, integer, character varying, character varying, character varying, character varying, character varying) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_vector_app(integer, integer, character varying, character varying, character varying, character varying, character varying) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_data_vector_app(integer, integer, character varying, character varying, character varying, character varying, character varying) TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_data_vector_app(integer, integer, character varying, character varying, character varying, character varying, character varying)
IS 'Funkce vrací SQL textový řetězec pro ziskani pomocné proměnné z vektorové vrstvy protnutim inventarizacnich bodu.';


-- </function>

---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- setting permisions from sequence t_auxiliary_data_id_seq
---------------------------------------------------------------------------------------------------;

	ALTER SEQUENCE @extschema@.t_auxiliary_data_id_seq OWNER 				TO adm_nfiesta_gisdata;
	GRANT ALL ON SEQUENCE @extschema@.t_auxiliary_data_id_seq 				TO adm_nfiesta_gisdata;
	GRANT USAGE, SELECT ON SEQUENCE @extschema@.t_auxiliary_data_id_seq 			TO app_nfiesta_gisdata;

---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- views
---------------------------------------------------------------------------------------------------;
-- v_auxiliary_data

-- <view name="v_auxiliary_data" schema="export_api" src="views/export_api/v_auxiliary_data.sql">
drop view if exists export_api.v_auxiliary_data;

CREATE OR REPLACE VIEW export_api.v_auxiliary_data
AS
with
w_plots as 		(
			select
				plots.country,
				plots.strata_set,
				plots.stratum,
				plots.panel,
				plots.cluster,
				plots.plot
			from
				nfiesta_etl.plots
			)
,w_config_collection as (
			-- vyber vypocitanych config_collection z tabulky t_auxiliary_data
			-- a doplneni o prislusne atributy z tabulky t_config, kde je hlavne
			-- dulezity atribut config_query, ktery v pripade hodnoty 500 nam 
			-- rika, ze jde o refefenci [pozn. referencni data se v tabulce]
			-- t_auxiliary_data neduplikuji
			select
				a.*,
				b.id as id_config,
				b.config_collection,
				b.config_query,
				b.label,
				b.categories
			from
					(
					select
						id,
						label,
						ref_id_layer_points,
						ref_id_total
					from
						@extschema@.t_config_collection
					where
						id in (select distinct config_collection from @extschema@.t_auxiliary_data)
					) as a
			inner
			join	@extschema@.t_config as b
			
			on		a.ref_id_total = b.config_collection
			)
,w_no_ref as 		(
			-- vyber zaznamu z withu w_config_collection, ktere nejsou referenci [config_query != 500]
			select w_config_collection.* from w_config_collection
			where w_config_collection.config_query is distinct from 500
			)
,w_ref as 		(
			-- vyber zaznamu z withu w_config_collection, ktere jsou referecni [config_query = 500]
			select w_config_collection.* from w_config_collection
			where w_config_collection.config_query = 500
			)
,w_data_no_ref as 	(
			-- vyber dat z tabulky t_auxiliary_data pro zaznamy z withu w_no_ref,
			-- kde jsou lze primo provest pres atribut config_collection = id z withu
			-- w_no_ref a config = id_config z withu w_no_ref
			select
				t_auxiliary_data.id,
				t_auxiliary_data.config_collection as config_collection_res,
				t_auxiliary_data.config as config_res,
				t_auxiliary_data.ident_point as pid,
				t_auxiliary_data.value,
				t_auxiliary_data.ext_version,
				t_auxiliary_data.gui_version
			from @extschema@.t_auxiliary_data
			inner join w_no_ref
			on t_auxiliary_data.config_collection = w_no_ref.id
			and t_auxiliary_data.config = w_no_ref.id_config
			)
,w_ref_attribute as 	(
			-- priprava zaznamu s atributy pro vyber dat z tabulky
			-- t_auxiliary_data pro cast zaznamu, ktere jsou referenci
			select
				w_ref_subselect.*,
				tcc_subselect.id as config_collection4auxiliary_data
			from
					(
					select
						w_ref.*,
						t_config.id as categories_integer,
						t_config.config_collection as config_colletion4cf600
					from
							w_ref
					inner
					join	@extschema@.t_config
					
					on		w_ref.categories = t_config.id::character varying 
					) as
						w_ref_subselect 
			inner
			join	(select * from @extschema@.t_config_collection where config_function = 600) as tcc_subselect
			
			on w_ref_subselect.config_colletion4cf600 = tcc_subselect.ref_id_total						
			)						
,w_data_ref as 		(
			-- vyber dat z tabulky t_auxiliary_data pro zaznamy z withu w_ref,
			-- pricemz vyber dat je proveden pres with w_ref_attribute, kde
			-- klicovymi atributy pro join jsou config_collection4auxiliary_data
			-- a categories_integer
			select
				t_auxiliary_data.id,
				w_ref_attribute.id as config_collection_res,
				w_ref_attribute.id_config as config_res,
				t_auxiliary_data.ident_point as pid,
				t_auxiliary_data.value,
				t_auxiliary_data.ext_version,
				t_auxiliary_data.gui_version						
			from
					@extschema@.t_auxiliary_data
			inner
			join	w_ref_attribute
			
			on t_auxiliary_data.config_collection = w_ref_attribute.config_collection4auxiliary_data
			and t_auxiliary_data.config = w_ref_attribute.categories_integer
			)
,w_no_ref_max as 	(
			-- z withu w_data_nor_ref a z withu w_data_ref se musi pro unikatni klic,
			-- resp pro unikatni kombinaci config_collection,config,ident_point,ext_version
			-- se musi vybrat zaznam pro nejaktualnejsi EXT_VERSION
			select config_collection_res, config_res, pid, max(ext_version) as max_ext_version
			from w_data_no_ref group by config_collection_res, config_res, pid
			)
,w_ref_max as 		(
			select config_collection_res, config_res, pid, max(ext_version) as max_ext_version
			from w_data_ref group by config_collection_res, config_res, pid
			)
,w_data_no_ref_res as 	(
			select
				w_data_no_ref.config_collection_res as config_collection,
				w_data_no_ref.config_res as config,
				w_data_no_ref.pid,
				w_data_no_ref.value
			from w_data_no_ref inner join w_no_ref_max
			on w_data_no_ref.config_collection_res = w_no_ref_max.config_collection_res
			and w_data_no_ref.config_res = w_no_ref_max.config_res
			and w_data_no_ref.pid = w_no_ref_max.pid
			and w_data_no_ref.ext_version = w_no_ref_max.max_ext_version
			)
,w_data_ref_res as 	(
			select
				w_data_ref.config_collection_res as config_collection,
				w_data_ref.config_res as config,
				w_data_ref.pid,
				w_data_ref.value
			from w_data_ref inner join w_ref_max
			on w_data_ref.config_collection_res = w_ref_max.config_collection_res
			and w_data_ref.config_res = w_ref_max.config_res
			and w_data_ref.pid = w_ref_max.pid
			and w_data_ref.ext_version = w_ref_max.max_ext_version
			)
,w_data_res as 		(
			select * from w_data_no_ref_res	union all
			select * from w_data_ref_res
			)
select
		w_plots.*,
		--w_data_res.config_collection,
		t_config_collection.auxiliary_variable,
		--w_data_res.config,
		t_config.auxiliary_variable_category,
		w_data_res.value,
		t_config.label as comment
from
		w_data_res
inner join	w_plots					on w_data_res.pid::character varying = w_plots.plot
left join 	@extschema@.t_config			on w_data_res.config = t_config.id
left join	@extschema@.t_config_collection		on t_config.config_collection = t_config_collection.id
order 
		by w_data_res.config_collection, w_data_res.config, w_data_res.pid
;

-- Permissions

ALTER TABLE export_api.v_auxiliary_data OWNER TO adm_nfiesta_gisdata;
COMMENT ON VIEW export_api.v_auxiliary_data IS 'View with the auxiliary plot data.';

GRANT ALL ON TABLE export_api.v_auxiliary_data TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.v_auxiliary_data TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.v_auxiliary_data TO public;

COMMENT ON COLUMN export_api.v_auxiliary_data.country IS 'Identifier of the country. Foreign key to c_country.';
COMMENT ON COLUMN export_api.v_auxiliary_data.strata_set IS 'Character identifier of strata set.';
COMMENT ON COLUMN export_api.v_auxiliary_data.stratum IS 'Character identifier of stratum.';
COMMENT ON COLUMN export_api.v_auxiliary_data.panel IS 'Character identifier of panel.';
COMMENT ON COLUMN export_api.v_auxiliary_data.cluster IS 'Average of relative plot sampling weights per cluster. Is used to compute pix and pixy.';
COMMENT ON COLUMN export_api.v_auxiliary_data.plot IS 'Identifier of the plot within cluster.';
COMMENT ON COLUMN export_api.v_auxiliary_data.auxiliary_variable IS 'Foreign key to c_auxiliary_variable.';
COMMENT ON COLUMN export_api.v_auxiliary_data.auxiliary_variable_category IS 'Foreign key to c_auxiliary_variable_category.';
COMMENT ON COLUMN export_api.v_auxiliary_data.value IS 'Value of the auxiliary plot data.';
COMMENT ON COLUMN export_api.v_auxiliary_data.comment IS 'Comment.';



-- </view>

---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;

---------------------------------------------------------------------------------------------------;
-- syntax correction in AUX_TOTAL functions
---------------------------------------------------------------------------------------------------;
-- <function name="fn_get_gids4aux_total_app" schema="@extschema@" src="functions/extschema/fn_get_gids4aux_total_app.sql">

-- DROP FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_gids4aux_total_app
(
	_config_id			integer,
	_estimation_cell		integer[],
	_gui_version			integer,
	_recount			boolean DEFAULT FALSE
)
RETURNS TABLE
(
	step				integer,
	config_id			integer,
	estimation_cell			integer,
	gid				integer
) AS
$BODY$
DECLARE
	-- spolecne promenne:
	_config_query					integer;
	_config_collection				integer;
	_configs					integer[];
	
	_ext_version_current				integer;
	_q						text;
	_complete					character varying;
	_categories					character varying;
	_config_ids_text				text;
	_config_ids_length				integer;
	_config_ids					integer[];
	_config_ids4check				integer[];
	_check_avc4categories				integer;
	_config_id_reference				integer;
	_config_id_base					integer;
	_check						integer;
			
	-- promenne pro vypocet:
	_gids4estimation_cell_i				integer[];
	_gids4estimation_cell_pocet			integer;
	_pocet						integer[];
	_max_pocet					integer;
	_gids4estimation_cell				integer[];
	_pocet_gids4estimation_cell			integer;
	_doplnek					integer[];
	_res_estimation_cell				integer[];
	_res_gids					integer[];
	_res_estimation_cell_summarization		integer[];
	_summarization					boolean[];
	
	-- promenne pro prepocet:
	_gids4estimation_cell_i_prepocet		integer[];
	_gids4estimation_cell_pocet_prepocet		integer;
	_pocet_prepocet					integer[];
	_max_pocet_prepocet				integer;
	_gids4estimation_cell_prepocet			integer[];
	_pocet_gids4estimation_cell_prepocet		integer;
	_doplnek_prepocet				integer[];
	_res_estimation_cell_prepocet			integer[][];
	_res_gids_prepocet				integer[][];
	_res_estimation_cell_prepocet_simple		integer[];
	_res_gids_prepocet_simple			integer[];
	_res_estimation_cell_prepocet_simple_original	integer[];
	_ecc4prepocet					integer;
	_eccl4prepocet					integer;
	_gids4estimation_cell_i_prepocet_mezistupen	integer[];
	_gids4estimation_cell_prepocet_mezistupen	integer[];
	_gids_mezistupen_with_vstupni_estimation_cell	integer[];
	_estimation_cells_mezistupen_i			integer[];
	_res_estimation_cell_prepocet_4_summarization	integer[];
	_res_gids_prepocet_4_recount			integer[];
	_last_ext_version_from_table			character varying;
	_last_ext_version_from_db			character varying;
	_res_estimation_cell_prepocet_4_recount		integer[];
	_res_estimation_cell_prepocet_4_check		integer[];

	_ext_version_current_label			character varying;

	_config_query_ii				integer;
	_config_id_reference_ii				integer;
	_config_id_base_ii				integer;
	_check_ii_1					integer;
	_check_ii_2					integer;
	_check_ii_boolean_1				integer[];
	_check_ii_boolean_2				integer[];
	_check_ii					integer;
	_check_ii_boolean				integer[];

	_res_gids_count_count				integer;
	_res_estimation_cell_4_with_i			integer[];
	_res_estimation_cell_4_with			integer[][];	
BEGIN
	--------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_gids4aux_total_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_gids4aux_total_app: Vstupni argument _estimation_cell nesmi byt NULL!';
	END IF;

	IF _gui_version IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_gids4aux_total_app: Vstupni argument _gui_version nesmi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	-- zjisteni konfiguracnich promennych z tabulky t_config pro vstupni _config_id
	SELECT
		tc.config_query,
		tc.config_collection
	FROM
		@extschema@.t_config AS tc
	WHERE
		tc.id = _config_id
	INTO
		_config_query,
		_config_collection;
	--------------------------------------------------------------------------------------------
	IF _config_query IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_gids4aux_total_app: Pro vstupni argument _config_id = % nenalezeno config_query v tabulce t_config!',_config_id;
	END IF;

	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_gids4aux_total_app: Pro vstupni argument _config_id = % nenalezeno config_collection v tabulce t_config!',_config_id;
	END IF;
	--------------------------------------------------------------------------------------------
	-- zjisteni vsech config_id pro _config_collection
	SELECT array_agg(tc.id ORDER BY tc.id) FROM @extschema@.t_config AS tc
	WHERE tc.config_collection = _config_collection
	INTO _configs;
	--------------------------------------------------------------------------------------------
	-- nalezeni nejaktualnejsi verze extenze pro vstupni verzi GUI aplikace
	select max(ext_version) FROM @extschema@.cm_ext_gui_version
	WHERE gui_version = _gui_version
	INTO _ext_version_current;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_current
	INTO _ext_version_current_label;
	--------------------------------------------------------------------------------------------
	IF _recount = TRUE -- prepocet
	THEN	
		-----------------------------------------------------------------------------------
		-- KONTROLA EXISTENCE v DB
		-- A: prepocet   JE mozny pokud pro danou estimation_cell existuji vsechny kategorie dane konfigurace [na verzi extenze nezalezi]
		-- B: prepocet NENI mozny pokud pro danou estimation_cell existuji vsechny kategorie dane konfigurace [na verzi extenze   zalezi]
		-----------------------------------------------------------------------------------
		FOR i IN 1..array_length(_estimation_cell,1)
		LOOP
			FOR ii IN 1..array_length(_configs,1)
			LOOP
				SELECT tc.config_query FROM @extschema@.t_config AS tc
				WHERE tc.id = _configs[ii]
				INTO _config_query_ii;
						
				IF _config_query_ii = 500 -- REFERENCE
				THEN
					SELECT tc.categories::integer
					FROM @extschema@.t_config AS tc
					WHERE tc.id = _configs[ii]
					INTO _config_id_reference_ii;

					IF _config_id_reference_ii IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 06: fn_get_gids4aux_total_app: Kontrolovane _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema ve sloupci categories nakonfigurovanou referenci!',_configs[ii];
					END IF;

					_config_id_base_ii := _config_id_reference_ii;
				ELSE
					_config_id_base_ii := _configs[ii]; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
				END IF;

				-----------------------------------------------
				-- kontrola A: prepocet JE mozny, pokud pro danou estimation_cell existuji vsechny kategorie dane konfigurace [na verzi extenze nezalezi]
				WITH
				w1 AS	(
					SELECT tat.* FROM @extschema@.t_aux_total AS tat
					WHERE tat.estimation_cell = _estimation_cell[i]
					AND tat.config = _config_id_base_ii
					-- na verzi extenze nezalezi
					)
				SELECT count(*) FROM w1	-- pokud je pocet vetsi jak 0 tak zaznam(y) exisuje(i)
				INTO _check_ii_1;

				IF _check_ii_1 > 0
				THEN
					IF ii = 1
					THEN
						_check_ii_boolean_1 := array[0];
					ELSE
						_check_ii_boolean_1 := _check_ii_boolean_1 || array[0];
					END IF;
				ELSE
					IF ii = 1
					THEN
						_check_ii_boolean_1 := array[1];
					ELSE
						_check_ii_boolean_1 := _check_ii_boolean_1 || array[1];
					END IF;
				END IF;
				-----------------------------------------------
				-- kontrola B: prepocet NENI mozny, pokud pro danou estimation_cell existuji vsechny kategorie dane konfigurace [na verzi extenze zalezi]
				WITH
				w1 AS	(
					SELECT tat.* FROM @extschema@.t_aux_total AS tat
					WHERE tat.estimation_cell = _estimation_cell[i]
					AND tat.config = _config_id_base_ii
					AND tat.ext_version = _ext_version_current	-- na verzi extenze zalezi
					)
				SELECT count(*) FROM w1
				INTO _check_ii_2;

				IF _check_ii_2 > 0 -- pokud je pocet vetsi jak 0 tak zaznam(y) exisuje(i)
				THEN
					IF ii = 1
					THEN
						_check_ii_boolean_2 := array[0];
					ELSE
						_check_ii_boolean_2 := _check_ii_boolean_2 || array[0];
					END IF;
				ELSE
					IF ii = 1
					THEN
						_check_ii_boolean_2 := array[1];
					ELSE
						_check_ii_boolean_2 := _check_ii_boolean_2 || array[1];
					END IF;
				END IF;
				-----------------------------------------------
			END LOOP;

			IF	(
				SELECT sum(t.check_ii_boolean_1) > 0
				FROM (SELECT unnest(_check_ii_boolean_1) AS check_ii_boolean_1) AS t
				)
			THEN
				RAISE EXCEPTION 'Chyba 07: fn_get_gids4aux_total_app: Vypocet neni mozny => pro zadanou estimation_cell = % doposud neexistuje v tabulce t_aux_total hodnota aux_total pro nekterou z config_id =  %!',_estimation_cell[i],_configs;
			END IF;


			IF	(
				SELECT sum(t.check_ii_boolean_2) = 0
				FROM (SELECT unnest(_check_ii_boolean_2) AS check_ii_boolean_2) AS t
				)
			THEN
				RAISE EXCEPTION 'Chyba 08: fn_get_gids4aux_total_app: Vypocet neni mozny => pro zadanou estimation_cell = % jiz v tabulce t_aux_total existuji hodnoty aux_total pro config_id =  % v aktualni verzi extenze = %!',_estimation_cell[i],_configs,_ext_version_current;
			END IF;
			
		END LOOP;
		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------
		IF _config_query = ANY(array[100,200,300,400])
		THEN
			-- proces ziskani:
			-- pole poli estimation_cell [jde jen o vstupni estimation_cell (muze jit i o estimation_cell, ktere jsou ZSJ)] <= _res_estimation_cell_prepocet
			-- pole poli ZSJ [jde o gidy, ktere tvori vstupni estimation_cell (gidy se zde mohou opakovat)] <= _res_gids_prepocet
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-- cyklus pro ziskani POCTu u PREPOCTU
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni gidu z tabulky f_a_cell pro i-tou _estimation_cell
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i_prepocet;

				IF _gids4estimation_cell_i_prepocet IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 09: fn_get_gids4aux_total_app: Pro _estimation_cell[i] = % nenalezeny zadne zaznamy v tabulce f_a_cell!',_estimation_cell[i];
				END IF;

				_gids4estimation_cell_pocet_prepocet := array_length(@extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i_prepocet),1);

				IF i = 1
				THEN
					_pocet_prepocet := array[_gids4estimation_cell_pocet_prepocet];
				ELSE
					_pocet_prepocet := _pocet_prepocet || array[_gids4estimation_cell_pocet_prepocet];
				END IF;
				
			END LOOP;
			-----------------------------------------------------
			-- zjisteni nejdelsiho pole _gids4estimation_cell_pocet_prepocet
			SELECT max(t.pocet) FROM (SELECT unnest(_pocet_prepocet) as pocet) AS t
			INTO _max_pocet_prepocet;
			-----------------------------------------------------
			-- cyklus ziskani GIDu nejnizsi urovne pro vstupni _estimation_cell[]
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni gidu z tabulky f_a_cell pro i-tou _estimation_cell
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i_prepocet;

				-- volani funkce fn_get_lowest_gids
				-- funkce by mela vratit seznam gidu nejnizsi urovne
				_gids4estimation_cell_prepocet := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i_prepocet);

				-- zjisteni poctu prvku(gidu) v poli _gids4estimation_cell_prepocet
				_pocet_gids4estimation_cell_prepocet := array_length(_gids4estimation_cell_prepocet,1);

				-- vytvoreni pole s nulama o poctu _max_pocet - _pocet_gids4estimation_cell_prepocet
				SELECT array_agg(t.gid_gs) FROM (SELECT 0 AS gid_gs FROM generate_series(1,(_max_pocet_prepocet - _pocet_gids4estimation_cell_prepocet))) AS t
				INTO _doplnek_prepocet;

				-- proces doplneni 0 tak aby pole _gids4estimation_cell melo delku _max_pocet
				_gids4estimation_cell_prepocet := _gids4estimation_cell_prepocet || _doplnek_prepocet;

				IF i = 1
				THEN
					_res_estimation_cell_prepocet := array[array[_estimation_cell[i]]];	-- do pole poli se ulozi pozadovane vstupni estimation_cell pro prepocet
					_res_gids_prepocet := array[_gids4estimation_cell_prepocet];		-- do pole poli se ulozi pozadovane gidy nejnizsi urovne pro prepocet
				ELSE
					_res_estimation_cell_prepocet := _res_estimation_cell_prepocet || array[array[_estimation_cell[i]]];
					_res_gids_prepocet := _res_gids_prepocet || array[_gids4estimation_cell_prepocet];
				END IF;

			END LOOP;
			-----------------------------------------------------
			-- po sem je hotovo:
			-- _res_estimation_cell_prepocet	=> jde o pole poli s estimation_cell ze vstupu uzivatele urcenych pro prepocet [muze obsahovat i zakladni stavebni jednotku]
								-- pokud to doslo az sem, tak se v poli nemuze vyskytovat estimation_cell nebo defakto ZSJ, ktera je ve shodne verzi jako
								-- je aktualni systemova verze extenze
			-- _res_gids_prepocet			=> jde o pole poli gidu zakladnich stavebnich jednotek pro vstupni estimation_cell urcenych pro prepocet, [vyskyt DUPLICIT]
								-- pokud to doslo az sem, tak se zde mohou vyskytovat ZSJ, ktere uz jsou v databazi ve shodne verzi jako je aktualni
								-- systemova verze extenze => tyto gidy neni nutno prepocitavat, prepocitaji se gidy nizssi verze nez je aktualni systemova
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
	

			-----------------------------------------------------
			-- CONFIG_ID_BASE --
			-----------------------------------------------------
			IF _config_query = 500 -- REFERENCE
			THEN
				SELECT tc.categories::integer
				FROM @extschema@.t_config AS tc
				WHERE tc.id = _config_id
				INTO _config_id_reference;

				IF _config_id_reference IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 10: fn_get_gids4aux_total_app: Vstupni _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema nakonfigurovanou referenci!',_config_id;
				END IF;

				_config_id_base := _config_id_reference;
			ELSE
				_config_id_base := _config_id; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
			END IF;
			-----------------------------------------------------
			-----------------------------------------------------

		
			-----------------------------------------------------
			-- proces kontroly, ze vstupni ESTIMATION_CELL uz
			-- byly alespon jednou v minulosti vypocitany, tzn.
			-- ze na verzi ext_version nezalezi
			-----------------------------------------------------
			-- prevedeni _res_estimation_cell_prepocet na simple pole
			SELECT array_agg(t.recp) FROM (SELECT unnest(_res_estimation_cell_prepocet) AS recp) AS t
			INTO _res_estimation_cell_prepocet_simple;

			-- kontrola pro _res_estimation_cell_prepocet_simple
			FOR i IN 1..array_length(_res_estimation_cell_prepocet_simple,1)
			LOOP
				IF	(
					SELECT count(tat.*) = 0
					FROM @extschema@.t_aux_total AS tat
					WHERE tat.config = _config_id_base
					AND tat.estimation_cell = _res_estimation_cell_prepocet_simple[i]
					-- zde ext_version nehraje roli
					)
				THEN
					RAISE EXCEPTION 'Chyba 11: fn_get_gids4aux_total_app: Pro config_id = % a estimation_cell = % doposud neexistuje zaznam v tabulce t_aux_total. Prepocet tedy neni mozny!',_config_id_base,_res_estimation_cell_prepocet_simple[i];
				END IF;
			END LOOP;
			-----------------------------------------------------
			-----------------------------------------------------


			-----------------------------------------------------
			-- proces kontroly, ze ZSJ, ktere tvori jednotlive
			-- vstupni estimation_cell, uz byly alespon jednou
			-- v minulosti vypocitany, verze extenze zde nehraje
			-- roli		
			-----------------------------------------------------
			-- proces prevedeni _res_gids_prepocet na simple pole
			WITH
			w AS	(
				SELECT DISTINCT t.rgp
				FROM (SELECT unnest(_res_gids_prepocet) AS rgp) AS t
				WHERE t.rgp IS DISTINCT FROM 0
				)
			SELECT array_agg(w.rgp) FROM w
			INTO _res_gids_prepocet_simple;

			-- samotna kontrola pro _res_gids_prepocet_simple
			FOR i IN 1..array_length(_res_gids_prepocet_simple,1)
			LOOP
				-- jde-li o CQ 100, pak kontrola musi probihat
				-- pres gidy, respektive pres sloupec cell
				-- v tabulce t_aux_total
				IF _config_query = 100
				THEN
					IF	(
						SELECT count(tat.*) = 0
						FROM @extschema@.t_aux_total AS tat
						WHERE tat.config = _config_id_base
						AND tat.cell = _res_gids_prepocet_simple[i]
						-- zde ext_version nehraje roli,
						-- poznamka: ZSJ, ktere jsou shodne verze jako je aktualni verze extenze, budou nize v kodu
						-- ze seznamu vyjmuty a do samotneho prepoctu nemusi jit znovu !!!
						)
					THEN
						RAISE EXCEPTION 'Chyba 12: fn_get_gids4aux_total_app: Pro config_id = % a cell = % doposud neexistuje zaznam v tabulce t_aux_total. Prepocet tedy neni mozny!',_config_id_base,_res_gids_prepocet_simple[i];
					END IF;
				END IF;

				-- jde-li o CQ 200,300,400, pak kontrola musi probihat
				-- rovnez pres gid, respektive pres sloupec
				-- cell v tabulce t_aux_total, ale je zde jeste vyjimka
				IF _config_query = ANY(array[200,300,400])
				THEN
					-- pokud je i-ty gid v _res_gids_prepocet_simple ZSJ, ktera je tvorena 2 a vice
					-- geometriemi, pak se nize uvedena kontrola provadet nemusi, a to proto, ze
					-- pro takove ZSJ se pro CQ 200,300,400 do databaze vubec neukladaji
					IF	(
						SELECT count(fac2.gid) = 1
						FROM @extschema@.f_a_cell AS fac2
						WHERE fac2.estimation_cell =
									(
									SELECT fac1.estimation_cell
									FROM @extschema@.f_a_cell AS fac1
									WHERE fac1.gid = _res_gids_prepocet_simple[i]
									)
						)
					THEN
						IF	(
							SELECT count(tat.*) = 0
							FROM @extschema@.t_aux_total AS tat
							WHERE tat.config = _config_id_base
							AND tat.cell = _res_gids_prepocet_simple[i]	
							-- zde ext_version nehraje roli
							-- poznamka: ZSJ, ktere jsou shodne verze jako je aktualni verze extenze, budou nize v kodu
							-- ze seznamu vyjmuty a do samotneho prepoctu nemusi jit znovu !!!
							)
						THEN
							RAISE EXCEPTION 'Chyba 13: fn_get_gids4aux_total_app: Pro config_id = % a cell = % doposud neexistuje zaznam v tabulce t_aux_total. Prepocet tedy neni mozny!',_config_id_base,_res_gids_prepocet_simple[i];
						END IF;
					END IF;
				END IF;
			END LOOP;
			-----------------------------------------------------
			-----------------------------------------------------


			-----------------------------------------------------
			-- zachovani _res_estimation_cell_prepocet_simple
			_res_estimation_cell_prepocet_simple_original := _res_estimation_cell_prepocet_simple;
			-----------------------------------------------------


			-----------------------------------------------------
			-- proces doplneni interni promenne
			-- _res_estimation_cell_prepocet_simple
			-- podrizenymi estimation_cell
			-----------------------------------------------------
			FOR i in 1..array_length(_estimation_cell,1)
			LOOP
				SELECT cec.estimation_cell_collection
				FROM @extschema@.c_estimation_cell AS cec
				WHERE cec.id = _estimation_cell[i]
				INTO _ecc4prepocet;
				
				SELECT cmecc.estimation_cell_collection_lowest
				FROM @extschema@.cm_estimation_cell_collection AS cmecc
				WHERE cmecc.estimation_cell_collection = _ecc4prepocet
				INTO _eccl4prepocet;

				IF _ecc4prepocet = _eccl4prepocet
				THEN
					-- pro prepocet uz se nemusi zjistovat podrizene estimation_cell
					_res_estimation_cell_prepocet_simple := _res_estimation_cell_prepocet_simple; -- datovy typ integer[]
				ELSE
					-- pro prepocet se musi zjistit podrizene estimation_cell
					
					-- zjisteni vsech zakladnich gidu tvorici i-tou estimation_cell
					SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
					WHERE fac.estimation_cell = _estimation_cell[i]
					INTO _gids4estimation_cell_i_prepocet_mezistupen;

					_gids4estimation_cell_prepocet_mezistupen := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i_prepocet_mezistupen);	

					_gids_mezistupen_with_vstupni_estimation_cell := @extschema@.fn_get_gids4under_estimation_cells_app(_gids4estimation_cell_prepocet_mezistupen,_gids4estimation_cell_i_prepocet_mezistupen);

					SELECT array_agg(fac.estimation_cell) FROM @extschema@.f_a_cell AS fac
					WHERE fac.gid IN (SELECT unnest(_gids_mezistupen_with_vstupni_estimation_cell))
					AND fac.estimation_cell IS DISTINCT FROM _estimation_cell[i]
					INTO _estimation_cells_mezistupen_i;

					IF _estimation_cells_mezistupen_i IS NULL
					THEN
						-- RAISE EXCEPTION 'Chyba 11: fn_get_gids4aux_total_app: Interni promenna _estimation_cells_mezistupen_i nesmi byt nikdy NULL!';
						-- tato vyjimka byla zde puvodne chybne, protoze pokud je na vstupu napr. okres, pak mezistupen pro estimation_cell mezi okresem
						-- a KU uz vlastne neni, toto nastava i mezi OPLO a PLO, takze potom to bude takto:
						_res_estimation_cell_prepocet_simple := _res_estimation_cell_prepocet_simple;

						-- chybejici PLO pro OPLO se zde ale musi doplnit => veresit ve funkci fn_get_gids4under_estimation_cells
					ELSE
						_res_estimation_cell_prepocet_simple := _res_estimation_cell_prepocet_simple || _estimation_cells_mezistupen_i;
					END IF;			
				END IF;	
			END LOOP;
			-----------------------------------------------------
			-----------------------------------------------------

			-----------------------------------------------------
			-- distinct hodnot estimation_cell v promenne
			-- _res_estimation_cell_prepocet_simple
			WITH
			w AS	(SELECT distinct t.ecp FROM (SELECT unnest(_res_estimation_cell_prepocet_simple) AS ecp) AS t)
			SELECT array_agg(w.ecp order by w.ecp) FROM w
			INTO _res_estimation_cell_prepocet_simple;

			-- _res_estimation_cell_prepocet_simple [seznam vsech moznych estimation_cell pro prepocet], obsahuje:
			-- v pripade NUTS => vstupni estimation_cell a muzou to byt i ZSJ 
			-- v pripade OPLO => vstupni estimation_cell, rozdrobene ZSJ aplikace neposle, ale napr. pro OPLO se vrati podrizeni PLOcka

			-- takze jde o seznam => VSTUPNI ESTIMATION_CELL + jejich vsechny mozne [ALL] PODRIZENE ESTIMATION_CELL
			-----------------------------------------------------
			

			-----------------------------------------------------
			-- propojeni _res_estimation_cell_prepocet_simple
			-- s databazi a zjisteni estimation_cell, ktere v
			-- databazi jeste pro dane config a ext_version
			-- nejou vypocitany
			-----------------------------------------------------
			WITH
			w1 AS	(
				SELECT DISTINCT tat.estimation_cell
				FROM @extschema@.t_aux_total AS tat
				WHERE tat.config = _config_id_base
				AND tat.cell IS NULL			-- cell se ukladaji jen pro ZSJ, takze toto zajisti ze ZSJ do INTO nepujdou
				AND tat.estimation_cell IN
					(SELECT unnest(_res_estimation_cell_prepocet_simple))
				AND tat.ext_version < _ext_version_current
				),
			w2 AS	(
				SELECT DISTINCT tat.estimation_cell
				FROM @extschema@.t_aux_total AS tat
				WHERE tat.config = _config_id_base
				AND tat.cell IS NULL
				AND tat.estimation_cell IN
					(SELECT unnest(_res_estimation_cell_prepocet_simple))
				AND tat.ext_version = _ext_version_current
									-- tato podminka je zde proto, ze uzivatel mohl pred tim PREPOCET ZASTAVIT,
									-- tzn. ze cast estimation_cell se uz mohlo pro ext_version_current vypocitat,
									-- takze pak do prepoctu musi jit jen estimation_cell, ktere jeste nemaji verzi
									-- _ext_version_current
				),
			w3 AS	(
				SELECT w1.estimation_cell FROM w1 EXCEPT
				SELECT w2.estimation_cell FROM w2
				)
			SELECT array_agg(w3.estimation_cell) FROM w3
			INTO _res_estimation_cell_prepocet_4_summarization;	-- THIS is A -- CQ 100,200,300,400 a NUTS1-4 nebo OPLO,PLO

			-- jde o seznam POZADOVANYCH ESTIMATION_CELL pro SUMARIZACI u PREPOCTU

			-- u CQ 100	a	NUTS1-4		=> to je v poradku
			-- u CQ 100	a	OPLO,PLO 	=> to je v poradku

			-- u CQ 300	a	NUTS1-4		=> to je v poradku <= CQ 100 uz musi existovat
			-- u CQ 300 	a	OPLO,PLO	=> to je v poradku <= CQ 100 uz musi existovat

			-- tzn. pro kontrolu proveditelnosti CQ 300 musi do cyklu jit _res_estimation_cell_prepocet_4_summarization
			-----------------------------------------------------
			-----------------------------------------------------


			--raise notice '_res_estimation_cell_prepocet_4_summarization:%',_res_estimation_cell_prepocet_4_summarization;
			--raise exception 'STOP';


			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-- propojeni seznamu ZSJ s databazi, v pride NUTS1-4
			-- jde o KU, v pripade OPLO,PLO jde o rozdrobene casti
			-- [ze seznamu se musi vyjmout ZSJ, ktere uz jsou ve
			-- shodne ext_version jako je aktualni systemova
			-- verze extenze]

			-- u CQ 100 se ZSJ ukladaji jak pro NUTS1-4 tak pro
			-- OPLO a PLO => takze zde se musi pracovat se
			-- sloupcem cell v tabulce t_aux_total, protoze pro
			-- ZSJ u OPLO,PLO se estimation_cell neuklada

			-- u CQ 200,300,400 a pro NUTS1-4 se ZSJ do t_aux_total
			-- ukladaji => zde se muze pracovat bud se sloupcem cell
			-- nebo se sloupcem estimation_cell
			
			-- u CQ 200,300,400 a pro OPLO,PLO se ZSJ do t_aux_total
			-- NE-ukladaji (proc, jde totiz o ZSJ, ktera sama netvori
			-- celou estimation_cell, jejich aux_total hodnoty se
			-- neukladaji, ukladaji se az jejich sumarizace), zde
			-- se musi pracovat ze sloupce estimation_cell
			-----------------------------------------------------
			IF _config_query = 100
			THEN
				WITH
				w1 AS	(SELECT unnest(_res_gids_prepocet_simple) AS gid),	-- _res_gids_prepocet_simple [ALL KU nebo ALL rozdrobene casti PLO tvorici vstupni estimation_cell pro prepocet]
				w2 AS	(
					SELECT tat.cell
					FROM @extschema@.t_aux_total AS tat
					WHERE tat.config = _config_id_base
					AND tat.cell IN (SELECT w1.gid FROM w1)
					AND tat.ext_version = _ext_version_current
					),
				w3 AS	(
					SELECT
						w1.gid,
						w2.cell
					FROM
						w1 LEFT JOIN w2 ON w1.gid = w2.cell
					)
				SELECT
					array_agg(w3.gid) FROM w3 WHERE w3.cell IS NULL
				INTO
					_res_gids_prepocet_4_recount;	-- THIS is B -- CQ 100 a NUTS1-4 nebo OPLO,PLO
			END IF;

			-- seznam ZSJ [KU nebo rozdrobene casti PLO] POZADOVANE
			-- pro prepocet [jde o gidy, ktere jsou starsi verze nez
			-- je aktualni systemova verze extenze
			-----------------------------------------------------
			-----------------------------------------------------

			
			-----------------------------------------------------
			-----------------------------------------------------
			IF _config_query = ANY(array[200,300,400])
			THEN			
				WITH
				w1 AS	(SELECT unnest(_res_gids_prepocet_simple) AS gid),	-- _res_gids_prepocet_simple [ALL KU nebo ALL rozdrobene casti PLO tvorici vstupni estimation_cell pro prepocet] 
				w2 AS	(-- ALL estimation_cell --				-- vyber estimation_cell z f_a_cell na zaklade gidu (ZSJ)
					SELECT DISTINCT fac.estimation_cell			-- provedeni distinctu estimation_cell
					FROM @extschema@.f_a_cell AS fac
					WHERE fac.gid IN (SELECT w1.gid FROM w1)
					),
					-- vyber (propojeni s DB) estimation_cell co je v DB, zde nejprve nezalezi na verzi ext_version
				w3 AS	(-- DB estimation_cell --
					SELECT tat1.estimation_cell
					FROM @extschema@.t_aux_total AS tat1
					WHERE tat1.config = _config_id_base
					AND tat1.estimation_cell IN (SELECT w2.estimation_cell FROM w2)
					),
				w4 AS	(-- DB estimation_cell + ext_version
					SELECT tat2.estimation_cell
					FROM @extschema@.t_aux_total AS tat2
					WHERE tat2.config = _config_id_base
					AND tat2.estimation_cell IN (SELECT w3.estimation_cell FROM w3)
					AND tat2.ext_version = _ext_version_current
					),
				w5 AS	(
					SELECT
						w3.estimation_cell AS estimation_cell_w3,
						w4.estimation_cell AS estimation_cell_w4
					FROM
						w3 LEFT JOIN w4 ON w3.estimation_cell = w4.estimation_cell
					)
				SELECT
					array_agg(w5.estimation_cell_w3) FROM w5 WHERE w5.estimation_cell_w4 IS NULL
				INTO
					_res_estimation_cell_prepocet_4_recount; -- THIS is C -- CQ 200,300,400 a NUTS1-4 nebo OPLO,PLO
			END IF;

			-- seznam ZSJ [KU nebo cele PLO] kodovanych pres estimation_cell,
			-- ktere jsou POZADOVANY pro prepocet [jde o ZSJ, ktere jsou
			-- starsi verze nez je aktualni systemova verze extenze
			----------------------------------------------------
			----------------------------------------------------
		END IF;
		-----------------------------------------------------
		-----------------------------------------------------
		
		-------------------------------------------
		--raise notice '_res_estimation_cell_prepocet_4_summarization:%',_res_estimation_cell_prepocet_4_summarization;
		--raise notice '_res_gids_prepocet_4_recount:%',_res_gids_prepocet_4_recount;
		--raise notice '_res_estimation_cell_prepocet_4_recount:%',_res_estimation_cell_prepocet_4_recount;
		--raise exception 'STOP';
		-------------------------------------------

		IF _config_query = 100	-- vetev pro zakladni protinani [ZSJ] a sumarizaci
		THEN
			_q :=
				'
				WITH
				---------------------------------------------------------
				w1 AS	(SELECT unnest($8) AS gid),	-- THIS is B
				w2 AS	(
					SELECT
						1 AS step,
						$3 AS config,
						fac.estimation_cell,
						w1.gid
					FROM
						w1
					LEFT
					JOIN	@extschema@.f_a_cell AS fac ON w1.gid = fac.gid -- toto doplni estimation_cell [v aplikaci by se mohlo spojit LABEL a GID]
					),
				---------------------------------------------------------
				w3 AS	(SELECT unnest($9) AS estimation_cell), -- THIS is A
				w4 AS	(
					SELECT
						2 AS step,
						$3 AS config,
						w3.estimation_cell,
						0::integer AS gid
					FROM
						w3
					ORDER
						BY w3.estimation_cell
					),
				w5 AS	(
					SELECT
						w2.step,
						w2.config,
						w2.estimation_cell,
						w2.gid
					FROM
						w2
					ORDER
						BY w2.step, w2.config, w2.gid, w2.estimation_cell
					)
				---------------------------------------------------------
				SELECT * FROM w5 UNION ALL
				SELECT * FROM w4					
				';
			
		ELSE
			-- _config_query = ANY(array[200,300,400,500]) -- vetev pro soucet a doplnky

			-- spojeni _res_estimation_cell_prepocet_4_summarization a _estimation_cell a udelan distinct
			-- proc? vstupni estimation_cell muze obsahovat i estimation_cell ZSJ
			-- _res_estimation_cell_prepocet_4_summarization neobsahuje estimation_cell ZSJ

			-- pridani jeste spojeni _res_estimation_cell_prepocet_4_recount
		
			WITH
			w1 AS	(SELECT unnest(_res_estimation_cell_prepocet_4_summarization) AS estimation_cell), 	-- seznam estimation_cell pro sumarizaci, odpovida co je v DB
			w2 AS	(SELECT unnest(_estimation_cell) AS estimation_cell),					-- vstupni estimation_cell, mohou obsahovat i ZSJ
			w3 AS	(SELECT unnest(_res_estimation_cell_prepocet_4_recount) AS estimation_cell),		-- estimation_cell jako ZSJ, odpovida tomu co je v DB
			w4 AS	(
				SELECT w1.estimation_cell FROM w1
				UNION					-- union provede defakto distinct
				SELECT w2.estimation_cell FROM w2
				UNION
				SELECT w3.estimation_cell FROM w3
				)
			SELECT array_agg(w4.estimation_cell) FROM w4
			INTO _res_estimation_cell_prepocet_4_check;

			-------------------------------------------------------
			-- raise notice '_res_estimation_cell_prepocet_4_check:%',_res_estimation_cell_prepocet_4_check;			
			-------------------------------------------------------

			-- KONTROLA prepocitatelnosti pro CONFIG_QUERY:
			-- 200,300,400	=> jsou v t_aux_total prepocitany data zakladu a odpovidaji posledni verzi
			-- 500		=> je v t_aux_total reference odpovidajici posledni verzi => zde probihaji jen kontroly => vystupem je bud vyjimka nebo 0 radku
		
			-- zjisteni potrebnych promennych z konfigurace pro vetev soucet nebo doplnky
			SELECT
				tc.complete,
				tc.categories
			FROM
				@extschema@.t_config AS tc WHERE tc.id = _config_id
			INTO
				_complete,
				_categories;

			-- raise notice '_complete: %',_complete;
			-- raise notice '_categories: %',_categories;

			-- spojeni konfiguracnich idecek _complete/_categories
			IF _complete IS NULL
			THEN
				_config_ids_text := concat('array[',_categories,']');
			ELSE
				_config_ids_text := concat('array[',_complete,',',_categories,']');
			END IF;

			-- raise notice '_config_ids_text: %',_config_ids_text;

			-- zjisteni poctu idecek tvorici _complete/_categories
			EXECUTE 'SELECT array_length('||_config_ids_text||',1)'
			INTO _config_ids_length;

			-- raise notice '_config_ids_length: %',_config_ids_length;

			-- zjisteni existujicich konfiguracnich idecek pro (_complete/_categories)
			EXECUTE 'SELECT array_agg(tc.id ORDER BY tc.id) FROM @extschema@.t_config AS tc WHERE tc.id in (select unnest('||_config_ids_text||'))'
			INTO _config_ids;

			-- raise notice '_config_ids: %',_config_ids;

			-- splneni podminky stejneho poctu idecek
			IF _config_ids_length = array_length(_config_ids,1)
			THEN
				-- kontrola zda i obsah je stejny
				EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text||') AS ids) AS t'
				INTO _config_ids4check;

				IF _config_ids4check = _config_ids
				THEN
					-- cyklus kontroly, zda pro soucty/doplnky a referenci jsou vypocitany vsechny potrebne
					-- kategorie a odpovidaji posledni ext_version
					-- pozn. zde si uz muzu sahat do tabulky t_aux_total, protoze pro tento typ vypoctu
					-- uz musi existovat zakladni (protinaci) aux_total hodnoty, respektive musely pro
					-- complete nebo categories probehnout _config_query 100 !!!

					--raise notice '_res_estimation_cell_prepocet_4_check: %',_res_estimation_cell_prepocet_4_check;
					
					FOR i IN 1..array_length(_res_estimation_cell_prepocet_4_check,1)
					LOOP
						FOR y IN 1..array_length(_config_ids,1)
						LOOP
							WITH
							w1 AS	(
								SELECT tat.* FROM @extschema@.t_aux_total AS tat
								WHERE tat.config = _config_ids[y]
								AND tat.estimation_cell = _res_estimation_cell_prepocet_4_check[i]
								AND tat.ext_version = _ext_version_current
								)
							SELECT count(*) FROM w1
							INTO _check_avc4categories;

							IF _check_avc4categories != 1
							THEN		
								IF _config_query = 500
								THEN
									RAISE EXCEPTION 'Chyba 14: fn_get_gids4aux_total: Vstupni _config_id = % je reference na _config_id = %	a pro estimation_cell = % neexistuje v tabulce t_aux_total vypocitana hodnota aux_total v nejaktualnejsi verzi ext_version = %!',_config_id_base,_config_ids[y],_res_estimation_cell_prepocet_4_check[i],_ext_version_current_label;
								ELSE
									RAISE EXCEPTION 'Chyba 15: fn_get_gids4aux_total: Vstupni _config_id = % je soucet nebo doplnek	a pro konfiguraci [z complete/categories] = % a pro estimation_cell = % neexistuje v tabulce t_aux_total vypocitana hodnota aux_total v nejaktualnejsi verzi ext_version = %!',_config_id_base,_config_ids[y],_res_estimation_cell_prepocet_4_check[i],_ext_version_current_label;
								END IF;
							END IF;
						END LOOP;
					END LOOP;
				ELSE
					RAISE EXCEPTION 'Chyba 16: fn_get_gids4aux_total: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v tabulce t_config!',_complete,_categories;
				END IF;
			ELSE
				RAISE EXCEPTION 'Chyba 17: fn_get_gids4aux_total: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v t_config!',_complete,_categories;
			END IF;

			IF _config_query = 500 -- u reference probihaji jen kontroly a do vystupu se nic nevraci
			THEN
				_q :=	'
					WITH
					w1 AS	(
						SELECT
							1 AS step,
							$3 AS config_id,
							NULL::integer AS estimation_cell,
							NULL::integer AS gid
						)
					SELECT
						w1.step,
						w1.config_id,
						w1.estimation_cell,
						w1.gid
					FROM
						w1 WHERE w1.step = 0;
					';
			ELSE	-- vetev pro config_query 200,300,400 => zde bude promenna _res_estimation_cell_prepocet_4_check, ta obsahuje jak pripadne ZSJ,
				-- tak estimation_cell ze vstupu + estimation_cell podrizene
				_q :=
					'
					WITH
					w1 AS	(SELECT unnest($10) AS estimation_cell),
					w2 AS	(
						SELECT
							1 AS step,
							$3 AS config,
							w1.estimation_cell,
							NULL::integer AS gid
						FROM
							w1
						)
					SELECT
						w2.step,
						w2.config,
						w2.estimation_cell,
						w2.gid
					FROM
						w2
					ORDER
						BY w2.step, w2.config, w2.estimation_cell
					';
			END IF;	
		END IF;		
	ELSE
		-- vetev pro VYPOCET

		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- Tento proces zatim neimplementuju, podle me neni nutny, tim ze to uzivateli pada na
		-- chybe 19, tak podle me spatne zadal geograficke urovne pro vypocet.
		-- proces odstraneni tech vstupnich ZSJ (ktere tvori estimation_cell jen jednou geometrii)
		-- ze vstupni promenne _estimation_cell, pokud _estimation_cell obsahuje jeji vyssi
		-- geografickou jednotku
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- KONTROLA EXISTENCE v DB --

		-- zde se v databazi kontroluje pritomnost zaznamu, a to
		-- pro i-tou estimation_cell a pro vsechny config_id dane
		-- "konfigurace" => pokud tato podminka je splnena, pak
		-- nastane vyjimka => uzivatel toto uz v aplikaci zadat nesmi

		-- tato kontrola je zde z duvodu, kdy uzivatel zastavi vypocet,
		-- verze extenze je zde nutna a jednotlive kategorie konfigurace
		-- musi odpovidat _ext_version_current
		
		FOR i IN 1..array_length(_estimation_cell,1)
		LOOP
			FOR ii IN 1..array_length(_configs,1)
			LOOP
				SELECT tc.config_query FROM @extschema@.t_config AS tc
				WHERE tc.id = _configs[ii]
				INTO _config_query_ii;
				
				IF _config_query_ii = 500 -- REFERENCE
				THEN
					SELECT tc.categories::integer
					FROM @extschema@.t_config AS tc
					WHERE tc.id = _configs[ii]
					INTO _config_id_reference_ii;

					IF _config_id_reference_ii IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 18: fn_get_gids4aux_total_app: Kontrolovane _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema ve sloupci categories nakonfigurovanou referenci!',_configs[ii];
					END IF;

					_config_id_base_ii := _config_id_reference_ii;
				ELSE
					_config_id_base_ii := _configs[ii]; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
				END IF;

				WITH
				w1 AS	(
					SELECT tat.* FROM @extschema@.t_aux_total AS tat
					WHERE tat.estimation_cell = _estimation_cell[i]
					AND tat.config = _config_id_base_ii
					AND tat.ext_version = _ext_version_current
					)
				SELECT count(*) FROM w1	-- pokud je pocet vetsi jak 0 tak zaznam(y) exisuje(i)
				INTO _check_ii;

				IF _check_ii > 0
				THEN
					IF ii = 1
					THEN
						_check_ii_boolean := array[0];
					ELSE
						_check_ii_boolean := _check_ii_boolean || array[0];
					END IF;
				ELSE
					IF ii = 1
					THEN
						_check_ii_boolean := array[1];
					ELSE
						_check_ii_boolean := _check_ii_boolean || array[1];
					END IF;
				END IF;

			END LOOP;

			IF	(
				SELECT sum(t.check_ii_boolean) = 0
				FROM (SELECT unnest(_check_ii_boolean) AS check_ii_boolean) AS t
				)
			THEN
				IF _config_query = 500
				THEN
					RAISE NOTICE 'NOTICE: fn_get_gids4aux_total_app: Vstupni kategorie _config_id = % z konfigurace	config_collection = % je REFERENCE a pro zadanou i_tou estimation_cell = % jiz v tabulce t_aux_total existuji hodnoty aux_total pro vsechny kategorie dane konfigurace ve verzi %. Tzn. ze nektera z predchozich kategorii dane konfigurace	nebyla referenci a jejim vypoctem doslo k vypoctu vsech kategorii dane konfigurace u i-te estimation_cell, coz je v poradku a neni duvod k vyjimce. Vstupni seznam pocitanych estimation_cell: %.',_config_id,_config_collection,_estimation_cell[i],_ext_version_current_label,_estimation_cell;
				ELSE
					RAISE EXCEPTION 'Chyba 19: fn_get_gids4aux_total_app: Vypocet neni mozny => pro zadanou estimation_cell = % jiz v tabulce t_aux_total existuji hodnoty aux_total pro vsechny kategorie dane konfigurace (config_collection = %) ve verzi %! Vstupni seznam estimation_cell: %.',_estimation_cell[i],_config_collection,_ext_version_current_label,_estimation_cell;
				END IF;
			END IF;

		END LOOP;
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- CONFIG_ID_BASE
		IF _config_query = 500 -- REFERENCE
		THEN
			SELECT tc.categories::integer
			FROM @extschema@.t_config AS tc
			WHERE tc.id = _config_id
			INTO _config_id_reference;

			IF _config_id_reference IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 20: fn_get_gids4aux_total_app: Vstupni _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema nakonfigurovanou referenci!',_config_id;
			END IF;

			_config_id_base := _config_id_reference;
		ELSE
			_config_id_base := _config_id; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
		END IF;
		--------------------------------------------------------------------------------------------
		-- pokud jde o REFERENCI, pak kontrola, ze _config_id_base pro current ext_version
		-- a i_tou estimation_cell uz v DB existuje, resp. musi existovat, jinak vyjimka !!!
		IF _config_query = 500 -- reference
		THEN
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				IF	(
					SELECT count(tat.*) = 0
					FROM @extschema@.t_aux_total AS tat
					WHERE tat.config = _config_id_base
					AND tat.estimation_cell = _estimation_cell[i]
					AND tat.ext_version = _ext_version_current
					)
				THEN
					RAISE EXCEPTION 'Chyba 21: fn_get_gids4aux_total_app: Vstupni _config_id = % je reference [config_query = 500, referencni id = %], jde o kategorii z konfigurace = %, a pro tuto referenci a estimation_cell = % nejsou doposud vypocitana data v aktualni verzi % extenze nfiesta_gisdata!',_config_id,_config_id_base,_config_collection,_estimation_cell[i],_ext_version_current_label;
				END IF;
			END LOOP;			
		END IF;
		--------------------------------------------------------------------------------------------
		-- KONTROLA proveditelnosti pro "VSTUPNI" CONFIG_QUERY [200,300,400]:
		-- 200,300,400	=> jsou v t_aux_total vypocitany data zakladu v current ext_version
		IF _config_query = ANY(array[200,300,400]) -- vetev pro soucet a doplnky
		THEN
			-- zjisteni potrebnych promennych z konfigurace pro vetev soucet nebo doplnky
			SELECT
				tc.complete,
				tc.categories
			FROM
				@extschema@.t_config AS tc WHERE tc.id = _config_id_base
			INTO
				_complete,
				_categories;

			-- raise notice '_complete: %',_complete;
			-- raise notice '_categories: %',_categories;

			IF _complete IS NULL AND _categories IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 22: fn_get_gids4aux_total_app: Vstupni _config_id = % [jde o kategorii z konfigurace %] je soucet nebo doplnek, a pro jeho konfiguraci nejsou spravne vyplnena pole _complete a _categories!',_config_id,_config_collection;
			END IF;

			-- spojeni konfiguracnich idecek _complete/_categories
			IF _complete IS NULL
			THEN
				_config_ids_text := concat('array[',_categories,']');
			ELSE
				_config_ids_text := concat('array[',_complete,',',_categories,']');
			END IF;

			-- raise notice '_config_ids_text: %',_config_ids_text;

			-- zjisteni poctu idecek tvorici _complete/_categories
			EXECUTE 'SELECT array_length('||_config_ids_text||',1)'
			INTO _config_ids_length;

			-- raise notice '_config_ids_length: %',_config_ids_length;

			-- zjisteni existujicich konfiguracnich idecek pro (_complete/_categories)
			EXECUTE 'SELECT array_agg(tc.id ORDER BY tc.id) FROM @extschema@.t_config AS tc WHERE tc.id in (select unnest('||_config_ids_text||'))'
			INTO _config_ids;

			-- raise notice '_config_ids: %',_config_ids;

			-- splneni podminky stejneho poctu idecek
			IF _config_ids_length = array_length(_config_ids,1)
			THEN
				-- kontrola zda i obsah je stejny
				EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text||') AS ids) AS t'
				INTO _config_ids4check;

				IF _config_ids4check = _config_ids
				THEN
					-- cyklus kontroly, zda pro souctet nebo doplnek jsou vypocitany vsechny potrebne
					-- kategorie a odpovidaji aktualni verzi ext_version_current
					-- pozn. zde si uz muzu sahat do tabulky t_aux_total, protoze pro tento typ vypoctu
					-- uz musi existovat zakladni (protinaci) aux_total hodnoty, respektive musely pro
					-- complete nebo categories probehnout _config_query 100 !!!

					-- raise notice '_estimation_cell: %',_estimation_cell;
					
					FOR i IN 1..array_length(_estimation_cell,1)
					LOOP
						FOR y IN 1..array_length(_config_ids,1)
						LOOP
							WITH
							w1 AS	(
								SELECT tat.* FROM @extschema@.t_aux_total AS tat
								WHERE tat.config = _config_ids[y]
								AND tat.estimation_cell = _estimation_cell[i]
								AND tat.ext_version = _ext_version_current
								)
							SELECT count(*) FROM w1
							INTO _check_avc4categories;

							IF _check_avc4categories != 1
							THEN		
								RAISE EXCEPTION 'Chyba 23: fn_get_gids4aux_total: Vstupni _config_id = % [kategorie z konfigurace %] je soucet nebo doplnek a pro konfiguraci [z complete/categories] = % a pro estimation_cell = % neexistuje v tabulce t_aux_total vypocitana hodnota aux_total v aktualni verzi ext_version = %!',_config_id_base,_config_collection,_config_ids[y],_estimation_cell[i],_ext_version_current_label;
							END IF;
						END LOOP;
					END LOOP;
				ELSE
					RAISE EXCEPTION 'Chyba 24: fn_get_gids4aux_total: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v t_config!',_complete,_categories;
				END IF;
			ELSE
				RAISE EXCEPTION 'Chyba 25: fn_get_gids4aux_total: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v t_config!',_complete,_categories;
			END IF;
		END IF;
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------

		IF _config_query = ANY(array[100,200,300,400])
		THEN
			-- zde je implementovan proces ziskani zakladnich stavebnich jednotek (ZSJ)
			---------------------------------------------------------------------------
				
			-- cyklus pro ziskani POCTu
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni zakladnich stavebnich jednotek (gidu) z tabulky f_a_cell pro i-tou _estimation_cell
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i;

				IF _gids4estimation_cell_i IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 26: fn_get_gids4aux_total_app: Pro _estimation_cell[i] = % nenalezeny zadne zaznamy v tabulce f_a_cell!',_estimation_cell[i];
				END IF;

				-- volani funkce fn_get_lowest_gids [funkce si saha do mapovaci tabulky cm_f_a_cell]
				-- funkce by mela vratit seznam zakladnich stavebnich jednotek (gidu), ktere tvori zadany seznam vyssich gidu
				-- v tomto cyklu ale nejprve pozaduju pocet gidu
				_gids4estimation_cell_pocet := array_length(@extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i),1);

				IF i = 1
				THEN
					_pocet := array[_gids4estimation_cell_pocet];
				ELSE
					_pocet := _pocet || array[_gids4estimation_cell_pocet];
				END IF;
				
			END LOOP;

			-- zjisteni nejdelsiho pole _gids4estimation_cell
			SELECT max(t.pocet) FROM (SELECT unnest(_pocet) as pocet) AS t
			INTO _max_pocet;

			-- cyklus ziskani GIDu
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni gidu z tabulky f_a_cell pro i-tou _estimation_cell
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i;

				-- volani funkce fn_get_lowest_gids
				-- funkce by mela vratit seznam gidu nejnizsi urovne, ktere tvori zadany seznam vyssich gidu
				_gids4estimation_cell := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i);

				-- zjisteni poctu prvku(gidu) v poli _gids4estimation_cell
				_pocet_gids4estimation_cell := array_length(_gids4estimation_cell,1);

				-- vytvoreni pole s nulama o poctu _max_pocet - _pocet_gids4estimation_cell
				SELECT array_agg(t.gid_gs) FROM (SELECT 0 AS gid_gs FROM generate_series(1,(_max_pocet - _pocet_gids4estimation_cell))) AS t
				INTO _doplnek;

				-- proces doplneni 0 tak aby pole _gids4estimation_cell melo delku _max_pocet
				_gids4estimation_cell := _gids4estimation_cell || _doplnek;

				IF i = 1
				THEN
					_res_estimation_cell := array[array[_estimation_cell[i]]];
					_res_gids := array[_gids4estimation_cell];
				ELSE
					_res_estimation_cell := _res_estimation_cell || array[array[_estimation_cell[i]]];
					_res_gids := _res_gids || array[_gids4estimation_cell];
				END IF;

			END LOOP;
		END IF;

		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------

		-- po sem mam:
			-- _res_estimation_cell => pole poli vstupnich estimation_cell co si uzivatel zvolil [muze
			-- jit o estimation_cell z ruznych geografickych urovni a muzou se zde vyskytovat estimation_cell jako ZSJ]
			-- _res_gids => seznam zakladnich stavebnich jednotek (gidu) pro zadane estimation_cell [pozor gidy se zde mohou duplikovat]

		--------------------------------------------------------------------------------------------
		--raise notice '_res_estimation_cell: %',_res_estimation_cell;
		--raise notice '_res_gids: %',_res_gids;
		--------------------------------------------------------------------------------------------
		--raise notice '_unnest_text:%',_unnest_text;
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- sestaveni vysledneho textu pro _config_query
		IF _config_query = 100	-- vetev pro zakladni (protinani)
		THEN
			-- kontrola ze pocet prvku v poli poli _res_estimation_cell se rovna poctu prvku v poli poli _res_gids
			IF (array_length(_res_estimation_cell,1) != array_length(_res_gids,1))
			THEN
				RAISE EXCEPTION 'Chyba 27: fn_get_gids4aux_total_app: Pocet prvku v interni promenne _res_estimation_cell neodpovida poctu prvku v interni promenne _res_gids!';
			END IF;

			-- zjisteni poctu prvku v jednotlivych prvcich u _res_gids [u multi array je vsude stejny pocet]
			SELECT array_length(_res_gids,2)
			INTO _res_gids_count_count;		

			-- proces sestaveni _res_estimation_cell_4_with do return query
			FOR i IN 1..array_length(_res_estimation_cell,1)
			LOOP
				WITH
				w AS	(
					SELECT
						unnest(_res_estimation_cell[i:i]) as estimation_cell,
						generate_series(1,_res_gids_count_count)
					)
				SELECT array_agg(w.estimation_cell) FROM w
				INTO _res_estimation_cell_4_with_i;

				IF i = 1
				THEN
					_res_estimation_cell_4_with := array[_res_estimation_cell_4_with_i];
				ELSE
					_res_estimation_cell_4_with := _res_estimation_cell_4_with || array[_res_estimation_cell_4_with_i];
				END IF;
			END LOOP;
			--------------------------------------------------------------------------------------------
			-- proces zjisteni k jednotlivym _res_estimation_cell techto informaci:
			-- je-li estimation_cell zakladni stavebni jednotkou nebo neni
			-- je-li estimation_cell zakladni stavebni jednotkou a je nutna jeste jeji sumarizace,
			WITH
			w1 AS	(
				SELECT unnest(_res_estimation_cell) AS estimation_cell
				),
			w2 AS	(	
				SELECT
					w1.estimation_cell,
					t1.estimation_cell_collection,
					t2.estimation_cell_collection_lowest
				FROM
						w1
				INNER JOIN 	@extschema@.c_estimation_cell 			AS t1	ON w1.estimation_cell = t1.id
				INNER JOIN	@extschema@.cm_estimation_cell_collection	AS t2	ON t1.estimation_cell_collection = t2.estimation_cell_collection
				),
			w3 AS	(
				SELECT
					w2.*,
					CASE
					WHEN w2.estimation_cell_collection = w2.estimation_cell_collection_lowest
					THEN TRUE ELSE FALSE
					END AS estimation_cell_w4
				FROM
					w2
				),
			w4 AS	(
				select
					fac.estimation_cell,
					count(fac.gid) as count_gids
				from @extschema@.f_a_cell AS fac
				where fac.estimation_cell in	(
								SELECT w1.estimation_cell FROM w1
								)
				group by fac.estimation_cell
				),
			w5 AS	(
				SELECT
					w3.*,
					w4.count_gids
				FROM
					w3 LEFT JOIN w4 ON w3.estimation_cell = w4.estimation_cell
				),
			w6 AS	(
				SELECT
					w5.*,
					CASE
						WHEN w5.estimation_cell_w4 = FALSE THEN TRUE
						ELSE
							CASE
							WHEN w5.count_gids > 1 THEN TRUE
							ELSE FALSE
							END
					END AS summarization
				FROM
					w5
				)	
			SELECT
				array_agg(w6.estimation_cell ORDER BY w6.estimation_cell),
				array_agg(w6.summarization ORDER BY w6.estimation_cell)
			FROM
				w6
			INTO
				_res_estimation_cell_summarization,
				_summarization;
			--------------------------------------------------------------------------------------------
			_q := 	'
				WITH
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------
				-- tato cast je plny (i s duplicitama) seznam gidu zakladnich stavebnich jednotek,
				-- ktery tvori jednotlive pozadovane _estimation_cell[]
				w1 AS	(
					SELECT
						$11 AS estimation_cell,	-- jde o pole poli => identifikace estimation_cell ze vstupu teto funkce [muze obsahovat estimation_cell i ZSJ]
						$2 AS gid		-- jde o pole poli => seznam gidu (ZSJ) tvorici danou estimation_cell
					),
				w2 AS 	(
					SELECT
						unnest(estimation_cell) AS estimation_cell,
						unnest(gid) AS gid
					FROM
						w1
					),
				w3 AS	(SELECT * FROM w2 WHERE gid IS DISTINCT FROM 0),	-- zde je roz-unnestovan with w1 a odstraneny nulove radky
				---------------------------------------------------------------------------
				-- pripojeni informace summarization k estimation_cell
				w4 AS	(
					SELECT
						unnest($5) AS estimation_cell_summarization,
						unnest($6) AS summarization
					),
				w5 AS	(
					SELECT
						w3.estimation_cell,
						w3.gid,
						w4.summarization
					FROM
						w3 LEFT JOIN w4 ON w3.estimation_cell = w4.estimation_cell_summarization
					),
				---------------------------------------------------------------------------
				-- tato cast je vyber zakladnich stavebnich jednotek (gidu), ktere jsou
				-- doposud vypocitany v t_aux_total a odpovidaji aktualni verzi _ext_version_current
				-- ty, ktere neodpovidaji se hold prepocitaji i za cenu, ze povysenim verze extenze
				-- nedoslo k upravam ve vypocetni oblasti vyvoje
				w9 AS 	(
					SELECT * FROM @extschema@.t_aux_total
					WHERE config = $3
					AND cell IS NOT NULL					-- ve sloupci cell se ukladaji jen gidy nejnizsi urovne z f_a_cell 
					AND cell IN (SELECT DISTINCT gid FROM w3)
					AND ext_version = $7		
					),
				---------------------------------------------------------------------------
				-- tato cast je pripojeni withu w9 k withu w5
				w10 AS	(
					SELECT
						w5.estimation_cell,
						w5.gid,			-- gid nejnizsi urovne, ktery tvori danou estimation_cell
						w5.summarization,			
						w9.cell			-- gid nejsizsi urovne, ktery je doposud vypocitan v t_aux_total
					FROM
						w5
					LEFT
					JOIN	w9	ON w5.gid = w9.cell
					),
				---------------------------------------------------------------------------
				-- tato cast rika, zda gid nejnizsi urovne je pro danou estimation_cell[i] jiz vypocitan nebo nikoliv
				-- pro danou estimation_cell => gidy s calculated vsude TRUE		=> vsechny pozadovane gidy uz jsou v DB vypocitany
				-- pro danou estimation_cell => gidy s calculated vsude FALSE		=> zadny z pozadovanych gidu jeste neni v DB vypocitan
				-- pro danou estimation_cell => gidy s calculated TRUE nebo FALSE	=> z pozadovanych gidu uz nektere v DB vypocitany jsou a nektere ne
				w11 AS	(
					SELECT
						$3 AS config_id,
						w10.estimation_cell,			-- vstupni hodnota i-te estimation_cell
						w10.gid,				-- gid nejnizsi urovne tvorici danou i-tou estimation_cell
						w10.summarization,

						CASE
						WHEN w10.cell IS NOT NULL THEN TRUE	-- toto plati je-li gid nejnizsi urovne pro danou i-tou estimation_cell je jiz vypocitan
						ELSE FALSE				-- toto plati je-li gid nejnizsi urovne pro danou i-tou estimation_cell neni jeste vypocitan
						END AS calculated
					FROM
						w10
					),
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------
				-- vyber zakladnich stavebnich jednotek pro vypocet
				w12 AS	(SELECT DISTINCT gid FROM w11 WHERE calculated = FALSE ORDER BY gid),
				w13 AS	(
					SELECT
						1 AS step,
						$3 AS config_id,
						fac.estimation_cell,
						w12.gid
					FROM
						w12
					---------------------------------------------------------------------
					LEFT JOIN	@extschema@.f_a_cell AS fac ON w12.gid = fac.gid	-- toto doplnuje zpatky estimation_cell [v aplikaci by se mohlo spojit LABEL a GID]
					---------------------------------------------------------------------
					ORDER
						BY w12.gid
					),
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------
				-- ted reseni SUMARIZACE => bude se provadet vzdy
				---------------------------------------------------------------------------
				w14 AS	(SELECT DISTINCT estimation_cell FROM w11 WHERE summarization = TRUE),
				w15 AS	( -- napojeni na DB
					SELECT
						tat.estimation_cell
					FROM
						@extschema@.t_aux_total AS tat
					WHERE
						tat.config = $3
					AND
						tat.cell IS NULL
					AND
						tat.estimation_cell IN (SELECT w14.estimation_cell FROM w14)
					AND
						tat.ext_version = $7
					),
				w16 AS	(
					SELECT
						w14.estimation_cell,
						CASE
						WHEN w15.estimation_cell IS NOT NULL THEN TRUE	-- estimation_cell, ktera uz je v DB sumarizovana v current verzi
						ELSE FALSE					-- estimation_cell, kterou je nutno do-sumarizovat
						END AS calculated
					FROM
						w14
					LEFT
					JOIN	w15	ON w14.estimation_cell = w15.estimation_cell
					),
				w17 AS	(
					SELECT
						2 AS step,
						$3 AS config_id,
						w16.estimation_cell,
						0::integer AS gid
					FROM
						w16
					WHERE
						w16.calculated = FALSE
					
					ORDER
						BY w16.estimation_cell
					)
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------	
				SELECT * FROM w13	-- => VZDY NUTNY DOPLNEK
				UNION ALL
				SELECT * FROM w17	-- => VZDY NUTNA SUMMARIZACE
				---------------------------------------------------------------------------			
				';
		ELSE
			IF _config_query = 500
			THEN
				_q :=	'
					WITH
					w1 AS	(
						SELECT
							1 AS step,
							$3 AS config_id,
							NULL::integer AS estimation_cell,
							NULL::integer AS gid
						)
					SELECT
						w1.step,
						w1.config_id,
						w1.estimation_cell,
						w1.gid
					FROM
						w1 WHERE w1.step = 0;
					';
			ELSE
				-- vetev pro config_query [200,300,400] => soucet a doplnky

				_q :=	'
					WITH
					w1 AS	(SELECT unnest($2) AS gids),	-- jde o seznam gidu (ZSJ), ktere tvori vstupni estimation_cell [vyskyt duplicitnich gidu]
					w2 AS	(SELECT DISTINCT gids FROM w1),	-- z-unikatneni gidu (ZSJ)
					w3 AS	(				-- vyber zaznamu z f_a_cell pro unikatni gidy (ZSJ)
						SELECT
							fac.gid,
							fac.estimation_cell
						FROM
							@extschema@.f_a_cell AS fac
						WHERE
							fac.gid IN (SELECT gids FROM w2)
						),
					w4 AS	(				-- zjisteni poctu gidu tvorici estimation_cell
						SELECT
							w3.estimation_cell,
							count(w3.gid) AS pocet
						FROM
							w3 GROUP BY w3.estimation_cell
						),
					w5 AS	(				-- vyber ZSJ, ktere tvori estimation_cell jen jednou geometrii
						SELECT w4.estimation_cell FROM w4
						WHERE w4.pocet = 1
						),
					w6 AS	(SELECT unnest($4) AS estimation_cell),	-- roz-unnestovani vstupnich estimation_cell
					w7 AS	(					-- odstraneni vstupnich estimation_cell z w5.estimation_cell
						SELECT w5.estimation_cell FROM w5 EXCEPT
						SELECT w6.estimation_cell FROM w6	-- vysledkem jsou estimation_cell (ZSJ) pro DOPLNENI
						),
					w8 AS	(				-- zjisteni gidu (ZSJ) pres estimation_cell co je opravdu v DB pro danou ext_version_current
						SELECT				-- u (ZSJ) je estimation_cell vzdy vyplneno pokud je estimation_cell tvoreno jednou geometrii
							tat.estimation_cell
						FROM
							@extschema@.t_aux_total AS tat
						WHERE
							config = $3
						AND
							tat.ext_version = $7
						AND
							tat.estimation_cell IN (SELECT w7.estimation_cell FROM w7)
						),
					w9 AS	(
						SELECT
							$3 AS config_id,
							w7.estimation_cell,
						
							CASE
							WHEN w8.estimation_cell IS NOT NULL THEN TRUE	-- toto plati je-li estimation_cell nejnizsi urovne pro danou ext_version_current => jiz vypocitano
							ELSE FALSE					-- toto plati je-li estimation_cell nejnizsi urovne pro danou ext_version_current => neni jeste vypocitano
							END AS calculated
						FROM
							w7 LEFT JOIN w8 ON w7.estimation_cell = w8.estimation_cell					
						),
					w10 AS	(				-- 1. cast do UNIONu
						SELECT
							1 AS step,
							w9.config_id,
							w9.estimation_cell,
							NULL::integer AS gid
						FROM
							w9 WHERE w9.calculated = FALSE
						ORDER
							BY w9.estimation_cell
						),
					-------------------------------------------------
					w11 AS	(
						SELECT
							2 AS step,
							$3 AS config_id,
							$4 AS estimation_cell,
							NULL::integer AS gid
						),
					w12 AS	(	
						SELECT
							w11.step,
							w11.config_id,
							unnest(w11.estimation_cell) AS estimation_cell,
							w11.gid
						FROM
							w11
						),
					w13 AS	( -- napojeni na DB
						SELECT
							tat.estimation_cell
						FROM
							@extschema@.t_aux_total AS tat
						WHERE
							tat.config = $3
						AND
							tat.cell IS NULL
						AND
							tat.estimation_cell IN (SELECT w12.estimation_cell FROM w12)
						AND
							tat.ext_version = $7
						),
					w14 AS	(

						SELECT
							w12.step,
							w12.config_id,
							w12.estimation_cell,
							w12.gid,
							CASE
							WHEN w13.estimation_cell IS NOT NULL THEN TRUE	-- estimation_cell, ktera uz je v DB sumarizovana v current verzi
							ELSE FALSE					-- estimation_cell, kterou je nutno do-sumarizovat
							END AS calculated
						FROM
							w12
						LEFT
						JOIN	w13	ON w12.estimation_cell = w13.estimation_cell
						),						
					w15 AS	(				-- 2. cast do UNIONu
						SELECT
							w14.step,
							w14.config_id,
							w14.estimation_cell,
							w14.gid
						FROM
							w14
						WHERE
							w14.calculated = FALSE
						ORDER
							BY w14.estimation_cell
						)
					-------------------------------------------------------
					SELECT w10.* FROM w10 UNION ALL
					SELECT w15.* FROM w15
					';
			END IF;
		END IF;
	END IF;
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||''
	USING
		_res_estimation_cell,				-- $1
		_res_gids,					-- $2
		_config_id,					-- $3
		_estimation_cell,				-- $4
		_res_estimation_cell_summarization,		-- $5
		_summarization,					-- $6
		_ext_version_current,				-- $7
		_res_gids_prepocet_4_recount,			-- $8
		_res_estimation_cell_prepocet_4_summarization,	-- $9
		_res_estimation_cell_prepocet_4_check,		-- $10
		_res_estimation_cell_4_with;			-- $11
	--------------------------------------------------------------------------------------------	
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


ALTER FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) IS
'Funkce vraci seznam vypocetnich udaju pro funkci fn_get_aux_total_app.';
-- </function>



-- <function name="fn_get_aux_total_app" schema="@extschema@" src="functions/extschema/fn_get_aux_total_app.sql">
-- Function: @extschema@.fn_get_aux_total_app(integer, integer, integer)

-- DROP FUNCTION @extschema@.fn_get_aux_total_app(integer, integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total_app(
    IN _config_id integer,
    IN _estimation_cell integer,
    IN _gid integer)
  RETURNS TABLE(estimation_cell integer, config integer, aux_total double precision, cell integer, ext_version integer) AS
$BODY$
DECLARE
	_config_collection			integer;
	_config_query				integer;
	_config_function			integer;
	_estimation_cell_exit			integer;
	_function_aux_total			text;
	_categories				character varying;
	_categories_sum				double precision;
	_res_aux_total				double precision;
	_estimation_cell_area			double precision;	
	_complete				character varying;
	_complete_sum				double precision;
	_aux_total				double precision;
	_estimation_cell_collection		integer;
	_estimation_cell_collection_lowest	integer;
	_estimation_cell_result			integer;
	_check_pocet				integer;
	_gid_result				integer;	
	_q					text;

	_ext_version_label_system		text;
	_ext_version_current			integer;
BEGIN
	--------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;	
	
	IF _estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total_app: Vstupni argument _estimation_cell nesmi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	-- proces ziskani aktualni extenze nfiesta_gisdata
	SELECT extversion FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_total_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_total_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;	
	--------------------------------------------------------------------------------------------
	-- proces ziskani potrebnych hodnot z konfigurace
	SELECT
		t.config_collection,
		t.config_query
	FROM
		@extschema@.t_config AS t
	WHERE
		id = _config_id
	INTO
		_config_collection,
		_config_query;
	--------------------------------------------------------------------------------------------
	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_aux_total_app: Argument _config_collection nesmi byt NULL!';
	END IF;	

	IF _config_query IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_aux_total_app: Argument _config_query nesmi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	IF _config_query = 500
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_aux_total_app: Vypocet nelze provest pro config_query = %!',_config_query;
	END IF;
	--------------------------------------------------------------------------------------------
	IF _gid IS NOT NULL AND _config_query = ANY(array[200,300,400])
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_get_aux_total_app: Vstupni argument _gid musi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	-- proces ziskani potrebnych hodnot z kolekce
	SELECT tcc.config_function FROM @extschema@.t_config_collection AS tcc
	WHERE tcc.id = _config_collection
	INTO _config_function;
	--------------------------------------------------------------------------------------------
	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_aux_total_app: Argument _config_function nesmi byt NULL!';
	END IF;	
	--------------------------------------------------------------------------------------------
	-- kontrola zdali pro zadanou konfiguraci a zadanou _estimation_cell
	-- uz je ci neni hodnota aux_total vypocitana a jestli neni nahodou duplicitni
	-- => toto by mel hlidat bud trigger nebo check-constraint v tabulce t_aux_total
	-- => nebo je implementovano jiz ve funkci fn_get_gids4aux_total
	--------------------------------------------------------------------------------------------	
	--------------------------------------------------------------------------------------------
	-- rozhodovaci proces co funkce fn_get_aux_total_app bude delat:
	CASE
		WHEN (_config_query = 100) -- zakladni with [vypocet z GIS vrstvy]
		THEN
			-- _gid zde nesmi byt hodnota 0 => to je pro identifikaci pro sumarizaci
			-- _calculated zde muze byt jen FALSE => JIRKOVA aplikace tuto funkci bude poustet jen na gidy, 
			-- ktere nejsou doposud vypocitany, nebo pokud se jedna o prepocet, a provede insert do t_aux_total
			-- pokud se    jedna o gid zakladu, pak do vystupu musi jit estimation_cell zakladu
			-- pokud se ne-jedna o gid zakladu, pak do vystupu pujde estimation_cell co je zde na vstupu

			IF _gid > 0
			THEN
				-- zjisteni estimation_cell do vystupu teto funkce
				SELECT fac.estimation_cell FROM @extschema@.f_a_cell AS fac WHERE fac.gid = _gid
				INTO _estimation_cell_exit;

				IF _estimation_cell_exit IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 10: fn_get_aux_total_app: Pro gid = % nenalezeno estimation_cell v tabulce f_a_cell!',_gid;
				END IF;
				
				-- povolen vypocet aux_total hodnoty pro vstupni gid [zakladni protinani]
				CASE
					WHEN (_config_function = 100)	-- vector
					THEN
						_function_aux_total := 'fn_get_aux_total_vector_app';

					WHEN (_config_function = 200)	-- raster
					THEN
						_function_aux_total := 'fn_get_aux_total_raster_app';

					WHEN (_config_function = 300)	-- vector_comb
					THEN
						_function_aux_total := 'fn_get_aux_total_vector_comb_app';

					WHEN (_config_function = 400)	-- raster_comb
					THEN
						_function_aux_total := 'fn_get_aux_total_raster_comb_app';
					ELSE
						RAISE EXCEPTION 'Chyba 11: fn_get_aux_total_app: Neznama hodnota parametru _config_function [%].',_config_function;
				END CASE;
			ELSE
				RAISE EXCEPTION 'Chyba 12: fn_get_aux_total_app: Nepovolena kombinace vstupnich argumentu [_config_id = %, _estimation_cell = %, _gid = %] pro vypocet aux_total hodnoty pro config_query 100!',_config_id,_estimation_cell,_gid;
			END IF;

		WHEN (_config_query = ANY(array[200,300,400]))
		THEN
			-- 200 [soucet existujicich kategorii]
			-- 300 [doplnek do rozlohy vypocetni bunky]
			-- 400 [doplnek do rozlohy existujici kategorie]
			
			_function_aux_total := NULL::text;

			IF _gid IS NULL
			THEN
				-- povolen vypocet aux_total hodnoty pro soucet/doplnky

				_estimation_cell_exit := _estimation_cell;

				--raise notice '_estimation_cell_exit:%',_estimation_cell_exit;

				-- 1. zde se nejpreve z konfigurace zjisti _categories
				SELECT tc.categories FROM @extschema@.t_config AS tc WHERE tc.id = _config_id
				INTO _categories;

				--raise notice '_categories:%',_categories;

				-- 2. potom se pro _categories provede kontrola zda v tabulce t_aux_total vubec _categories existuji
				-- => toto uz je implementovano ve funkci fn_get_gids4aux_total

				-- 3. zde se provede suma za _categories pro danou _estimation_cell
				EXECUTE '
				SELECT sum(t.aux_total) FROM @extschema@.t_aux_total AS t
				WHERE t.estimation_cell = $1
				AND t.config IN (SELECT unnest(array['||_categories||']))
				AND t.ext_version = $2
				'
				USING _estimation_cell, _ext_version_current
				INTO _categories_sum;

				--raise notice '_categories_sum: %',_categories_sum;

				-- 4. proces ziskani _res_aux_total pro jednotlive varianty _config_query 200,300,400
				CASE
				WHEN _config_query = 200	-- soucet
				THEN
					_res_aux_total := _categories_sum;
					
				WHEN _config_query = 300	-- doplnek do rozlohy vypocetni bunky [doplnek pro danou estimation_cell]
				THEN
					-- zjisteni plochy pro danou estimation_cell z tabulky f_a_cell
					SELECT sum(ST_Area(fac.geom))/10000.0 FROM @extschema@.f_a_cell AS fac
					WHERE fac.estimation_cell = _estimation_cell
					INTO _estimation_cell_area;

					IF _estimation_cell_area IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 13: fn_get_aux_total_app: Pro zadanou _estimation_cell = % nezjistena plocha z tabulky f_a_cell!',_estimation_cell;
					END IF;

					_res_aux_total := _estimation_cell_area - _categories_sum;

				WHEN _config_query = 400
				THEN
					-- 1. zde se nejpreve z konfigurace zjisti complete
					SELECT tc.complete FROM @extschema@.t_config AS tc WHERE tc.id = _config_id
					INTO _complete;

					-- 2. potom se pro _complete provede kontrola zda v tabulce t_aux_total vubec existuje
					-- => toto uz je implementovano ve funkci fn_get_gids4aux_total

					-- 3. zjisteni sumy za _complete;
					EXECUTE '
					SELECT sum(t.aux_total) FROM @extschema@.t_aux_total AS t
					WHERE t.estimation_cell = $1
					AND t.config IN (SELECT unnest(array['||_complete||']))
					AND t.ext_version = $2
					'
					USING _estimation_cell, _ext_version_current
					INTO _complete_sum;

					_res_aux_total := _complete_sum - _categories_sum;	
				ELSE
					RAISE EXCEPTION 'Chyba 14: fn_get_aux_total_app: Pro _config_query = % doposud v tele funkce neprovedena implemetace procesu ziskani hodnoty _res_aux_total!',_config_query;
				END CASE;

			ELSE
				RAISE EXCEPTION 'Chyba 15: fn_get_aux_total_app: Nepovolena kombinace vstupnich argumentu [_config_id = %, _estimation_cell = %, _gid = %, _calculated = %] pro vypocet aux_total hodnoty pro config_query 200,300 nebo 400!',_config_id,_estimation_cell,_gid,_calculated;
			END IF;
		ELSE
			RAISE EXCEPTION 'Chyba 16: fn_get_aux_total_app: Neznama hodnota parametru _config_query [%].',_config_query;
	END CASE;
	--------------------------------------------------------------------------------------------
	IF _function_aux_total IS NOT NULL
	THEN
		EXECUTE 'SELECT @extschema@.'||_function_aux_total||'($1,$2)'
		USING _config_id, _gid
		INTO _aux_total;
	ELSE
		_aux_total := _res_aux_total;
	END IF;
	--------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------
	-- zjisteni do jake collection patri zadana vstupni estimation_cell
	SELECT cec.estimation_cell_collection FROM @extschema@.c_estimation_cell  AS cec WHERE cec.id = _estimation_cell
	INTO _estimation_cell_collection;
	--------------------------------------------------------------------------------------------
	-- zjisteni estimation_cell_collection_lowest pro zadanou _estimation_cell_collection
	SELECT cecc.estimation_cell_collection_lowest FROM @extschema@.cm_estimation_cell_collection AS cecc
	WHERE cecc.estimation_cell_collection = _estimation_cell_collection
	INTO _estimation_cell_collection_lowest;
	--------------------------------------------------------------------------------------------
	IF _config_query = 100
	THEN
		IF _estimation_cell_collection != _estimation_cell_collection_lowest	-- VSTUPNI estimation_cell NENI zaklad [pozn. gidy pro protinani jsou vzdy zaklad]
		THEN
			-- ale pokud funkce fn_get_gids4aux_total_app vratila gid pro danou vstupni estimation_cell => tzn. ze zakladni gid jeste neni v t_aux_total
			-- a musel se jiz drive v kodu pro gid dohledat estimation_cell
			-- napr. chci TREBIC a zakladni gidy pro tuto estimation_cell nejsou jeste ulozeny v t_aux_total
			-- vystup za NUTS1-4 a rajonizace => pujde pres step 2 a sumarizaci
			-- vystup za OPLO => pujde pres step 2 a sumarizaci

			IF _gid > 0
			THEN		
				_estimation_cell_result := _estimation_cell_exit;
			ELSE
				RAISE EXCEPTION 'Chyba 17: fn_get_aux_total_app: Jde-li o config_query = 100, pak gid nesmi byt nikdy NULL nebo hodnota 0!';
			END IF;
		ELSE
			-- VSTUPNI estimation_cell JE zakladd
			
			-- zjisteni poctu geometrii, ktere tvori vstupni estimation_cell
			SELECT count(fac.gid) FROM @extschema@.f_a_cell AS fac WHERE fac.estimation_cell = _estimation_cell
			INTO _check_pocet;

			IF _check_pocet IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 18: fn_get_aux_total_app: Pro estimation_cell = % nenalezeny geometrie v tabulce f_a_cell!',_estimation_cell;
			END IF;

			IF _check_pocet = 1 -- vstupni estimation_cell uz neni nijak geometricky rozdrobena v f_a_cell
			THEN
				_estimation_cell_result := _estimation_cell_exit;
			ELSE
				_estimation_cell_result := NULL::integer;
			END IF;
		END IF;

		_gid_result := _gid;
	ELSE
		IF _config_query = ANY(array[200,300,400])
		THEN
			_estimation_cell_result := _estimation_cell_exit;
			_gid_result := NULL::integer;
		ELSE
			RAISE EXCEPTION 'Chyba 19: fn_get_aux_total_app: Neznama hodnota config_query = %!',_config_query;
		END IF;
	END IF;
	--------------------------------------------------------------------------------------------
	_q := 'SELECT $1,$2,$3,$4,$5';
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||'' USING _estimation_cell_result,_config_id,_aux_total,_gid_result,_ext_version_current;
	--------------------------------------------------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) IS
'Funkce vraci vypocitanou hodnotu aux_total pro zadanou konfiguraci (config_id) a celu (estimation_cell) nebo gid.';
-- </function>



-- <function name="fn_get_aux_total4estimation_cell_app" schema="@extschema@" src="functions/extschema/fn_get_aux_total4estimation_cell_app.sql">
-- DROP function @extschema@.fn_get_aux_total4estimation_cell_app (integer,integer,integer,boolean)

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app
	(
	_config_id			integer,
	_estimation_cell		integer,
	_gid				integer,
	_recount			boolean DEFAULT FALSE
	)
RETURNS TABLE
	(
        estimation_cell			integer,
        config				integer, 
        aux_total			double precision,
        cell				integer,
        ext_version			integer
        )
AS
$BODY$
DECLARE
	_estimation_cell_collection		integer;
	_estimation_cell_collection_lowest	integer;
	_check_estimation_cell			boolean;
	_check_pocet				integer;
	_gids4estimation_cell			integer[];
	_gids4estimation_cell_lowest		integer[];
	_gids_in_t_aux_total_check		integer;
	_aux_total				double precision;
	_q					text;

	_ext_version_label_system		text;
	_ext_version_current			integer;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total4estimation_cell_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total4estimation_cell_app: Vstupni argument _estimation_cell nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_total4estimation_cell_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;

	IF _gid != 0
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_total4estimation_cell_app: Vstupni argument _gid musi byt hodnota 0!';
	END IF;

	IF _recount IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_aux_total4estimation_cell_app: Vstupni argument _recount nesmi byt NULL!';
	END IF;	
	-------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------
	-- proces ziskani aktualni systemove extenze nfiesta_gisdata
	
	SELECT extversion FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_aux_total4estimation_cell_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_aux_total4estimation_cell_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;		
	-------------------------------------------------------------------------------------------
	-- kontrola, ze estimation_cell:
	-- 1. nesmi byt zakladni stavebni jednotka [ZSJ] => vyjimka plati jen pro viz. bod 2
	-- 2. muze to byt ZSJ ale pocet geometrii, ktery tvori onu ZSJ musi byt vice nez 1
	--------------------------------------------------------------------------------------------
	-- zjisteni do jake estimation_cell_collection patri zadana vstupni estimation_cell
	SELECT cec.estimation_cell_collection FROM @extschema@.c_estimation_cell AS cec WHERE cec.id = _estimation_cell
	INTO _estimation_cell_collection;
	--------------------------------------------------------------------------------------------
	-- zjisteni estimation_cell_collection_lowest pro zjistenou _estimation_cell_collection
	SELECT cecc.estimation_cell_collection_lowest FROM @extschema@.cm_estimation_cell_collection AS cecc
	WHERE cecc.estimation_cell_collection = _estimation_cell_collection
	INTO _estimation_cell_collection_lowest;
	--------------------------------------------------------------------------------------------
	IF _estimation_cell_collection != _estimation_cell_collection_lowest
	THEN
		-- vstupni estimation_cell neni ZSJ => splnena 1. podminka kontroly
		_check_estimation_cell := TRUE;
	ELSE
		-- vstupni estimation_cell je ZSJ
		
		-- zjisteni poctu geometrii, ktere tvori vstupni estimation_cell, ktera je ZSJ
		SELECT count(fac.gid) FROM @extschema@.f_a_cell AS fac
		WHERE fac.estimation_cell = _estimation_cell
		INTO _check_pocet;

		IF _check_pocet IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 08: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = % nenalezena zadna geometrie v tabulce f_a_cell!',_estimation_cell;
		END IF;

		IF _check_pocet = 1 -- vstupni estimation_cell (ZSJ) uz neni nijak geometricky rozdrobena v f_a_cell na mensi casti
		THEN
			_check_estimation_cell := FALSE;
		ELSE
			_check_estimation_cell := TRUE;
		END IF;
	END IF;
	--------------------------------------------------------------------------------------------
	IF _check_estimation_cell = FALSE
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_aux_total4estimation_cell_app: Zadanou estimation_cell = % neni mozno sumarizovat. Jedna se totiz o nejnizsi geografickou uroven, ktera neni geometricky rozdrobena na mensi casti!',_estimation_cell;
	END IF;
	--------------------------------------------------------------------------------------------
	-- kontrola zda jiz pro zadanou estimation_cell, config_id a ext_version neni uz vypocitana
	-- hodnota aux_total v tabulce t_aux_total
	-- kontrola se provadi pri VYPOCTU i PREPOCTU [puvodne zde bylo jen pri VYPOCTU]
	-- jelikoz je u vstupnich argumentu ponechan _recount, pak IF jsem upravil nasledovne
	IF _recount = ANY(array[TRUE,FALSE])
	THEN
		IF	(
			SELECT count(tat.*) > 0
			FROM @extschema@.t_aux_total AS tat
			WHERE tat.config = _config_id
			AND tat.estimation_cell = _estimation_cell
			AND tat.ext_version = _ext_version_current
			)
		THEN
			RAISE EXCEPTION 'Chyba 10: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = %, config_id = % a ext_version = % jiz existuje hodnota aux_total v tabulce t_aux_total. Sumarizace neni mozna!',_estimation_cell,_config_id,_ext_version_label_system;
		END IF;
	END IF;
	-------------------------------------------------------------------------------------------
	-- zjisteni gidu (ZSJ) z tabulky f_a_cell, ktere tvori zadanou _estimation_cell
	SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
	WHERE fac.estimation_cell = _estimation_cell
	INTO _gids4estimation_cell;

	IF _gids4estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 11: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = % nenalezeny zadne zaznamy v tabulce f_a_cell!',_estimation_cell;
	END IF;

	-- volani funkce fn_get_lowest_gids [funkce si saha do mapovaci tabulky cm_f_a_cell]
	-- funkce by mela vratit seznam gidu nejnizsi urovne (ZSJ), ktere tvori zadany seznam vyssich gidu
	_gids4estimation_cell_lowest := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell);
	-------------------------------------------------------------------------------------------
	-- kontrola zda v tabulce t_aux_total jsou pro sumarizaci vsechny gidy nejnizsi urovne
	-- pro config_id a ext_version
	WITH
	w1 AS	(SELECT unnest(_gids4estimation_cell_lowest) AS gids),
	w2 AS	(
		SELECT
			tat.config,
			tat.cell
		FROM
			@extschema@.t_aux_total AS tat
		WHERE
			tat.config = _config_id
		AND
			tat.cell IN (SELECT gids FROM w1)
		AND
			tat.ext_version = _ext_version_current
		),
	w3 AS	(
		SELECT
			w1.gids,
			w2.cell
		FROM
			w1 LEFT JOIN w2
		ON
			w1.gids = w2.cell
		)
	SELECT
		count(*) FROM w3 WHERE w3.cell IS NULL
	INTO
		_gids_in_t_aux_total_check;
	-------------------------------------------------------------------------------------------
	IF _gids_in_t_aux_total_check > 0
	THEN
		RAISE EXCEPTION 'Chyba 12: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = %, config_id = % a ext_version = % neni v tabulce t_aux_total kompletni seznam hodnot aux_total pro sumarizaci!',_estimation_cell,_config_id,_ext_version_label_system;
	END IF;
	-------------------------------------------------------------------------------------------
	-- vypocet hodnoty aux_total pro danou estimation_cell
	WITH
	w1 AS	(SELECT unnest(_gids4estimation_cell_lowest) AS gids),
	w2 AS	(
		SELECT
			tat.cell,
			tat.aux_total
		FROM
			@extschema@.t_aux_total AS tat
		WHERE
			tat.config = _config_id
		AND
			tat.cell IN (SELECT gids FROM w1)
		AND
			tat.ext_version = _ext_version_current
		)
	SELECT
		sum(w2.aux_total) AS aux_total
	FROM
		w2
	INTO
		_aux_total;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	_q := 'SELECT $1,$2,$3,NULL::integer,$4';
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||'' USING _estimation_cell,_config_id,_aux_total,_ext_version_current;
	--------------------------------------------------------------------------------------------
END ;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) IS
'Funkce vraci hodnotu aux_total pro zadanou estimation_cell a config_id.';

-- </function>



-- <function name="fn_get_aux_total_raster_app" schema="@extschema@" src="functions/extschema/fn_get_aux_total_raster_app.sql">
-- DROP FUNCTION @extschema@.fn_get_aux_total_raster_app(integer,integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total_raster_app
( 
	_config_id			integer,
	_gid				integer
)
RETURNS double precision AS
$BODY$
DECLARE
	_config_collection		integer;
	_condition			character varying;
	_band				integer;
	_reclass			integer;
	_schema_name			character varying;
	_table_name			character varying;
	_column_name			character varying;
	_unit				double precision;
	_gid_text			character varying;
	_command			text;
	_res				double precision;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total_raster_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total_raster_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro vstupni _config_id z t_config
	SELECT
		config_collection,
		condition,
		band,
		reclass
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id
	INTO
		_config_collection,
		_condition,
		_band,
		_reclass;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro vstupni _config_collection z t_config_collection
	SELECT
		schema_name,
		table_name,
		column_name,
		unit
	FROM
		@extschema@.t_config_collection
	WHERE
		id = _config_collection
	INTO
		_schema_name,
		_table_name,
		_column_name,
		_unit;
	-------------------------------------------------------------------------------------------
	-- prevedeni gidu na character varying
	_gid_text := _gid::character varying;
	-------------------------------------------------------------------------------------------
	-- vytvoreni (sestaveni) vysledneho sql dotazu pro ziskatni hodnoty aux_total
	SELECT @extschema@.fn_sql_aux_total_raster_app(_config_id, _schema_name, _table_name, _column_name, _band, _reclass,  _condition, _unit, _gid_text)
	INTO _command;
	-------------------------------------------------------------------------------------------
	EXECUTE ''||_command||'' INTO _res;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	RETURN _res;
	-------------------------------------------------------------------------------------------
END ;
$BODY$
LANGUAGE plpgsql VOLATILE;
	
ALTER FUNCTION @extschema@.fn_get_aux_total_raster_app(integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_raster_app(integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_raster_app(integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_raster_app(integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total_raster_app(integer,integer) IS
'Funkce vraci vypocitanou hodnotu aux_total_raster pro zadanou konfiguraci config_id a zadany gid geometrie z tabulky f_a_cell.';

-- </function>



-- <function name="fn_get_aux_total_raster_comb_app" schema="@extschema@" src="functions/extschema/fn_get_aux_total_raster_comb_app.sql">
-- DROP FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total_raster_comb_app
( 
	_config_id			integer,
	_gid				integer
)
RETURNS double precision AS
$BODY$
DECLARE
	_config_id_raster		integer;
	_config_id_raster_1		integer;
	_schema_name			character varying;
	_table_name			character varying;
	_column_name			character varying;
	_unit				double precision;
	_schema_name_1			character varying;
	_table_name_1			character varying;
	_column_name_1			character varying;
	_unit_1				double precision;
	_band				integer;
	_reclass			integer;
	_condition			character varying;
	_band_1				integer;
	_reclass_1			integer;
	_condition_1			character varying;
	_gid_text			character varying;
	_command			text;
	_res				double precision;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total_raster_comb_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total_raster_comb_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;						
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro vstupni _config_id
	SELECT
		raster,
		raster_1
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id
	INTO
		_config_id_raster,
		_config_id_raster_1;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro raster z tabulky t_config_collection
	SELECT
		schema_name,
		table_name,
		column_name,
		unit
	FROM
		@extschema@.t_config_collection
	WHERE
		id = (SELECT config_collection FROM @extschema@.t_config WHERE id = _config_id_raster)
	INTO
		_schema_name,
		_table_name,
		_column_name,
		_unit;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro raster_1 z tabulky t_config_collection
	SELECT
		schema_name,
		table_name,
		column_name,
		unit
	FROM
		@extschema@.t_config_collection
	WHERE
		id = (SELECT config_collection FROM @extschema@.t_config WHERE id = _config_id_raster_1)
	INTO
		_schema_name_1,
		_table_name_1,
		_column_name_1,
		_unit_1;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro raster z tabulky t_config
	SELECT
		band,
		reclass,
		condition
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id_raster
	INTO
		_band,
		_reclass,
		_condition;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro raster_1 z tabulky t_config
	SELECT
		band,
		reclass,
		condition
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id_raster_1
	INTO
		_band_1,
		_reclass_1,
		_condition_1;
	-------------------------------------------------------------------------------------------
	-- prevedeni gidu na character varying
	_gid_text := _gid::character varying;
	-------------------------------------------------------------------------------------------
	-- vytvoreni (sestaveni) vysledneho sql dotazu pro ziskatni hodnoty aux_total
	SELECT @extschema@.fn_sql_aux_total_raster_comb_app
		(
		_config_id,
		_schema_name, _table_name, _column_name, _band, _reclass, _condition, _unit,
		_schema_name_1, _table_name_1, _column_name_1, _band_1, _reclass_1,  _condition_1, _unit_1,
		_gid_text
		)
	INTO _command;	
	-------------------------------------------------------------------------------------------
	EXECUTE ''||_command||'' INTO _res;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	RETURN _res;
	-------------------------------------------------------------------------------------------
END ;
$BODY$
LANGUAGE plpgsql VOLATILE ;
	
ALTER FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer) IS
'Funkce vraci vypocitanou hodnotu aux_total_raster_combination pro zadanou konfiguraci (config_id) a zadany gid geometrie z tabulky f_a_cell.';

-- </function>



-- <function name="fn_get_aux_total_vector_app" schema="@extschema@" src="functions/extschema/fn_get_aux_total_vector_app.sql">
-- DROP FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total_vector_app
(
    _config_id integer,
    _gid integer
)
  RETURNS double precision AS
$BODY$
DECLARE
	_config_collection		integer;
	_condition			character varying;
	_schema_name			character varying;
	_table_name			character varying;
	_column_name			character varying;
	_unit				double precision;
	_gid_text			character varying;
	_command			text;
	_res				double precision;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total_vector_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total_vector_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro vstupni _config_id
	SELECT
		config_collection,
		condition
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id
	INTO
		_config_collection,
		_condition;
	-------------------------------------------------------------------------------------------
	SELECT
		schema_name,
		table_name,
		column_name,
		unit
	FROM
		@extschema@.t_config_collection
	WHERE
		id = _config_collection
	INTO
		_schema_name,
		_table_name,
		_column_name,
		_unit;
	-------------------------------------------------------------------------------------------
	-- prevedeni gidu na character varying
	_gid_text := _gid::character varying;
	-------------------------------------------------------------------------------------------
	-- vytvoreni (sestaveni) vysledneho sql dotazu pro ziskatni hodnoty aux_total
	SELECT @extschema@.fn_sql_aux_total_vector_app(_config_id, _schema_name, _table_name, _column_name, _condition, _unit, _gid_text)
	INTO _command;
	-------------------------------------------------------------------------------------------
	EXECUTE ''||_command||'' INTO _res;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------	
	RETURN _res;
	-------------------------------------------------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer) IS
'Funkce vraci vypocitanou hodnotu aux_total_vector pro zadanou konfiguraci (config_id) a zadany gidu geometrie z tabulky f_a_cell.';

-- </function>



-- <function name="fn_get_aux_total_vector_comb_app" schema="@extschema@" src="functions/extschema/fn_get_aux_total_vector_comb_app.sql">
-- DROP FUNCTION @extschema@.fn_get_aux_total_vector_comb_app(integer,integer);
  
CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total_vector_comb_app
( 
	_config_id			integer,
	_gid				integer
)
RETURNS double precision AS
$BODY$
DECLARE
	_config_id_vector		integer;
	_config_id_raster		integer;
	_schema_name			character varying;
	_table_name			character varying;
	_column_name			character varying;
	_schema_name_1			character varying;
	_table_name_1			character varying;
	_column_name_1			character varying;
	_unit				double precision;
	_unit_1				double precision;
	_condition			character varying;	
	_condition_1			character varying;
	_band_1				integer;
	_reclass_1			integer;
	_gid_text			character varying;
	_command			text;
	_res				double precision;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total_vector_comb_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total_vector_comb_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;	
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro vstupni _config_id
	SELECT
		vector,
		raster
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id
	INTO
		_config_id_vector,
		_config_id_raster;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro vector z t_config_collection pro _config_id_vector
	SELECT
		schema_name,
		table_name,
		column_name,
		unit
	FROM
		@extschema@.t_config_collection
	WHERE
		id = (SELECT config_collection FROM @extschema@.t_config WHERE id = _config_id_vector)
	INTO
		_schema_name,
		_table_name,
		_column_name,
		_unit;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro raster z t_config_collection pro _config_id_raster
	SELECT
		schema_name,
		table_name,
		column_name,
		unit
	FROM
		@extschema@.t_config_collection
	WHERE
		id = (SELECT config_collection FROM @extschema@.t_config WHERE id = _config_id_raster)
	INTO
		_schema_name_1,
		_table_name_1,
		_column_name_1,
		_unit_1;
	-------------------------------------------------------------------------------------------	
	-- proces ziskani konfiguraci pro vektor z t_config pro _config_id_vector
	SELECT
		condition
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id_vector
	INTO
		_condition;
	-------------------------------------------------------------------------------------------
	SELECT
		condition,
		band,
		reclass
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id_raster
	INTO
		_condition_1,
		_band_1,
		_reclass_1;
	-------------------------------------------------------------------------------------------
	-- prevedeni gidu na character varying
	_gid_text := _gid::character varying;
	-------------------------------------------------------------------------------------------
	-- vytvoreni (sestaveni) vysledneho sql dotazu pro ziskatni hodnoty aux_total
	SELECT @extschema@.fn_sql_aux_total_vector_comb_app
		(
		_config_id,
		_schema_name, _table_name, _column_name, _condition, _unit,
		_schema_name_1, _table_name_1, _column_name_1, _band_1, _reclass_1,  _condition_1, _unit_1,
		_gid_text
		)
	INTO _command;	
	-------------------------------------------------------------------------------------------
	EXECUTE ''||_command||'' INTO _res;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	RETURN _res;
	-------------------------------------------------------------------------------------------
END ;
$BODY$
LANGUAGE plpgsql VOLATILE ;
	
ALTER FUNCTION @extschema@.fn_get_aux_total_vector_comb_app(integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_vector_comb_app(integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_vector_comb_app(integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_vector_comb_app(integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total_vector_comb_app(integer,integer) IS
'Funkce vraci vypocitanou hodnotu aux_total_vector_combination pro zadanou konfiguraci (config_id) a zadany gid geometrie z tabulky f_a_cell.';

-- </function>
---------------------------------------------------------------------------------------------------;
