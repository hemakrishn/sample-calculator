#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import argparse
import re

def clear_file(fname):
    print('clearing %s' % fname)
    f = open(fname, 'r')
    fw = open(fname+'.tmp', 'w')
    lines = f.readlines()
    fline = False
    for i, l in enumerate(lines):
        if re.search('-- <function.*>', l) or re.search('-- <view.*>', l): 
            fline = True
            fw.write(lines[i])
        if not fline:
            fw.write(lines[i])
        if re.search('-- </function>', l) or re.search('-- </view>', l): 
            fline = False
            fw.write(lines[i])
    f.close()
    fw.close()
    os.replace(fname+'.tmp', fname)

def update_file(fname):
    print('updating %s' % fname)    
    f = open(fname, 'r')
    fw = open(fname+'.tmp', 'w')
    lines = f.readlines()
    dirname = os.path.dirname(os.path.abspath(fname))
    for i, l in enumerate(lines):
        fw.write(lines[i])
        if re.search('-- <function.*>', l) or re.search('-- <view.*>', l):
            srcfname = os.path.join(dirname, re.search('src="(.*)"', lines[i]).group(1))
            print('inserting source %s' % srcfname)
            srcf = open(srcfname, 'r')
            for srcl in srcf.readlines():
                fw.write(srcl)
            fw.write('\n')
            srcf.close()
    f.close()
    fw.close()
    os.replace(fname+'.tmp', fname)


def main():
    parser = argparse.ArgumentParser(description="Utility for management of changescript.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            epilog='''\
Example:
    template -> changescript
        python template_chscript_convertor.py -i chscript.sql
    changescript -> changescript (update of funcions in changescript)
        python template_chscript_convertor.py -i chscript.sql
    changescript -> template
        python template_chscript_convertor.py -i chscript.sql -t
            ''')
    parser.add_argument('-t', '--template', action='store_true', required=False, help='create template from changescript')
    parser.add_argument('-i', '--infile', required=True, help='input file to scan')

    args = parser.parse_args()

    if not args.template:
        clear_file(args.infile)
        update_file(args.infile)
    else:
        clear_file(args.infile)

if __name__ == '__main__':
    main()
