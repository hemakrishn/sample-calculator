# gitlab CI/CD environment

## get docker image

Create docker image

```bash
docker build -t registry.gitlab.com/nfiesta/nfiesta_gisdata:latest .
docker push registry.gitlab.com/nfiesta/nfiesta_gisdata:latest
```

or pull directly from repository

```bash
docker pull registry.gitlab.com/nfiesta/nfiesta_gisdata:latest
```

run image locally
```bash
docker image ls
docker run -i -t -v `pwd`:/home/vagrant/work_dir -p 5432:5432 --name nfiesta_gisdata_dev --hostname nfiesta_gisdata_dev registry.gitlab.com/nfiesta/nfiesta_gisdata:latest /bin/bash
docker container ls -a
docker container rm -v nfiesta_gisdata_dev
```

set-up local environment and run CI/CD jobs locally
```
sudo dpkg-reconfigure tzdata
# git clone <dotfiles>
# sudo apt install ssh && set up or copy ssh keys && chmod 700 ~/.ssh/id_rsa
sudo mkdir -p /builds/nfiesta/nfiesta_gisdata
sudo chown -R vagrant /builds
# see https://gitlab.com/nfiesta/nfiesta_gisdata/-/blob/master/.gitlab-ci.yml
sudo sed -i "s/#jit = on/jit = off/" /etc/postgresql/12/main/postgresql.conf
sudo service postgresql start
sudo -u postgres psql -c "create user vagrant with superuser;"
sudo -u postgres psql -c "create role adm_nfiesta_gisdata;"
sudo -u postgres psql -c "create role app_nfiesta_gisdata;"
sudo apt install git-lfs
git lfs install
git clone https://gitlab.com/nfiesta/nfiesta_gisdata.git
cd /builds/nfiesta/nfiesta_gisdata/extension
sudo make install
make installcheck
```
